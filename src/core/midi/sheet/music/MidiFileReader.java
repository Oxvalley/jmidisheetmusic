package core.midi.sheet.music;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @class MidiFileReader The MidiFileReader is used to read low-level binary
 *        data from a file. This class can do the following:
 *
 *        - Peek at the next char : the file. - Read a char - Read a 16-bit big
 *        endian short - Read a 32-bit big endian int - Read a fixed length
 *        ascii String (not null terminated) - Read a "variable length" integer.
 *        The format of the variable length int is described at the top of this
 *        file. - Skip ahead a given number of chars - Return the current
 *        offset.
 */

public class MidiFileReader
{
   private int parse_offset2 = 0;
   private String data2;

   /** The current offset while parsing */

   /**
    * Create a new MidiFileReader for the given filename
    *
    * @throws MidiFileException
    * @throws IOException
    */
   public MidiFileReader(String filename) throws MidiFileException, IOException
   {
      File info = new File(filename);
      if (!info.exists())
      {
         throw new MidiFileException("File " + filename + " does not exist", 0);
      }
      if (info.length() == 0)
      {
         throw new MidiFileException(
               "File " + filename + " is empty (0 chars)", 0);
      }



      /*
       * Create byte array large enough to hold the content of the file. Use
       * File.length to determine size of the file in bytes.
       */


      data2 = getContents(filename);
      parse_offset2 = 0;

      /*
       * To read content of the file in byte array, use int read(byte[]
       * byteArray) method of java FileInputStream class.
       */
   }

   /** Create a new MidiFileReader from the given data */
   public MidiFileReader()
   {
   }


   /**
    * Fetch the entire contents of a text file, and return it in a String. This
    * style of implementation does not throw Exceptions to the caller.
    *
    * @param aFile
    *           is a file which already exists and can be read.
    */
   static public String getContents(String filePath)
   {
      StringBuffer retVal = new StringBuffer();
      try
      {
         BufferedInputStream stream1 = new BufferedInputStream(
               new FileInputStream(filePath));
         int c1;

         while ((c1 = stream1.read()) != -1)
         {
            retVal.append((char) c1);
         }
         
         stream1.close();
      }
      catch (FileNotFoundException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      return retVal.toString();
   }


   /**
    * Read the next char : the file, but don't increment the parse offset
    *
    * @throws MidiFileException
    */
   public char Peek2() throws MidiFileException
   {
      checkRead2(1);
 
      return data2.charAt(parse_offset2);
   }


   public char ReadByte2() throws MidiFileException
   {
      checkRead2(1);
      char x = data2.charAt(parse_offset2);
      parse_offset2++;
      return x;
   }

   /**
    * Check that the given number of chars doesn't exceed the file size
    *
    * @throws MidiFileException
    */
   private void checkRead2(int amount) throws MidiFileException
   {
    if (parse_offset2 + amount > data2.length())
    {
       throw new MidiFileException("File is truncated",parse_offset2);
    }
   }


   /**
    * Read a variable-length integer (1 to 4 chars). The integer ends when you
    * encounter a char that doesn't have the 8th bit set (a char less than
    * 0x80).
    *
    * @throws MidiFileException
    */
   public int ReadVarlen2() throws MidiFileException
   {
      int result = 0;
      int count = 0;
      int b = ReadByte2();

     while (b  >= 128 && count < 4)
    {
         count++;
         // Always remove bit 8 if any
         b = b % 128;
         result = result * 128 + b;
         b = ReadByte2();
      }

     // Last number always less than 128
     result = result * 128 + b;
      return result;
   }

   /**
    * Skip over the given number of chars
    *
    * @throws MidiFileException
    */
   public void Skip2(int amount) throws MidiFileException
   {
      checkRead2(amount);
      parse_offset2 += amount;
   }


   public int GetOffset2()
   {
      return parse_offset2;
   }


   /** Return the raw midi file char data */
   public byte[] GetData2()
   {
      return data2.getBytes();
   }

   public String ReadAscii2(int i)
   {
      String retVal = data2.substring(parse_offset2, parse_offset2 + i);
      parse_offset2 += i;
      return retVal;
   }

   public int ReadInt2()
   {
      int retVal = data2.charAt(parse_offset2) * 256 * 256 * 256
            + data2.charAt(parse_offset2 + 1) * 256 * 256
            + data2.charAt(parse_offset2 + 2) * 256
            + data2.charAt(parse_offset2 + 3);
      parse_offset2 += 4;
      return retVal;
   }

   public int ReadShort2()
   {
      int retVal = data2.charAt(parse_offset2) * 256
            + data2.charAt(parse_offset2 + 1);
      parse_offset2 += 2;
      return retVal;
   }

 /**
 * Read the given number of chars from the file
 *
 * @throws MidiFileException
 */
public char[] ReadBytes2(int amount) throws MidiFileException
{
   checkRead2(amount);
   String result = "";

   for (int i = 0; i < amount; i++)
   {
      result = data2.substring(parse_offset2,parse_offset2+amount);
   }
   parse_offset2 += amount;
   return result.toCharArray();
}

}
