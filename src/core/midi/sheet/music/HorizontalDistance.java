package core.midi.sheet.music;

public class HorizontalDistance
{

   public boolean getRetVal()
   {
      return retVal;
   }

   public int getDistance()
   {
      return distance;
   }

   private boolean retVal;
   private int distance;

   public HorizontalDistance(boolean retVal)
   {
      this.retVal = retVal;
   }

   public HorizontalDistance(boolean retVal, int distance)
   {
      this(retVal);
      this.distance = distance;
   }

}
