package core.midi.sheet.music;

/**
 * @class MidiFileException A MidiFileException is thrown when an error occurs while parsing the Midi
 *        File. The constructor takes the file offset (in chars) where the error occurred, and a
 *        String describing the error.
 */
public class MidiFileException extends Exception
{
    /**
    *
    */
    private static final long serialVersionUID = -3830427592553437661L;

    public MidiFileException(String s, long l)
    {
        super(s + " at offset " + l);
    }
}
