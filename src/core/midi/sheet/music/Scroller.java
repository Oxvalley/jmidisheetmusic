package core.midi.sheet.music;

import java.awt.Rectangle;

import javax.swing.JScrollPane;

public class Scroller implements ScrollerInterface
{

   private JScrollPane iScrollView;

   public Scroller(JScrollPane scrollView)
   {
      this.iScrollView = scrollView;
   }

   @Override
   public void scrollRectToVisible(Rectangle rectangle)
   {

      if (!iScrollView.getViewport().getVisibleRect().contains(rectangle))
      {
         iScrollView.getViewport().scrollRectToVisible(rectangle);
      }
   }

}
