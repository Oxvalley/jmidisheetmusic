package core.midi.sheet.music;

/** Accidentals */
public enum Accid
{
    None, Sharp, Flat, Natural
}
