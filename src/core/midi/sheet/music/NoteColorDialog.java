package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

/**
 * @class NoteColorDialog The NoteColorDialog is used to choose what color to
 *        display for each of the 12 notes in a scale, as well as the shade
 *        color.
 */
public class NoteColorDialog extends AbstractJDialogExt
{

   /**
   *
   */
   private static final long serialVersionUID = 1L;
   // private Color[] colors;
   /** The 12 colors used for each note in the note scale */
   private JButtonExt[] JButtons;
   /** The 12 JButtons used to select the colors. */
   private Color shadeColor;
   /** The color used for shading notes during playback */
   private Color shade2Color;
   /** The color used for shading the left hand piano. */
   private JButtonExt shadeButton;
   /** The JButton used to select the shade color */
   private JButton shade2Button;
   private ColoredKeyManager man;

   /** The JButton used to select the shade2 color */

   /** The dialog box */

   /**
    * Create a new NoteColor Call the ShowDialog() method to display the
    *
    */
   public NoteColorDialog()
   {
      /* Create the dialog box */
      setFont(new Font(JMidiSheetMusic.GENERIC_FONT_NAME, Font.PLAIN,
            (int) (JMidiSheetMusic.GENERIC_FONT_SIZE * 1.4)));
      int unit = getFont().getSize() * 4 / 3;
      int xstart = unit * 2;
      int ystart = unit * 2;
      int labelheight = unit * 3 / 2;
      int maxwidth = 0;

      setTitle("Choose Note Colors");
      setResizable(false);
      ((java.awt.Frame) getOwner()).setIconImage(getToolkit().getImage("images/NotePair.ico"));
      setModal(true);
      setLayout(null);

      /* Initialize the colors */
      shadeColor = new Color(210, 205, 220);
      shade2Color = new Color(150, 200, 220);
      // colors = new Color[12];
      // colors[0] = new Color(180, 0, 0);
      // colors[1] = new Color(230, 0, 0);
      // colors[2] = new Color(220, 128, 0);
      // colors[3] = new Color(130, 130, 0);
      // colors[4] = new Color(187, 187, 0);
      // colors[5] = new Color(0, 100, 0);
      // colors[6] = new Color(0, 140, 0);
      // colors[7] = new Color(0, 180, 180);
      // colors[8] = new Color(0, 0, 120);
      // colors[9] = new Color(0, 0, 180);
      // colors[10] = new Color(88, 0, 147);
      // colors[11] = new Color(129, 0, 215);

      JButtons = new JButtonExt[12];

      String[] names = { "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };

      /* Create the first column, note labels A thru D */
      for (int i = 0; i < 6; i++)
      {
         JLabel note = new JLabel();
         add(note);
         note.setText(names[i]);
         note.setLocation(xstart, ystart + i * labelheight);
         maxwidth = Math.max(maxwidth, note.getWidth());
      }

      /* Create the second column, the colors */
      xstart += maxwidth * 4 / 3;
      for (int i = 0; i < 6; i++)
      {
         JButtons[i] = new JButtonExt();
         final JButtonExt thisButton = JButtons[i];
         add(JButtons[i]);
         JButtons[i].setText("");
         JButtons[i].setTag(i);
         JButtons[i].setLocation(xstart, ystart + i * labelheight);
         JButtons[i].setSize(maxwidth * 3 / 2, labelheight - 4);
         man = ColoredKeyManager.getInstance();
         JButtons[i].setBackground(man.getKeyColor(i));
         JButtons[i].setForeground(man.getKeyColor(i));

         JButtons[i].addActionListener(new ActionListener()
         {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
               SetNoteColor(thisButton);
            }
         });

      }

      /* Create the third column, note labels D# thru G# */
      xstart += maxwidth * 2;
      for (int i = 6; i < 12; i++)
      {
         JLabel note = new JLabel();
         add(note);
         note.setText(names[i]);
         note.setLocation(xstart, (i - 6) * labelheight + ystart);
         maxwidth = Math.max(maxwidth, note.getWidth());
      }

      /* Create the fourth column, the colors */
      xstart += maxwidth * 4 / 3;
      for (int i = 6; i < 12; i++)
      {
         JButtons[i] = new JButtonExt();
         final JButtonExt thisButton = JButtons[i];
         add(JButtons[i]);
         JButtons[i].setText("");
         JButtons[i].setTag(i);
         JButtons[i].setLocation(xstart, (i - 6) * labelheight + ystart);
         JButtons[i].setSize(maxwidth * 3 / 2, labelheight - 4);
         JButtons[i].setBackground(man.getKeyColor(i));
         JButtons[i].setForeground(man.getKeyColor(i));

         JButtons[i].addActionListener(new ActionListener()
         {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
               SetNoteColor(thisButton);
            }
         });
      }

      /* Create the shade color JButtons */
      xstart = unit * 2;
      JLabel label = new JLabel();
      add(label);
      label.setText("Right Shade");
      label.setLocation(xstart, 6 * labelheight + ystart);
      label.setSize(maxwidth * 4, labelheight);
      label.setHorizontalAlignment(SwingConstants.CENTER);

      shadeButton = new JButtonExt();
      add(shadeButton);
      shadeButton.setText(" ");
      shadeButton.setLocation((int) JButtons[11].getLocation().getX(),
            (int) label.getLocation().getY());
      shadeButton.setSize(maxwidth * 3 / 2, labelheight - 4);
      shadeButton.setBackground(shadeColor);
      shadeButton.setForeground(shadeColor);
      shadeButton.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            SetNoteColor(shadeButton);
         }
      });

      /* Create the shade2 color JButtons */
      label = new JLabel();
      add(label);
      label.setText(" Left Shade");
      label.setLocation(xstart, 7 * labelheight + ystart);
      label.setSize(maxwidth * 4, labelheight);
      label.setHorizontalAlignment(SwingConstants.CENTER);

      shade2Button = new JButton();
      add(shade2Button);
      shade2Button.setText(" ");
      shade2Button.setLocation((int) (JButtons[11].getLocation().getX()),
            (int) (label.getLocation().getY()));
      shade2Button.setSize(maxwidth * 3 / 2, labelheight - 4);
      shadeButton.setBackground(shade2Color);
      shadeButton.setForeground(shade2Color);
      shadeButton.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            SetNoteColor(shadeButton);
         }
      });

      /* Create the OK and Cancel JButtons */
      xstart = unit * 2;
      JButton ok = new JButton();
      add(ok);
      ok.setText(" OK");
      ok.setLocation(xstart, 9 * labelheight + ystart);
      ok.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            setOKClicked(true);
            Dispose();
         }
      });

      JButton cancel = new JButton();
      add(cancel);
      cancel.setText(" Cancel");
      cancel.setLocation(xstart + ok.getWidth() + unit, 9 * labelheight + ystart);
      cancel.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            setOKClicked(false);
            Dispose();
         }
      });

      setSize((int) (cancel.getLocation().getX() + cancel.getSize().getWidth() + 40),
            (int) (cancel.getLocation().getY() + cancel.getSize().getHeight() + 80));
   }

   /**
    * Display the NoteColor Save the old colors for restoring, in case "Cancel"
    * is clicked. Return DialogResult.OK if "OK" was clicked. Return
    * DialogResult.Cancel if "Cancel" was clicked.
    */
   @Override
   public boolean ShowDialog()
   {
      Color[] oldcolors = new Color[12];
      for (int i = 0; i < 12; i++)
      {
         oldcolors[i] = man.getKeyColor(i);
      }
      Color oldShadeColor = shadeColor;
      Color oldShade2Color = shade2Color;
      boolean result = super.ShowDialog();
      if (result == false)
      {
         /* Restore the old colors */
         for (int i = 0; i < 12; i++)
         {
            // TODO We can not change colors for now
            // man.setKeyColor(oldcolors[i]);
            JButtons[i].setForeground(oldcolors[i]);
            JButtons[i].setBackground(oldcolors[i]);
         }
         shadeColor = oldShadeColor;
         shade2Color = oldShade2Color;
      }
      return result;
   }

   /**
    * The JButton event handler. Open a Color Chooser dialog, and set the
    * JButton color to the color chosen.
    */
   private void SetNoteColor(final JButtonExt b)
   {
      final ColorChooserDemo cd = new ColorChooserDemo(b.getForeground());
      cd.showDialog();
      cd.addChangeListener(new ChangeListener()
      {
         @Override
         public void stateChanged(ChangeEvent e)
         {
            if (b == shadeButton)
            {
               shadeColor = cd.getColor();
               shadeButton.setBackground(cd.getColor());
               shadeButton.setForeground(cd.getColor());
            }
            else if (b == shade2Button)
            {
               shade2Color = cd.getColor();
               shade2Button.setBackground(cd.getColor());
               shade2Button.setForeground(cd.getColor());
            }
            else
            {
               int index = b.getTag();
               // TODO can not set colors for now
               // man.setKeyColor(index, cd.getColor();
               b.setBackground(cd.getColor());
               b.setForeground(cd.getColor());
            }

         }
      });

      cd.dispose();

   }

//   /**
//    * Get the colors used for each note. There are 12 colors in the array.
//    */
//   public Color[] getColors()
//   {
//      return colors;
//   }

   /** Get the shade color selected */
   public Color getShadeColor()
   {
      return shadeColor;
   }

   /** Get the shade2 color selected */
   public Color getShade2Color()
   {
      return shade2Color;
   }

}
