package core.midi.sheet.music;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

public class JMidiSheetMusic
{

   public static final double GENERIC_FONT_SIZE = 8;
   public static final String GENERIC_FONT_NAME = "Arial";

   /** The Main function for this application. */
   // [STAThread]
   public static void main(String[] args)
   {
      JSheetMusicWindow form = new JSheetMusicWindow();

      if (args.length == 1)
      {
         form.OpenMidiFile(args[0]);
      }
      form.setVisible(true);
   }

}
