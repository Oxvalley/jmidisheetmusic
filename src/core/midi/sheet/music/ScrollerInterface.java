package core.midi.sheet.music;

import java.awt.Rectangle;

public interface ScrollerInterface
{

   void scrollRectToVisible(Rectangle rectangle);

}
