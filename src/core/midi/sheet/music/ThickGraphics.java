package core.midi.sheet.music;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class ThickGraphics
{




   public static Graphics2D setStrokeThickness(Graphics g, int size)
   {
      Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(size));
      return g2;
   }

   public static void resetStrokeThickness(Graphics2D g2)
   {
      g2.setStroke(new BasicStroke(25));
   }

}
