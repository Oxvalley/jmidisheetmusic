package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

/**
 * A window with a custom paintComponent method. Used to illustrate that you can make a
 * general-purpose method that can print any component, regardless of whether that component
 * performs custom drawing. See the PrintUtilities class for the printComponent method that lets you
 * print an arbitrary component with a single function call. 7/99 Marty Hall,
 * http://www.apl.jhu.edu/~hall/java/
 */

public class DrawingPanel extends JPanel
{
    /**
    *
    */
    private static final long serialVersionUID = 1991578555819090993L;
    private int fontSize = 90;
    private String message = "Java 2D";
    private int messageWidth;

    public DrawingPanel()
    {
        setBackground(Color.WHITE);
        Font font = new Font("Serif", Font.PLAIN, fontSize);
        setFont(font);
        FontMetrics metrics = getFontMetrics(font);
        messageWidth = metrics.stringWidth(message);
        int width = messageWidth * 5 / 3;
        int height = fontSize * 3;
        setPreferredSize(new Dimension(width, height));
    }

    /**
     * Draws a black String with a tall angled "shadow" of the String behind it.
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        int x = messageWidth / 10;
        int y = fontSize * 5 / 2;
        g2d.translate(x, y);
        g2d.setPaint(Color.LIGHT_GRAY);
        AffineTransform origTransform = g2d.getTransform();
        g2d.shear(-0.95, 0);
        g2d.scale(1, 3);
        g2d.drawString(message, 0, 0);
        g2d.setTransform(origTransform);
        g2d.setPaint(Color.BLACK);
        g2d.drawString(message, 0, 0);
    }
}
