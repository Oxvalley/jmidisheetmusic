package core.midi.sheet.music;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class SortJList extends JPanel implements ActionListener
{
   /**
    *
    */
   private static final long serialVersionUID = -2605160250559942728L;
   JButton jb1, jb2;
   JTextField tf;
   JList list;
   int i = 1;

   public SortJList()
   {
      setLayout(new BorderLayout());
      list = new JList();
      list.setModel(new DefaultListModel());
      add(new JScrollPane(list), "Center");
      add(jb1 = new JButton("Add"), "West");
      add(jb2 = new JButton("Sort"), "East");
      add(tf = new JTextField(), "North");
      jb1.addActionListener(this);
      jb2.addActionListener(this);
   }

   @Override
   public Dimension getPreferredSize()
   {
      return new Dimension(50, 50);
   }

   @Override
   public void actionPerformed(ActionEvent ae)
   {
      DefaultListModel dlm;
      if (ae.getSource() == jb1)
      {
         // add
         dlm = (DefaultListModel) list.getModel();
         dlm.addElement(tf.getText());
      }
      else
      {
         // sort
         dlm = (DefaultListModel) list.getModel();
         int numItems = dlm.getSize();
         List<String> lst = new ArrayList<String>();
         for (int i1 = 0; i1 < numItems; i1++)
         {
            lst.add((String) dlm.getElementAt(i1));
         }

         Collections.sort(lst);

         for (int i1 = 0; i1 < numItems; i1++)
         {
            dlm.setElementAt(lst.get(i1), i1);
         }
         invalidate();
      }
   }

   public static void main(String[] args)
   {
      JFrame frame = new JFrame("Sort JList");
      SortJList panel = new SortJList();
      frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      frame.getContentPane().add(panel, "Center");

      frame.setSize(200, 200);
      frame.setVisible(true);

   }
}
