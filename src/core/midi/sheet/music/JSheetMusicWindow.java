package core.midi.sheet.music;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan Java port Copyright(c) 2012 Lars Svensson
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License version 2.
 *
 * This program is distributed : the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.rtf.RTFEditorKit;

import common.properties.SettingProperties;
import common.utils.AppInfoInterface;

/**
 * @class SheetMusicWindow
 *
 *        The SheetMusicWindow is the main window/form of the application, that contains the Midi
 *        Player and the Sheet Music. The form supports the following menu commands
 *
 *        File Open Open a midi file, read it into a MidiFile object, and create a SheetMusic child
 *        control based on the MidiFile. The SheetMusic control is then displayed.
 *
 *        Open Sample Song Open one of the sample midi files that comes with MidiSheetMusic.
 *
 *        Close Close the SheetMusic control.
 *
 *        Save As Images Save the sheet music as images (one per page)
 *
 *        Print Preview Create a PrintPreview dialog and generate a preview of the SheetMusic. The
 *        SheetMusic.DoPrint() method handles the PrintPage callback function.
 *
 *        Print Create a PrintDialog to print the sheet music.
 *
 *        Exit Exit the application.
 *
 *        View Scroll Vertically Scroll the sheet music vertically.
 *
 *        Scroll Horizontally Scroll the sheet music horizontally.
 *
 *        Large/Small Notes Display large or small note sizes
 *
 *        Color Enable Color Show colored notes instead of black notes
 *
 *        Choose Color Choose the colors for each note
 *
 *        Tracks Track X Select which tracks of the Midi file to display
 *
 *        Use One/Two Staffs Display the Midi tracks : one staff per track, or two staffs for all
 *        tracks.
 *
 *        Choose Instruments... Choose which instruments to use per track, when playing the sound.
 *
 *        Notes
 *
 *        Key Signature Change the key signature.
 *
 *        Time Signature Change the time signature to 3/4, 4/4, etc
 *
 *        Transpose Keys Shift the note keys up or down.
 *
 *        Shift Notes Shift the notes left/right by the given number of 8th notes.
 *
 *        Measure Length Adjust the length (in pulses) of a single measure
 *
 *        Combine Notes Within Combine notes within the given millisec interval.
 *
 *        Show Note Letters : the sheet music, display the note letters (A, A#, Bb, etc) next to the
 *        notes.
 *
 *        Show Lyrics If the midi file has lyrics, display them under the notes.
 *
 *        Show Measure Numbers. : the sheet music, display the measure numbers : the staffs.
 *
 *        Play Measures : a Loop Play the selected measures : a loop
 *
 *        Help Contents Display a text area describing the MidiSheetMusic options.
 */

public class JSheetMusicWindow extends JFrame implements PropertyChangeListener
{
    private static final String MIDI_PATH = "midi.path";
    private static final String IMAGE_PATH = "image.path";
    /**
    *
    */
    private static final long serialVersionUID = 1L;
    MidiFileJavax midifile; /* The MIDI file to display */
    SheetMusic sheetmusic; /* The Control which displays the sheet music */
    JScrollPane scrollView; /* The Control for scrolling the sheetmusic */
    ScrollerInterface iScroller;
    MidiPlayerPanel player; /* The top panel for playing the music */
    Piano piano; /* The piano at the top, for highlighting notes */
    // PrintDocument printdoc; /* The printer settings */
    int toPage; /* The last page number to print */
    int currentpage; /* The current page we are printing */

    /* Color options */
    NoteColorDialog colordialog;

    /* Dialog for showing sample songs */
    SampleSongDialog sampleSongDialog;

    /* Dialog for choosing instruments */
    InstrumentDialog instrumentDialog;

    /* Dialog for playing measures : a loop */
    PlayMeasuresDialog playMeasuresDialog;

    /* Label on how to select a MIDI file */
    JLabel selectMidi;

    /* Menu Items */
    JMenuItem openMenu;
    JMenuItem openSampleSongMenu;
    JMenuItem saveMenu;
    JMenuItem closeMenu;
    JMenuItem previewMenu;
    JMenuItem printMenu;
    JMenuItem exitMenu;
    JMenu trackMenu;
    JMenu trackDisplayMenu;
    JMenu trackMuteMenu;
    JMenuItem oneStaffMenu;
    JMenuItem twoStaffMenu;
    JMenu notesMenu;
    JMenu showLettersMenu;
    JCheckBoxMenuItem showLyricsMenu;
    JCheckBoxMenuItem showMeasuresMenu;
    JMenu measureMenu;
    JMenu changeKeyMenu;
    JMenu transposeMenu;
    JMenu shiftNotesMenu;
    JMenu timeSigMenu;
    JMenu combineNotesMenu;
    JCheckBoxMenuItem playMeasuresMenu;
    JMenu colorMenu;
    private JCheckBoxMenuItem colorKeysMenu;
    // JCheckBoxMenuItem useColorMenu;
    // JMenuItem chooseColorMenu;
    JMenu chooseInstrumentsMenu;
    private JMenuBar menubar;
    private String iSongTitle;
    private String iFilename;
    private int iLastZoomValue;

    /**
     * Create a new instance of this Form. This window has three child controls: - The MidiPlayer -
     * The SheetMusic - The scrollView, for scrolling the SheetMusic. Create the menus. Create the
     * color dialog.
     */
    public JSheetMusicWindow()
    {

        setTitle("JMidi Sheet Music");
        setIconImage(ImageHelper.loadImage("images/NotePair.png"));

        getContentPane().setBackground(Color.GRAY);
        Dimension screenSize = getScreenDimensions();

        double screenwidth = screenSize.getWidth();
        double screenheight = screenSize.getHeight();

        setSize((int) screenwidth - 20, (int) screenheight * 9 / 10);

        CreateMenu();
        EnableMenus(false);
        setLayout(new BorderLayout());

        JSheetMusicPanel panel = new JSheetMusicPanel();
        panel.setLayout(null);
        add(panel, BorderLayout.CENTER);
        panel.getRootPane().addComponentListener(new ComponentListener()
        {

            @Override
            public void componentShown(ComponentEvent e)
            {
            }

            @Override
            public void componentResized(ComponentEvent e)
            {
                setScrollWindowSize();
            }

            @Override
            public void componentMoved(ComponentEvent e)
            {
            }

            @Override
            public void componentHidden(ComponentEvent e)
            {
            }
        });

        /* Create the player panel */
        piano = new Piano();
        player = new MidiPlayerPanel(piano);
        player.addPropertyChangeListener(this);
        panel.add(player);
        player.setBackground(Color.LIGHT_GRAY);
        player.setBounds(0, 0, player.getWidth(), player.getHeight());

        panel.add(piano);
        piano.setBounds(0, player.getHeight(), piano.getWidth(), piano.getHeight());

        scrollView = new JScrollPane();
        iScroller = new Scroller(scrollView);
        sheetmusic = new SheetMusic(iScroller);
        scrollView.setViewportView(sheetmusic);

        scrollView.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollView.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panel.add(scrollView);

        selectMidi = new JLabel("Use the menu File:Open to select a MIDI file");
        selectMidi.setFont(new Font("Arial", Font.BOLD, 16));
        scrollView.getViewport().add(selectMidi);
        setScrollWindowSize();

        colordialog = new NoteColorDialog();

        CreatePrintSettings();
        setSize(Math.max(getWidth(), piano.getWidth() + 40), getHeight());

        // /** When the window is resized, adjust the scrollView to fill the
        // window */
        // protected void OnResize(EventArgs e) {
        // base.OnResize(e);
        // if (piano != null) {
        // piano.Size = new Dimension(player.getWidth(), piano.Size.Height);
        // }
        // if (scrollView != null) {
        // scrollView.Size = new Dimension(player.getWidth(), ClientSize.Height -
        // (player.Height + piano.Height));
        // }
        // }

        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                SettingProperties prop = SettingProperties.getInstance();
                prop.save();
                System.exit(0);
            }
        });
    }

    public void setScrollWindowSize()
    {
        scrollView.setBounds(0, player.getHeight() + piano.getHeight(), getWidth() - 10,
                             getHeight() - player.getHeight() - piano.getHeight() - 70);
        scrollView.revalidate();
    }

    /**
     * @return
     */
    public static Dimension getScreenDimensions()
    {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();

        int screenwidth = 0;
        int screenheight = 0;

        // Get size of each screen
        for (int i = 0; i < gs.length; i++)
        {
            DisplayMode dm = gs[i].getDisplayMode();
            screenwidth = dm.getWidth();
            screenheight = dm.getHeight();
        }
        return new Dimension(screenwidth, screenheight);
    }

    /**
     * Create the MidiOptions based on the menus
     *
     * @throws Exception
     */
    MidiOptions GetMidiOptions() throws Exception
    {
        MidiOptions options = new MidiOptions(midifile);

        /* Get the list of selected tracks from the Tracks menu */
        for (int track = 0; track < midifile.getTracks().size(); track++)
        {
            options.tracks[track] =
                ((JCheckBoxMenuItem) trackDisplayMenu.getMenuComponent(track + 2)).isSelected();
            options.mute[track] =
                ((JCheckBoxMenuItem) trackMuteMenu.getMenuComponent(track + 2)).isSelected();
        }
        options.twoStaffs = twoStaffMenu.isSelected();
        options.showNoteLetters = MidiOptions.NoteNameNone;
        for (int i = 0; i < 6; i++)
        {
            if (((JRadioButtonMenuItem) showLettersMenu.getMenuComponent(i)).isSelected())
            {
                options.showNoteLetters =
                    (int) ((JRadioButtonMenuItemExt) showLettersMenu.getMenuComponent(i)).getTag();
            }
        }
        if (showLyricsMenu != null)
        {
            options.showLyrics = showLyricsMenu.isSelected();
        }
        options.showMeasures = showMeasuresMenu.isSelected();
        options.shifttime = 0;
        options.transpose = 0;
        options.key = -1;
        options.time = midifile.getTime();

        /* Get the time signature to use */
        for (Component menu2 : timeSigMenu.getMenuComponents())
        {

            JRadioButtonMenuItem menu = null;

            if (menu2 instanceof JRadioButtonMenuItem)
            {
                menu = (JRadioButtonMenuItem) menu2;
                int quarter = options.time.getQuarter();
                int tempo = options.time.getTempo();
                if (menu.isSelected() && !menu.getText().contains("default"))
                {
                    if (menu.getText().equals("3/4"))
                    {
                        options.time = new TimeSignature(3, 4, quarter, tempo);
                    }
                    else if (menu.getText().equals("4/4"))
                    {
                        options.time = new TimeSignature(4, 4, quarter, tempo);
                    }
                }
            }
        }

        /* Get the measure length to use */
        for (Component menu2 : measureMenu.getMenuComponents())
        {

            JRadioButtonMenuItemExt menu = null;

            if (menu2 instanceof JRadioButtonMenuItemExt)
            {
                menu = (JRadioButtonMenuItemExt) menu2;
                if (menu.isSelected())
                {
                    int num = options.time.getNumerator();
                    int denom = options.time.getDenominator();
                    long measure = menu.getTag();
                    int tempo = options.time.getTempo();
                    long quarter = measure * options.time.getQuarter() / options.time.getMeasure();
                    options.time = new TimeSignature(num, denom, quarter, tempo);
                }
            }
        }

        /* Get the amount to shift the notes left/right */
        for (Component menu2 : shiftNotesMenu.getMenuComponents())
        {
            JRadioButtonMenuItemExt menu = null;

            if (menu2 instanceof JRadioButtonMenuItemExt)
            {
                menu = (JRadioButtonMenuItemExt) menu2;
                if (menu.isSelected())
                {
                    int shift = (int) menu.getTag();
                    if (shift >= 0)
                        options.shifttime = midifile.getTime().getQuarter() / 2 * (shift);
                    else
                        options.shifttime = shift;
                }

            }

        }

        /* Get the key signature to use */
        for (Component menu2 : changeKeyMenu.getMenuComponents())
        {
            JRadioButtonMenuItemExt menu = null;

            if (menu2 instanceof JRadioButtonMenuItemExt)
            {
                menu = (JRadioButtonMenuItemExt) menu2;
                if (menu.isSelected() && !menu.getText().contains("Default"))
                {
                    int tag = (int) menu.getTag();
                    /*
                     * If the tag is positive, it has the number of sharps. If the tag is negative,
                     * it has the number of flats.
                     */
                    int num_flats = 0;
                    int num_sharps = 0;
                    if (tag < 0)
                        num_flats = -tag;
                    else
                        num_sharps = tag;
                    options.key =
                        new KeySignature(num_sharps, num_flats, sheetmusic.getSizes()).Notescale();
                }
            }
        }

        /* Get the amount to transpose the key up/down */
        for (Component menu2 : transposeMenu.getMenuComponents())
        {
            JRadioButtonMenuItemExt menu = null;

            if (menu2 instanceof JRadioButtonMenuItemExt)
            {
                menu = (JRadioButtonMenuItemExt) menu2;
                if (menu.isSelected())
                {
                    options.transpose = (int) menu.getTag();
                }
            }
        }

        /* Get the time interval for combining notes into the same chord. */
        for (Component menu2 : combineNotesMenu.getMenuComponents())
        {
            JRadioButtonMenuItemExt menu = null;

            if (menu2 instanceof JRadioButtonMenuItemExt)
            {
                menu = (JRadioButtonMenuItemExt) menu2;
                if (menu.isSelected())
                {
                    options.combineInterval = (int) menu.getTag();
                }
            }
        }

        /* Get the list of instruments from the Instrument dialog */
        options.instruments = instrumentDialog.getInstruments();
        options.useDefaultInstruments = instrumentDialog.isDefault();

        /* Get the speed/tempo to use */
        options.tempo = midifile.getTime().getTempo();
        options.DivisionType = midifile.getDivisionType();
        options.Resolution = midifile.getResolution();

        /* Get whether to play measures : a loop */
        options.playMeasuresInLoop = playMeasuresDialog.isEnabled();
        if (options.playMeasuresInLoop)
        {
            options.playMeasuresInLoopStart = playMeasuresDialog.GetStartMeasure();
            options.playMeasuresInLoopEnd = playMeasuresDialog.GetEndMeasure();
            if (options.playMeasuresInLoopStart > options.playMeasuresInLoopEnd)
            {
                options.playMeasuresInLoopEnd = options.playMeasuresInLoopStart;
            }
        }

        /* Get the note colors to use */
        options.shadeColor = colordialog.getShadeColor();
        options.shade2Color = colordialog.getShade2Color();
        // if (useColorMenu.isSelected())
        // {
        // options.colors = colordialog.getColors();
        // }
        // else
        // {
        // options.colors = null;
        // }
        return options;
    }

    /**
     * The Sheet Music needs to be redrawn. Gather the sheet music options from the menu items. Then
     * create the sheetmusic control, and add it to this form. Update the MidiPlayer with the new
     * midi file.
     *
     * @param filename
     */
    void RedrawSheetMusic(String filename)
    {
        if (selectMidi != null)
        {
            selectMidi = null;
        }

        if (sheetmusic != null)
        {
            sheetmusic = null;
        }

        MidiOptions options = null;
        try
        {
            options = GetMidiOptions();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /* Create a new SheetMusic Control from the midifile */

        if (sheetmusic == null)
        {
            sheetmusic = new SheetMusic(midifile, options, iScroller);
        }
        else
        {
            sheetmusic.init(midifile, options);
        }

        if (filename != null)
        {
            sheetmusic.setSongTitle(filename);
        }

        sheetmusic.setBounds(0, 300, SheetMusicSizes.DEFAULT_NOTE_PAGE_WIDTH,
                             SheetMusicSizes.DEFAULT_NOTE_PAGE_HEIGHT);
        sheetmusic.setPreferredSize(new Dimension(SheetMusicSizes.DEFAULT_NOTE_PAGE_WIDTH,
            SheetMusicSizes.DEFAULT_NOTE_PAGE_HEIGHT));

        sheetmusic.initPageDivider();

        scrollView.setViewportView(sheetmusic);

        /* Update the Midi Player and piano */
        piano.SetMidiFile(midifile, options);
        // piano.SetShadeColors(colordialog.getShadeColor(),
        // colordialog.getShade2Color());
        player.SetMidiFile(midifile, options, sheetmusic);
    }

    /** Create the menu for this form. */
    void CreateMenu()
    {

        menubar = new JMenuBar();

        CreateFileMenu();
        CreateColorMenu();

        trackMenu = new JMenu("Tracks");
        trackMenu.setMnemonic(KeyEvent.VK_T);
        menubar.add(trackMenu);

        notesMenu = new JMenu("Notes");
        notesMenu.setMnemonic(KeyEvent.VK_N);
        menubar.add(notesMenu);

        CreateHelpMenu();

        setJMenuBar(menubar);
    }

    /** Create the File menu */
    void CreateFileMenu()
    {
        JMenu filemenu = new JMenu("File");
        filemenu.setMnemonic(KeyEvent.VK_F);

        // Event = Open
        openMenu =
            getMenuItem(filemenu, openMenu, "Open...", KeyEvent.VK_O, KeyEvent.VK_O,
                        ActionEvent.CTRL_MASK);
        openMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Open();
            }
        });

        // Event = OpenSampleSong
        openSampleSongMenu = getMenuItem(filemenu, openSampleSongMenu, "Open Sample Song...");
        openSampleSongMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                OpenSampleSong();
            }
        });

        // Event = Close
        closeMenu =
            getMenuItem(filemenu, closeMenu, "Close", KeyEvent.VK_C, KeyEvent.VK_C,
                        ActionEvent.CTRL_MASK);
        closeMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Close();
            }
        });

        // Event = SaveImages
        saveMenu =
            getMenuItem(filemenu, saveMenu, "Save As Images...", KeyEvent.VK_S, KeyEvent.VK_S,
                        ActionEvent.CTRL_MASK);
        saveMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                SaveImages();
            }
        });

        // Event = PrintPreview
        previewMenu = getMenuItem(filemenu, previewMenu, "Print Preview...");
        previewMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                PrintPreview(player.getZoomValue());
            }
        });

        // Event = Exit
        exitMenu = getMenuItem(filemenu, exitMenu, "Exit");
        exitMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Exit();
            }
        });

        menubar.add(filemenu);
    }

    private static JMenuItem getMenuItem(JMenu filemenu, JMenuItem menu, String text)
    {
        return getMenuItem(filemenu, menu, text, -1, -1, -1);
    }

    /** Create the Color menu */
    void CreateColorMenu()
    {
        colorMenu = new JMenu("Color");
        colorMenu.setMnemonic(KeyEvent.VK_C);

        // Event = UseColor
        colorKeysMenu = new JCheckBoxMenuItem("Color keys");
        SettingProperties prop = SettingProperties.getInstance();
        colorKeysMenu.setSelected(prop.getBooleanProperty(Piano.KEYBOARD_COLORED, false));
        colorKeysMenu.setMnemonic(KeyEvent.VK_K);
        colorMenu.add(colorKeysMenu);
        colorKeysMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                colorKeys(colorKeysMenu);
            }
        });

        // Event = UseColor
        // useColorMenu = new JCheckBoxMenuItem("Use Color");
        // useColorMenu.setSelected(true);
        // useColorMenu.setMnemonic(KeyEvent.VK_U);
        // colorMenu.add(useColorMenu);
        // useColorMenu.addActionListener(new ActionListener()
        // {
        // @Override
        // public void actionPerformed(ActionEvent arg0)
        // {
        // UseColor(useColorMenu);
        // }
        // });
        //
        // // Event = ChooseColor
        // chooseColorMenu = getMenuItem(colorMenu, chooseColorMenu, "Choose
        // Colors...",
        // KeyEvent.VK_C);
        // chooseColorMenu.addActionListener(new ActionListener()
        // {
        // @Override
        // public void actionPerformed(ActionEvent arg0)
        // {
        // ChooseColor();
        // }
        // });

        menubar.add(colorMenu);
    }

    /* Create the "Select Tracks to Display" menu. */
    void CreateTrackDisplayMenu()
    {
        final JMenuItem menu;
        trackDisplayMenu = new JMenu("Select Tracks to Display");
        trackMenu.add(trackDisplayMenu);

        // Event = SelectAllTracks
        menu = getMenuItem(trackDisplayMenu, "Select All Tracks");
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                SelectAllTracks();
            }
        });

        // Event = DeselectAllTracks
        final JMenuItem menu2 = getMenuItem(trackDisplayMenu, "Deselect All Tracks");
        menu2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                DeselectAllTracks();
            }
        });

        for (int i = 0; i < midifile.getTracks().size(); i++)
        {
            int num = i + 1;
            String name = midifile.getTracks().get(i).getInstrumentName();
            if (!name.equals(""))
            {
                name = "   (" + name + ")";
            }

            // Event = TrackSelect
            final JCheckBoxMenuItem menu3 = new JCheckBoxMenuItem("Track " + num + name);
            if (midifile.getTracks().get(i).getInstrumentName().equals("Percussion"))
            {
                menu3.setSelected(false); // Disable percussion by default
            }
            else
            {
                menu3.setSelected(true);
            }
            menu3.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    TrackSelect(menu3);
                }
            });

            trackDisplayMenu.add(menu3);
        }
    }

    /* Create the "Select Tracks to Mute" menu. */
    void CreateTrackMuteMenu()
    {
        final JMenuItem menu;
        trackMuteMenu = new JMenu("Select Tracks to Mute");
        trackMenu.add(trackMuteMenu);

        // Event = MuteAllTracks
        menu = getMenuItem(trackMuteMenu, "Mute All Tracks");
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                MuteAllTracks();
            }
        });

        // Event = UnmuteAllTracks
        JMenuItem menu2 = getMenuItem(trackMuteMenu, "Unmute All Tracks");
        menu2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                UnmuteAllTracks();
            }
        });

        for (int i = 0; i < midifile.getTracks().size(); i++)
        {
            int num = i + 1;
            String name = midifile.getTracks().get(i).getInstrumentName();
            if (!name.equals(""))
            {
                name = "   (" + name + ")";
            }

            // Event = TrackMute
            menu2 = new JCheckBoxMenuItem("Track " + num + name);
            if (midifile.getTracks().get(i).getInstrumentName().equals("Percussion"))
            {
                menu2.setSelected(true); // Disable percussion by default
            }
            else
            {
                menu2.setSelected(false);
            }
            menu2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    TrackMute(menu);
                }
            });

            trackMuteMenu.add(menu2);
        }
    }

    /**
     * Create the "Track" Menu after a Midi file has been selected. Add a menu item to
     * enable/disable displaying each track. Add a menu item to mute/unmute each track.
     *
     * The JMenuItem.Tag is the track number (starting at 0). Add a menu item to select one staff
     * per track. Add a menu item to combine all tracks into two staffs. Add a menu item to choose
     * track instruments.
     */
    void CreateTrackMenu()
    {
        trackMenu.removeAll();
        CreateTrackDisplayMenu();
        CreateTrackMuteMenu();
        trackMenu.addSeparator();

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        // Event = UseOneStaff
        oneStaffMenu = new JRadioButtonMenuItem("Show One Staff Per Track");
        group.add(oneStaffMenu);
        trackMenu.add(oneStaffMenu);
        oneStaffMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                UseOneStaff();
            }
        });

        // Event = UseTwoStaffs
        if (midifile.getTracks().size() == 1)
        {
            twoStaffMenu = new JRadioButtonMenuItem("Split Track Into Two Staffs");
            twoStaffMenu.setSelected(true);
            group.add(twoStaffMenu);
            twoStaffMenu.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    UseTwoStaffs();
                }
            });
            trackMenu.add(twoStaffMenu);
        }
        else
        {
            twoStaffMenu = new JRadioButtonMenuItem("Combine All Tracks Into Two Staffs");
            oneStaffMenu.setSelected(true);
            group.add(twoStaffMenu);
            twoStaffMenu.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    UseTwoStaffs();
                }
            });
            trackMenu.add(twoStaffMenu);
        }

        trackMenu.addSeparator();

        // Event = ChooseInstruments
        JMenuItem menu = getMenuItem(trackMenu, chooseInstrumentsMenu, "Choose Instruments...");
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ChooseInstruments();
            }
        });
    }

    /** Enable the "Notes" menu after a Midi file has been selected. */
    void CreateNotesMenu()
    {
        try
        {
            notesMenu.removeAll();
            CreateKeySignatureMenu();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        CreateTimeSignatureMenu();
        CreateTransposeMenu();
        CreateShiftNoteMenu();
        CreateMeasureLengthMenu();
        CreateCombineNotesMenu();
        CreateShowLettersMenu();
        CreateShowLyricsMenu();
        CreateShowMeasuresMenu();
        CreatePlayMeasuresMenu();
    }

    /**
     * Create the "Key Signature" sub-menu. Create the sub-menus for changing the key signature. The
     * Menu.Tag contains the number of sharps (if positive) or the number of flats (if negative) :
     * the key.
     *
     * @throws Exception
     */
    void CreateKeySignatureMenu() throws Exception
    {
        final JRadioButtonMenuItemExt menu;
        KeySignature key;

        changeKeyMenu = new JMenu("Key Signature");
        changeKeyMenu.setMnemonic(KeyEvent.VK_K);

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        // Event = ChangeKeySignature
        menu = new JRadioButtonMenuItemExt("Default");
        menu.setSelected(true);
        group.add(menu);
        changeKeyMenu.add(menu);
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ChangeKeySignature(menu);
            }
        });

        /* Add the sharp key signatures */
        for (int sharps = 0; sharps <= 5; sharps++)
        {
            key = new KeySignature(sharps, 0, sheetmusic.getSizes());

            // Event = ChangeKeySignature
            final JRadioButtonMenuItemExt menu2 =
                new JRadioButtonMenuItemExt(key.toString(), sharps);
            group.add(menu2);
            changeKeyMenu.add(menu2);
            menu2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    ChangeKeySignature(menu2);
                }
            });

            changeKeyMenu.add(menu);
        }

        /* Add the flat key signatures */
        for (int flats = 1; flats <= 6; flats++)
        {
            key = new KeySignature(0, flats, sheetmusic.getSizes());

            // Event = ChangeKeySignature
            final JRadioButtonMenuItemExt menu3 =
                new JRadioButtonMenuItemExt(key.toString(), -flats);
            group.add(menu3);
            changeKeyMenu.add(menu3);
            menu3.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    ChangeKeySignature(menu3);
                }
            });
        }
        notesMenu.add(changeKeyMenu);
    }

    /**
     * Create the Time Signature Menu. : addition to the default time signature, add 3/4 and 4/4
     */
    void CreateTimeSignatureMenu()
    {

        timeSigMenu = new JMenu("Time Signature");
        timeSigMenu.setMnemonic(KeyEvent.VK_I);

        String defText = " (default)";
        String text1 = "3/4";
        String text2 = "4/4";
        String text3 = "???";
        boolean checked1 = false;
        boolean checked2 = false;
        boolean checked3 = false;

        if (midifile.getTime().getNumerator() == 3 && midifile.getTime().getDenominator() == 4)
        {
            text1 += defText;
            checked1 = true;
        }
        else if (midifile.getTime().getNumerator() == 4 && midifile.getTime().getDenominator() == 4)
        {
            text2 += defText;
            checked2 = true;
        }
        else
        {
            // Event = ChangeTimeSignature (default) if exists
            text3 =
                midifile.getTime().getNumerator() + "/" + midifile.getTime().getDenominator() +
                    defText;
            checked3 = true;
        }

        // a group of radio button menu items
        ButtonGroup group2 = new ButtonGroup();

        // Event = ChangeTimeSignature 3/4
        final JRadioButtonMenuItem menu1 = new JRadioButtonMenuItem(text1);
        menu1.setSelected(checked1);
        group2.add(menu1);
        timeSigMenu.add(menu1);
        menu1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ChangeTimeSignature(menu1);
            }
        });

        // Event = ChangeTimeSignature 4/4
        final JRadioButtonMenuItem menu2 = new JRadioButtonMenuItem(text2);
        menu2.setSelected(checked2);
        group2.add(menu2);
        timeSigMenu.add(menu2);
        menu2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ChangeTimeSignature(menu2);
            }
        });

        // Event = ChangeTimeSignature (third choice
        final JRadioButtonMenuItem menu3 = new JRadioButtonMenuItem(text3);
        menu3.setSelected(checked3);
        group2.add(menu3);
        timeSigMenu.add(menu3);
        menu3.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ChangeTimeSignature(menu3);
            }
        });

        notesMenu.add(timeSigMenu);

    }

    /**
     * Create the "Transpose" sub-menu. Create sub-menus for moving the key up or down. The Menu.Tag
     * contains the amount to shift the key by.
     */
    void CreateTransposeMenu()
    {

        transposeMenu = new JMenu("Transpose");
        transposeMenu.setMnemonic(KeyEvent.VK_T);
        int[] amounts = new int[]
        { 12, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -12 };

        // a group of radio button menu items
        ButtonGroup group2 = new ButtonGroup();

        for (int amount : amounts)
        {
            String name = "none";
            if (amount > 0)
                name = "Up " + amount;
            else if (amount < 0)
                name = "Down " + (-amount);

            // Event = Transpose
            final JRadioButtonMenuItemExt menu = new JRadioButtonMenuItemExt(name, amount);
            menu.setSelected(true);
            group2.add(menu);
            menu.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    Transpose(menu);
                }
            });

            if (amount == 0)
                menu.setSelected(true);
            else
                menu.setSelected(false);
            transposeMenu.add(menu);
        }
        notesMenu.add(transposeMenu);
    }

    /**
     * Create the "Shift Note" sub-menu. For the "Left to Start" sub-menu, the Menu.Tag contains the
     * time (in pulses) where the first note occurs. For the "Right" sub-menus, the Menu.Tag
     * contains the number of eighth notes to shift right by.
     */
    void CreateShiftNoteMenu()
    {
        final JRadioButtonMenuItemExt menu;
        shiftNotesMenu = new JMenu("Shift Notes");

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        long firsttime = midifile.getTime().getMeasure() * 10;
        for (MidiTrack t : midifile.getTracks())
        {
            if (firsttime > t.getNotes().get(0).getStartTime())
            {
                firsttime = t.getNotes().get(0).getStartTime();
            }
        }

        // Event = ShiftTime
        menu = new JRadioButtonMenuItemExt("Left to start", -firsttime);
        menu.setSelected(false);
        group.add(menu);
        shiftNotesMenu.add(menu);
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShiftTime(menu);
            }
        });

        String[] labels = new String[]
        { "none (default)", "Right 1/8 note", "Right 1/4 note", "Right 3/8 note", "Right 1/2 note",
            "Right 5/8 note", "Right 3/4 note", "Right 7/8 note" };
        for (int i = 0; i < 8; i++)
        {
            // Event = ShiftTime
            final JRadioButtonMenuItemExt menu3 = new JRadioButtonMenuItemExt(labels[i], i);
            group.add(menu3);
            shiftNotesMenu.add(menu3);
            if (i == 0)
            {
                menu3.setSelected(true);
            }
            else
            {
                menu3.setSelected(false);
            }

            menu3.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    ShiftTime(menu3);
                }
            });
        }
        notesMenu.add(shiftNotesMenu);
    }

    /**
     * Create the Measure Length sub-menu. The method MidiFile.GuessMeasureLength guesses possible
     * values for the measure length (in pulses). Create a sub-menu for each possible measure
     * length. The Menu.Tag field contains the measure length (in pulses) for each menu item.
     */
    void CreateMeasureLengthMenu()
    {
        final JRadioButtonMenuItemExt menu;

        measureMenu =
            getMenuWithSubMenuItem(notesMenu, measureMenu, "Measure Length", KeyEvent.VK_M);

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        // Event = MeasureLength
        menu =
            new JRadioButtonMenuItemExt(midifile.getTime().getMeasure() + " pulses (default)",
                midifile.getTime().getMeasure());
        menu.setSelected(true);
        group.add(menu);
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                MeasureLength(menu);
            }
        });
        measureMenu.add(menu);
        measureMenu.addSeparator();
        List<Long> lengths = midifile.GuessMeasureLength();
        for (long len : lengths)
        {
            // Event = MeasureLength
            final JRadioButtonMenuItemExt menu2 =
                new JRadioButtonMenuItemExt(len + " pulses ", len);
            measureMenu.add(menu2);
            menu2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    MeasureLength(menu2);
                }
            });
        }
        notesMenu.add(measureMenu);
    }

    /**
     * Create the Combine Notes Within Interval sub-menu. The method MidiFile.RoundStartTimes() is
     * used to combine notes within a given time interval (millisec) into the same chord. The
     * Menu.Tag field contains the millisecond value.
     */
    void CreateCombineNotesMenu()
    {
        combineNotesMenu =
            getMenuWithSubMenuItem(notesMenu, combineNotesMenu, "Combine Notes Within Interval",
                                   KeyEvent.VK_C);

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        for (int millisec = 20; millisec <= 100; millisec += 20)
        {
            if (millisec == 40)
            {
                // Event = CombineNotes
                final JRadioButtonMenuItemExt menu =
                    new JRadioButtonMenuItemExt(millisec + " milliseconds (default)", millisec);
                menu.setSelected(true);
                menu.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent arg0)
                    {
                        CombineNotes(menu);
                    }
                });
                group.add(menu);
                combineNotesMenu.add(menu);
            }
            else
            {
                // Event = CombineNotes
                final JRadioButtonMenuItemExt menu =
                    new JRadioButtonMenuItemExt(millisec + " milliseconds", millisec);
                menu.addActionListener(new ActionListener()
                {

                    @Override
                    public void actionPerformed(ActionEvent arg0)
                    {
                        CombineNotes(menu);
                    }

                });
                group.add(menu);
                combineNotesMenu.add(menu);
            }
        }
        notesMenu.add(combineNotesMenu);
    }

    /** Create the "Show Note Letters" sub-menu. */
    void CreateShowLettersMenu()
    {
        final JRadioButtonMenuItemExt menu;
        showLettersMenu = new JMenu("Show Note Letters");

        // a group of radio button menu items
        ButtonGroup group = new ButtonGroup();

        // Event = ShowNoteLetters
        menu = new JRadioButtonMenuItemExt("None", 0);
        menu.setSelected(true);
        group.add(menu);
        showLettersMenu.add(menu);
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu);
            }
        });

        // Event = ShowNoteLetters
        final JRadioButtonMenuItemExt menu2 =
            new JRadioButtonMenuItemExt("Letters", MidiOptions.NoteNameLetter);
        group.add(menu2);
        showLettersMenu.add(menu2);
        menu2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu2);
            }
        });

        // Event = ShowNoteLetters
        final JRadioButtonMenuItemExt menu3 =
            new JRadioButtonMenuItemExt("Fixed Do-Re-Mi", MidiOptions.NoteNameFixedDoReMi);
        group.add(menu3);
        showLettersMenu.add(menu3);
        menu3.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu3);
            }
        });

        // Event = ShowNoteLetters
        final JRadioButtonMenuItemExt menu4 =
            new JRadioButtonMenuItemExt("Movable Do-Re-Mi", MidiOptions.NoteNameMovableDoReMi);
        group.add(menu4);
        showLettersMenu.add(menu4);
        menu4.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu4);
            }
        });

        // Event = ShowNoteLetters
        final JRadioButtonMenuItemExt menu5 =
            new JRadioButtonMenuItemExt("Fixed Numbers", MidiOptions.NoteNameFixedNumber);
        group.add(menu5);
        showLettersMenu.add(menu5);
        menu5.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu5);
            }
        });

        // Event = ShowNoteLetters
        final JRadioButtonMenuItemExt menu6 =
            new JRadioButtonMenuItemExt("Movable Numbers", MidiOptions.NoteNameMovableNumber);
        group.add(menu6);
        showLettersMenu.add(menu6);
        menu6.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowNoteLetters(menu6);
            }
        });

        notesMenu.add(showLettersMenu);
    }

    /** Create the "Show Measure Numbers" sub-menu. */
    void CreateShowMeasuresMenu()
    {
        // Event = ShowMeasures
        showMeasuresMenu = new JCheckBoxMenuItem("Show Measure Numbers");
        showMeasuresMenu.setSelected(false);
        notesMenu.add(showMeasuresMenu);
        showMeasuresMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowMeasures(showMeasuresMenu);
            }
        });
    }

    /**
     * Create the "Play Measures : a Loop" sub-menu.
     *
     */
    void CreatePlayMeasuresMenu()
    {
        // Event = PlayMeasuresInLoop
        playMeasuresMenu = new JCheckBoxMenuItem("Play Measures in a Loop...");
        playMeasuresMenu.setSelected(false);
        notesMenu.add(playMeasuresMenu);
        playMeasuresMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                PlayMeasuresInLoop(playMeasuresMenu);
            }
        });
    }

    /** Create the "Show Lyrics" sub-menu. */
    void CreateShowLyricsMenu()
    {
        if (!midifile.HasLyrics())
        {
            showLyricsMenu = null;
            return;
        }
        // Event = ShowLyrics
        showLyricsMenu = new JCheckBoxMenuItem("Show Lyrics");
        showLyricsMenu.setSelected(true);
        notesMenu.add(showLyricsMenu);
        showLyricsMenu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                ShowLyrics(showLyricsMenu);
            }
        });
    }

    /** Create the Help menu */
    void CreateHelpMenu()
    {
        JMenu helpmenu = new JMenu("Help");
        helpmenu.setMnemonic(KeyEvent.VK_H);

        // Event = About
        JMenuItem menu = getMenuItem(helpmenu, "About...", KeyEvent.VK_A);
        menu.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                About();
            }
        });

        // Event = Help
        menu = getMenuItem(helpmenu, "Help Contents...", KeyEvent.VK_H);
        menu.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Help();
            }
        });

        menubar.add(helpmenu);
    }

    /** Initialize the Printer settings */
    void CreatePrintSettings()
    {
        // printdoc = new PrintDocument();
        // printdoc.PrinterSettings.FromPage = 1;
        // printdoc.PrinterSettings.ToPage = printdoc.PrinterSettings.MaximumPage;
        // printdoc.PrintPage += new PrintPageEventHandler(PrintPage);
        // toPage = 0;
        // currentpage = 1;
    }

    /**
     * The callback function for the "Open..." menu. Display a "File Open" dialog, to select a midi
     * filename. If a file is selected, call OpenMidiFile()
     */
    void Open()
    {
        // Create a file chooser
        final JFileChooser dialog = new JFileChooser();
        SettingProperties prop = SettingProperties.getInstance();
        String path = prop.getProperty(MIDI_PATH);
        if (path != null)
        {
            dialog.setCurrentDirectory(new File(path));
        }

        // Add a custom file filter Midi Files (*.mid)|*.mid*|All Files (*.*)|*.*
        dialog.addChoosableFileFilter(new MidiFileFilter());

        // Show it.
        int returnVal = dialog.showDialog(this, "Select");

        // Process the results.
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = dialog.getSelectedFile();
            prop.setProperty(MIDI_PATH, file.getParent());
            OpenMidiFile(file.getAbsolutePath());
            javax.swing.SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    scrollView.getVerticalScrollBar().setValue(1);
                    scrollView.getVerticalScrollBar().setValue(0);
                }
            });
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Action cancelled by user.", null,
                                          JOptionPane.INFORMATION_MESSAGE);
        }

    }

    /**
     * The callback function for the "Open Sample Song..." menu. Create a SampleSongDialog. If a
     * song is chosen, read the resource file, and save it to an actual file : the temp directory.
     * Then call OpenMidiFile() using that temp filename.
     */
    void OpenSampleSong()
    {
        if (sampleSongDialog == null)
        {
            sampleSongDialog = new SampleSongDialog();
        }

        if (sampleSongDialog.ShowDialog() == true)
        {
            String name = sampleSongDialog.GetSong();
            String path = "songs/" + name + ".mid";
            OpenMidiFile(new File(path).getAbsolutePath());
        }
    }

    /**
     * Read the midi file into a MidiFile instance. Create a SheetMusic control based on the
     * MidiFile. Add the sheetmusic control to this form. Enable all the menu items.
     *
     * If any error occurs while reading the midi file, display a MessageBox with the error message.
     *
     * @throws Exception
     */
    public void OpenMidiFile(String filename) // throws Exception
    {
        filename= filename.replace("\"","");
        iFilename = filename;
        try
        {
            MidiPlayer.resetInstance();
            midifile = new MidiFileJavax(filename);
            EnableMenus(false);
            EnableMenus(true);
            File file = new File(filename);
            iSongTitle = file.getName();
            iSongTitle = iSongTitle.replace("__", ": ");
            iSongTitle = iSongTitle.replace("_", " ");
            iSongTitle = iSongTitle.replace(".mid", "");
            setTitle(iSongTitle + " - JMidi Sheet Music");
            player.setTempo(100);
            instrumentDialog = new InstrumentDialog(midifile);
            playMeasuresDialog = new PlayMeasuresDialog(midifile);
            RedrawSheetMusic(iSongTitle);
        }
        catch (MidiFileException e)
        {
            String message = "";
            message += "MidiSheetMusic was unable to open the file " + filename;
            message += "\nIt does not appear to be a valid midi file.\n" + e.getMessage();
            JOptionPane.showMessageDialog(this, message, "Error Opening File",
                                          JOptionPane.ERROR_MESSAGE);
            midifile = null;
            EnableMenus(false);
        }
        catch (IOException e)
        {
            String message = "";
            message += "MidiSheetMusic was unable to open the file " + filename;
            message += "because:\n" + e.getMessage() + "\n";
            JOptionPane.showMessageDialog(this, message, "Error Opening File",
                                          JOptionPane.ERROR_MESSAGE);

            midifile = null;
            EnableMenus(false);
        }
        catch (InvalidMidiDataException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * The callback function for the "Close" menu. When invoked this will - Dispose of the
     * SheetMusic Control - Disable the relevant menu items.
     */
    void Close()
    {
        if (sheetmusic == null)
        {
            return;
        }
        player.SetMidiFile(null, null, null);
        sheetmusic.setVisible(false);
        sheetmusic = null;
        EnableMenus(false);
        setBackground(Color.GRAY);
        scrollView.setBackground(Color.GRAY);
        setTitle("Midi Sheet Music");
        instrumentDialog.Dispose();
        playMeasuresDialog.setVisible(false);
    }

    /**
     * The callback function for the "Save As Images" menu. When invoked this will save the sheet
     * music as several images, one per page. For each page : the sheet music: - Create a new
     * bitmap, PageWidth by PageHeight - Create a Graphics object for the bitmap - Call the
     * SheetMusic.DoPrint() method to draw the music onto the bitmap - Save the bitmap to the file.
     */
    void SaveImages()
    {
        if (sheetmusic == null)
        {
            return;
        }
        /* Stop the player so that no notes are shaded */
        player.Stop();

        // /* We can only save sheet music : 'vertical scrolling' view */
        // ScrollVertically();

        int numpages = sheetmusic.GetTotalPages();
        final JFileChooser dialog = new JFileChooser();
        SettingProperties prop = SettingProperties.getInstance();
        String path = prop.getProperty(IMAGE_PATH);
        if (path != null)
        {
            dialog.setCurrentDirectory(new File(path));
        }

        // Add a custom file filter Midi Files (*.mid)|*.mid*|All Files (*.*)|*.*
        dialog.addChoosableFileFilter(new PngFilter());

        // Show it.
        int returnVal = dialog.showDialog(this, "Select");

        /* The initial filename : the dialog will be <midi filename>.png */
        String initname = midifile.getFileName();
        initname = initname.replace(".mid", "") + ".png";

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = dialog.getSelectedFile();
            prop.setProperty(IMAGE_PATH, file.getParent());
            String filename = file.getName();
            if (filename.substring(filename.length() - 4, 4).equals(".png"))
            {
                filename = filename.substring(0, filename.length() - 4);
            }
            sheetmusic.saveMeToGIF(filename);
        }
    }

    /**
     * The callback function for the "Print Preview" menu. When invoked, this will spawn a
     * PrintPreview dialog. The dialog will then invoke the PrintPage() event handler to actually
     * render the pages.
     *
     * @param zoomValue
     */
    void PrintPreview(int zoomValue)
    {
        /* Stop the player so that no notes are shaded */
        player.Stop();

        // Scroll to suitable size
        iLastZoomValue = zoomValue;
        sheetmusic.changeZoom(2.3);
        setScrollWindowSize();

        currentpage = 1;
        PrintPreview pp = new PrintPreview(sheetmusic.getPageDivider());
        pp.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                sheetmusic.changeZoom(iLastZoomValue);
            }
        });
        pp.setVisible(true);
    }

    /**
     * The callback function for the "Print..." menu. When invoked, this will spawn a Print dialog.
     * The dialog will then invoke the PrintPage() event handler to actually render the pages.
     */
    void Print()
    {
        /* Stop the player so that no notes are shaded */
        player.Stop();

        for (SheetMusicPage page : sheetmusic.getPageDivider().getPages())
        {
            PrintUtilities.printComponent(page);
        }

    }

    /**
     * The callback function for the "Exit" menu. Exit the application.
     */
    void Exit()
    {
        player.Stop();
        player.DeleteSoundFile();
        this.dispose();
        System.exit(0);
    }

    /**
     * The callback function for the "Track <num>" menu items. Update the checked status of the menu
     * item. Then, redraw the sheetmusic.
     *
     * @param menu
     */
    void TrackSelect(JMenuItem menu)
    {
        reInitiateOptions();
    }

    /**
     * The callback function for "Select All Tracks" menu item. Check all the tracks. Then redraw
     * the sheetmusic.
     */
    void SelectAllTracks()
    {
        int i;
        for (i = 0; i < midifile.getTracks().size(); i++)
        {
            ((JCheckBoxMenuItem) trackDisplayMenu.getMenuComponent(i + 2)).setSelected(true);
        }
        reInitiateOptions();
    }

    /**
     * The callback function for "Deselect All Tracks" menu item. Uncheck all the tracks. Then
     * redraw the sheetmusic.
     */
    void DeselectAllTracks()
    {
        int i;
        for (i = 0; i < midifile.getTracks().size(); i++)
        {
            ((JCheckBoxMenuItem) trackDisplayMenu.getMenuComponent(i + 2)).setSelected(false);
        }
        reInitiateOptions();
    }

    /**
     * The callback function for the "Mute Track <num>" menu items. Update the checked status of the
     * menu item. Then, redraw the sheetmusic.
     */
    void TrackMute(JMenuItem menu)
    {
        reInitiateOptions();
    }

    /**
     * The callback function for "Mute All Tracks" menu item. Check all the tracks. Then redraw the
     * sheetmusic.
     */
    void MuteAllTracks()
    {
        int i;
        for (i = 0; i < midifile.getTracks().size(); i++)
        {
            ((JCheckBoxMenuItem) trackMuteMenu.getMenuComponent(i + 2)).setSelected(true);
        }
        reInitiateOptions();
    }

    /**
     * The callback function for "Unmute All Tracks" menu item. Uncheck all the tracks. Then redraw
     * the sheetmusic.
     */
    void UnmuteAllTracks()
    {
        int i;
        for (i = 0; i < midifile.getTracks().size(); i++)
        {
            ((JCheckBoxMenuItem) trackMuteMenu.getMenuComponent(i + 2)).setSelected(false);
        }
        reInitiateOptions();
    }

    /**
     * The callback function for the "One Staff per Track" menu. Update the checked status of the
     * menu items, and redraw the sheet music.
     */
    void UseOneStaff()
    {
        oneStaffMenu.setSelected(true);
        twoStaffMenu.setSelected(false);
        reInitiateOptions();
    }

    /**
     * The callback function for the "Combine/Split Into Two Staffs" menu. Update the checked status
     * of the menu item, and then redraw the sheet music.
     */
    void UseTwoStaffs()
    {
        twoStaffMenu.setSelected(true);
        oneStaffMenu.setSelected(false);
        reInitiateOptions();
    }

    /** The callback function for the "Show Note Letters" menu. */
    void ShowNoteLetters(JRadioButtonMenuItemExt menu)
    {
        for (int i = 0; i < 6; i++)
        {
            ((JRadioButtonMenuItemExt) showLettersMenu.getMenuComponent(i)).setSelected(false);
        }
        menu.setSelected(true);
        reInitiateOptions();
        try
        {
            piano.setShowNoteLetters(GetMidiOptions());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void reInitiateOptions()
    {
        try
        {
            sheetmusic.init((String) null, GetMidiOptions());
            sheetmusic.repaint();
            // invalidate();
            // validate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return;
    }

    /** The callback function for the "Show Lyrics" menu. */
    void ShowLyrics(JMenuItem menu)
    {
        reInitiateOptions();
    }

    /** The callback function for the "Show Measure Numbers" menu. */
    void ShowMeasures(JMenuItem menu)
    {
        reInitiateOptions();
    }

    /** The callback function for the "Key Signature" menu. */
    void ChangeKeySignature(JRadioButtonMenuItemExt menu)
    {
        if (menu.isSelected())
            return;

        for (Component othermenu : changeKeyMenu.getMenuComponents())
        {
            ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
        }
        menu.setSelected(true);
        reInitiateOptions();
    }

    /** The callback function for the "Transpose" menu. */
    void Transpose(JRadioButtonMenuItemExt menu)
    {
        if (menu.isSelected())
            return;

        for (Component othermenu : transposeMenu.getMenuComponents())
        {
            ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
        }
        menu.setSelected(true);
        reInitiateOptions();
    }

    /** The callback function for the "Shift Notes" menu. */
    void ShiftTime(JRadioButtonMenuItemExt menu)
    {

        for (Component othermenu : shiftNotesMenu.getMenuComponents())
        {
            ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
        }
        menu.setSelected(true);
        reInitiateOptions();
    }

    /** The callback function for the "Time Signature" menu. */
    void ChangeTimeSignature(JMenuItem menu3)
    {
        for (Component othermenu : timeSigMenu.getMenuComponents())
        {
            if (othermenu instanceof JRadioButtonMenuItemExt)
            {
                ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
            }
        }
        menu3.setSelected(true);

        /*
         * The default measure length changes when we change the time signature.
         */
        int defaultmeasure;
        if (menu3.getText().equals("3/4"))
            defaultmeasure = 3 * midifile.getTime().getQuarter();
        else if (menu3.getText().equals("4/4"))
            defaultmeasure = 4 * midifile.getTime().getQuarter();
        else
            defaultmeasure = midifile.getTime().getMeasure();
        ((JRadioButtonMenuItemExt) measureMenu.getMenuComponent(0))
            .setText(defaultmeasure + " pulses (default))");
        ((JRadioButtonMenuItemExt) measureMenu.getMenuComponent(0)).setTag(defaultmeasure);

        reInitiateOptions();
    }

    /** The callback function for the "Measure Length" menu. */
    void MeasureLength(JRadioButtonMenuItemExt menu)
    {
        for (Component othermenu : measureMenu.getMenuComponents())
        {
            if (othermenu instanceof JRadioButtonMenuItemExt)
            {
                ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
            }
        }
        menu.setSelected(true);
        reInitiateOptions();
    }

    /** The callback function for the "Combine Notes Within Interval" menu. */
    void CombineNotes(JRadioButtonMenuItemExt menu)
    {

        for (Component othermenu : combineNotesMenu.getMenuComponents())
        {
            if (othermenu instanceof JRadioButtonMenuItemExt)
            {
                ((JRadioButtonMenuItemExt) othermenu).setSelected(false);
            }
        }
        menu.setSelected(true);
        reInitiateOptions();
    }

    /** The callback function for the "Use Color" menu. */
    void UseColor(JMenuItem menu)
    {
        reInitiateOptions();
    }

    /** The callback function for the "Use Color" menu. */
    void colorKeys(JMenuItem menu)
    {
        SettingProperties prop = SettingProperties.getInstance();
        prop.setProperty(Piano.KEYBOARD_COLORED, menu.isSelected());
        piano.repaint();
    }

    /** The callback function for the "Choose Colors" menu */
    void ChooseColor()
    {
        if (colordialog.ShowDialog() == true)
        {
            reInitiateOptions();
        }
    }

    /** The callback function for the "Choose Instruments" menu */
    void ChooseInstruments()
    {
        if (instrumentDialog.ShowDialog() == true)
        {
            try
            {
                // RedrawSheetMusic(new File(iFilename).getName());
                // midifile.ChangeSound(iFilename, GetMidiOptions());
                // OpenMidiFile(iFilename);

                changeInstrument(GetMidiOptions(), midifile);

                // player.SetMidiFile(midifile, GetMidiOptions(), sheetmusic);
                System.out.println("Instruments changed");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private void changeInstrument(MidiOptions options, MidiFileJavax midifile)
    {
        List<MidiTrack> midiTrack = midifile.getTracks();
        for (int i = 0; i < midiTrack.size(); i++)
        {
            MidiTrack track = midiTrack.get(i);
            track.setInstrument(options.instruments[i]);
        }

        Sequence sequence = MidiPlayer.getInstance().getSequence(iFilename);
        Track[] tracksArr = sequence.getTracks();

        Sequencer sequencer = MidiPlayer.getInstance().getSequencer();

        long pos = 0;
        if (sequencer != null)
        {
            pos = sequencer.getTickPosition();
        }

        for (int i = 0; i < tracksArr.length; i++)
        {
            Track track = tracksArr[i];
            boolean isFirst = true;

            for (int j = 0; j < track.size(); j++)
            {
                javax.sound.midi.MidiEvent event = track.get(j);
                MidiMessage message = event.getMessage();

                if (message instanceof ShortMessage)
                {
                    ShortMessage sm2 = (ShortMessage) message;
                    System.out.println(event.getTick() + ", " + sm2.getChannel() + ", " +
                        sm2.getCommand() + ", " + sm2.getData1() + ", " + sm2.getData2());

                    if (sm2.getCommand() == ShortMessage.PROGRAM_CHANGE)
                    {
                        try
                        {
                            sm2.setMessage(sm2.getCommand(), sm2.getChannel(),
                                           options.instruments[i], sm2.getData2());
                            if (isFirst)
                            {
                                track.add(new javax.sound.midi.MidiEvent(sm2, pos));
                                isFirst = false;
                            }
                        }
                        catch (InvalidMidiDataException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * The callback function for the "Play Measures : a Loop" menu
     *
     * @param playMeasuresMenu2
     */
    void PlayMeasuresInLoop(JCheckBoxMenuItem playMeasuresMenu2)
    {
        playMeasuresDialog.ShowDialog();
        playMeasuresMenu.setSelected(playMeasuresDialog.IsEnabled());
        try
        {
            player.SetMidiFile(midifile, GetMidiOptions(), sheetmusic);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * The callback function for the "About" menu. Display the About dialog.
     */
    void About()
    {
        final JDialog dialog = new JDialog();

        /* Create the dialog box */
        AppInfoInterface appInfo = AppInfo.getInstance();
        dialog.setTitle("About JMidi Sheet Music");
        Font font = dialog.getFont();
        if (font != null)
        {
            dialog.setFont(new Font(font.getFamily(), Font.PLAIN, (int) (font.getSize() * 1.4)));
        }

        dialog.setResizable(false);
        dialog.setModal(true);
        dialog.setLayout(null);

        JLabel box =
            ImageHelper.getJLabelWithImage(AppInfo.APPLICATION_ICON_PATH, 15, 20, 30, 30, null);
        dialog.add(box);

        JLabel name = new JLabel();
        name.setText("About " + appInfo.getApplicationName());
        dialog.add(name);
        name.setFont(new Font("Arial", Font.BOLD, 16));
        name.setBounds(100, 20, 180, 30);

        JLabel label = new JLabel();
        label.setText("<html>Version " + appInfo.getVersion() +
            "<br/>Copyright 2007-2012 Madhav Vaidyanathan<br/>Copyright 2012-" +
            Calendar.getInstance().get(Calendar.YEAR) +
            " Lars Svensson<br/>https://bitbucket.org/Oxvalley/jmidisheetmusic</html>");
        dialog.add(label);
        label.setBounds(15, 45, 250, 80);

        JButton ok = new JButton();
        ok.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                dialog.setVisible(false);
            }
        });
        ok.setText("OK");
        dialog.add(ok);
        ok.setBounds(135, 130, 60, 30);

        dialog.setSize(350, 200);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    /**
     * Callback function for the "Help Contents" Menu. Display the Help Dialog.
     */
    void Help()
    {
        JDialog dialog = new JDialog();
        ((java.awt.Frame) dialog.getOwner())
            .setIconImage(dialog.getToolkit().getImage("images/NotePair.ico"));
        dialog.setTitle("Midi Sheet Music Help Contents");
        // Create an RTF editor window
        RTFEditorKit rtf = new RTFEditorKit();
        final JEditorPane editor = new JEditorPane();
        editor.setEditorKit(rtf);
        editor.setBackground(Color.WHITE);

        // This text could be big so add a scroll pane
        JScrollPane scroller = new JScrollPane();
        scroller.getViewport().add(editor);
        dialog.add(scroller, BorderLayout.CENTER);

        // Load an RTF file into the editor
        FileInputStream fi = null;
        try
        {
            fi = new FileInputStream(RTFViewer.HELP_RTF);
            rtf.read(fi, editor.getDocument(), 0);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (fi != null)
                {
                    fi.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        dialog.setBackground(Color.WHITE);
        dialog.setSize(new Dimension(600, 500));
        dialog.setLocationRelativeTo(this);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                editor.scrollRectToVisible(new Rectangle(1, 1, 1, 1));
            }
        });
        dialog.setVisible(true);
    }

    /**
     * The function which handles Print events from the Print Preview and Print menus. Determine
     * which page we are printing, and call the SheetMusic.DoPrint() method. Check that the Page
     * settings are valid, and cancel the print job if they're not.
     *
     * @param insets
     */
    void PrintPage(Object obj)// , PrintPageEventArgs printevent)
    {
        if (sheetmusic == null)
        {
            return;
        }

        sheetmusic.DoPrint(getGraphics(), currentpage);
        currentpage++;
        if (currentpage > toPage)
        {
            currentpage = 1;
        }
        else
        {
            // printevent.HasMorePages = true;
        }
    }

    /**
     * Enable all menus once a MidiFile has been selected. Enable all top-level menus (file, color,
     * track, time, help) Enable all the File menus (close, save, print, preview) Create the track
     * menu Create the time menu For the "Track" menu Add a menu item for each track Add the one
     * staff/two staff menus For the "Notes" menu Add the time signature menu Add the measure length
     * menu Add the shift notes menu
     */

    /**
     * Disable certain menus if there is no MidiFile selected. For the Track menu, remove any
     * sub-menus under the Track menu.
     */

    void EnableMenus(boolean value)
    {
        // useColorMenu.setEnabled(value);
        // chooseColorMenu.setEnabled(value);
        trackMenu.setEnabled(value);
        notesMenu.setEnabled(value);

        closeMenu.setEnabled(value);
        saveMenu.setEnabled(value);
        previewMenu.setEnabled(value);

        if (value)
        {
            CreateTrackMenu();
            CreateNotesMenu();
        }
        else
        {
            notesMenu.removeAll();
        }

    }

    private static JMenuItem getMenuItem(JMenuItem filemenu,
                                         JMenuItem menu,
                                         String text,
                                         int keyEvent1)
    {
        return getMenuItem(filemenu, menu, text, keyEvent1, -1, -1);
    }

    private static JMenuItem getMenuItem(JMenuItem filemenu, String text, int keyEvent1)
    {
        return getMenuItem(filemenu, null, text, keyEvent1, -1, -1);
    }

    private static JMenuItem getMenuItem(JMenuItem filemenu, String text)
    {
        return getMenuItem(filemenu, null, text, -1, -1, -1);
    }

    private static JMenuItem getMenuItem(JMenuItem menu,
                                         JMenuItem menuItem2,
                                         String text,
                                         int keyEvent1,
                                         int keyEvent2,
                                         int actionEvent)
    {
        if (menu == null)
        {
            throw new RuntimeErrorException(new Error(), "Menu can not be null");
        }

        JMenuItem menuItem = menuItem2;

        if (keyEvent1 == -1)
        {
            menuItem = new JMenuItem(text);
        }
        else
        {
            menuItem = new JMenuItem(text, keyEvent1);
        }

        if (keyEvent2 != -1)
        {
            menuItem.setAccelerator(KeyStroke.getKeyStroke(keyEvent2, actionEvent));
        }

        menu.add(menuItem);
        return menuItem;
    }

    private static JMenu getMenuWithSubMenuItem(JMenuItem menu,
                                                JMenu menuItem2,
                                                String text,
                                                int keyEvent1)
    {
        if (menu == null)
        {
            throw new RuntimeErrorException(new Error(), "Menu can not be null");
        }

        JMenu menuItem = menuItem2;

        if (keyEvent1 == -1)
        {
            menuItem = new JMenu(text);
        }
        else
        {
            menuItem = new JMenu(text);
        }

        menu.add(menuItem);
        return menuItem;
    }

    @Override
    public void propertyChange(PropertyChangeEvent arg0)
    {
        if (scrollView != null)
        {
            // TODO If equals "Zoom"
            setScrollWindowSize();
        }
    }

}
