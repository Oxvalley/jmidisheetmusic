package core.midi.sheet.music;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

public class MidiPlayer
{
   private static MidiPlayer iMidiPlayer;
   private String iFileName;
   private Sequence sequence;
   private Sequencer sequencer = null;
   private int iTempoFactor;

   private MidiPlayer()
   {
   }

   public void init(String midiFileName, int tempoFactor, ControllerEventListener listener)
   {
      iFileName = midiFileName;
      iTempoFactor = tempoFactor;

      if (sequencer == null)
      {
         try
         {
            // Create a sequencer for the sequence
            sequencer = MidiSystem.getSequencer();
            sequencer.open();

            addActionListener(listener);
         }
         catch (MidiUnavailableException e)
         {
            e.printStackTrace();
         }
      }
   }

   public final void addActionListener(ControllerEventListener listener)
   {
      int[] arg1 = { 127 };
      sequencer.addControllerEventListener(listener, arg1);
   }

   public final Sequencer getSequencer()
   {
      return sequencer;
   }

   public final boolean isRunning()
   {
      return (sequencer != null && sequencer.isRunning());
   }

   public void startPlaying()
   {
      if (sequencer != null && !sequencer.isRunning())
      {
         try
         {
            // From file
            sequence = getSequence(iFileName);

            // From URL
            // Sequence sequence = MidiSystem.getSequence(new
            // URL("http://hostname/midiaudiofile"));

            sequencer.setSequence(sequence);
            sequencer.setTempoFactor((float) (iTempoFactor / 100.0));
            // sequencer.setTempoInMPQ(iTempo);

            // Start playing
            sequencer.setTickPosition(0);
            sequencer.start();
         }
         catch (InvalidMidiDataException e)
         {
         }
      }
   }

   public Sequence getSequence(String fileName)
   {
      if (sequence == null)
      {
         try
         {
            sequence = MidiSystem.getSequence(new File(fileName));
         }
         catch (InvalidMidiDataException e)
         {
            e.printStackTrace();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
      }

      return sequence;
   }

   public void continuePlaying()
   {
      if (sequencer != null && !sequencer.isRunning())
      {
         sequencer.start();
      }
   }

   public void stopPlaying()
   {
      if (sequencer != null && sequencer.isRunning())
      {
         sequencer.stop();
      }
   }

   public static MidiPlayer getInstance()
   {
      if (iMidiPlayer == null)
      {
         iMidiPlayer = new MidiPlayer();
      }
      return iMidiPlayer;
   }

   public void setTempoFactor(int value)
   {
      if (sequence != null)
      {
         iTempoFactor = value;
         sequencer.setTempoFactor((float) (iTempoFactor / 100.0));
      }
   }

   public static void resetInstance()
   {
      // iMidiPlayer = null;
      MidiPlayer inst = MidiPlayer.getInstance();
      // Sequencer seq = inst.getSequencer();
      inst.stopPlaying();
      inst.resetSequencer();

   }

   private void resetSequencer()
   {
      sequencer = null;
      sequence = null;
   }

}
