package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.sound.midi.InvalidMidiDataException;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import common.properties.SettingProperties;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

/**
 * @class SheetMusic
 *
 *        The SheetMusic Control is the main class for displaying the sheet
 *        music. The SheetMusic class has the following public methods:
 *
 *        SheetMusic() Create a new SheetMusic control from the given midi file
 *        and options.
 *
 *        DoPrint() Print a single page of sheet music.
 *
 *        GetTotalPages() Get the total number of sheet music pages.
 *
 *        OnPaint() Method called to draw the SheetMuisc
 *
 *        These public methods are called from the MidiSheetMusic Form Window.
 *
 */
public class SheetMusic extends JComponent
{

   /**
    *
    */
   private static final long serialVersionUID = 4602638605430958970L;
   /*
    * Measurements used when drawing. All measurements are in pixels. The values
    * depend on whether the menu 'Large Notes' or 'Small Notes' is selected.
    */
   // TODO font size may vary with pageWidth
   public static Font LetterFont = new Font("Arial", Font.BOLD, 8);
   /** The font for drawing the letters */

   private List<Staff> staffs;
   /** The array of staffs to display (from top to bottom) */
   private KeySignature mainkey;
   /** The main key signature */
   private int numtracks;
   /** The number of tracks */
   private String filename;
   /** The name of the midi file */
   private int showNoteLetters;
   /** Display the note letters */
   private Color[] NoteColors;
   /** The note colors to use */
   private Color shadeBrush;
   /** The brush for shading */
   private Color shade2Brush;
   /** The brush for shading left-hand piano */
   private Color pen;
   /** The black pen for drawing */
   private String iFilename;
   private MidiOptions iOptions;
   private MidiFileJavax iFile;
   private SheetMusicSizes iSizes;
   private PageDivider iPageDivider;
   private String iSongTitle;
   private long iCurrentPlayPosition;
   private ScrollerInterface iScroller;

   /**
    * Initialize the default note sizes.
    *
    * @param scroller
    * @param scroller
    */
   SheetMusic(ScrollerInterface scroller)
   {
      if (iSizes == null)
      {
         iSizes = new SheetMusicSizes();
      }

      resetPlayPosition();
      iScroller = scroller;
   }

   public boolean saveMeToGIF(String filename1)
   {
      try
      {
         File saveFile = new File(filename1);
         Image img = createImage(getWidth(), getHeight());
         Graphics g = img.getGraphics();
         paint(g);
         ImageIO.write(ImageHelper.toBufferedImage(img), "gif", saveFile);
         JOptionPane.showMessageDialog(null, "Image saved to " + saveFile.toString());
         g.dispose();
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
         return false;
      }
      return true;
   }

   /**
    * Create a new SheetMusic control, using the given parsed MidiFile. The
    * options can be null.
    *
    * @param scroller
    */
   public SheetMusic(MidiFileJavax file, MidiOptions options, ScrollerInterface scroller)
   {
      this(scroller);
      iFile = file;
      init(file, options);
   }

   /**
    * Create a new SheetMusic control, using the given midi filename. The
    * options can be null.
    *
    * @param scroller
    */
   public SheetMusic(String filename, MidiOptions options, ScrollerInterface scroller)
   {
      this(scroller);
      init(filename, options);
   }

   /**
    * Use null for both arguments to redraw without changes
    *
    * @param filename1
    * @param options
    */
   public void init(String filename1, MidiOptions options)
   {
      if (filename1 != null)
      {
         iFilename = filename1;
      }

      if (options != null)
      {
         iOptions = options;
      }

      MidiFileJavax file = null;
      try
      {
         if (filename1 == null && iFilename != null)
         {
            // Use old file
            file = new MidiFileJavax(iFilename);
         }
         else
         {
            if (filename1 == null)
            {
               file = iFile;
            }
            else
            {
               file = new MidiFileJavax(filename1);
            }

         }
      }
      catch (MidiFileException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (InvalidMidiDataException e)
      {
         e.printStackTrace();
      }

      init(file, iOptions);
   }

   /**
    * Create a new SheetMusic control. MidiFile is the parsed midi file to
    * display. SheetMusic Options are the menu options that were selected.
    *
    * - Apply all the Menu Options to the MidiFile tracks. - Calculate the key
    * signature - For each track, create a list of MusicSymbols (notes, rests,
    * bars, etc) - Vertically align the music symbols in all the tracks -
    * Partition the music notes into horizontal staffs
    */
   void init(MidiFileJavax file, MidiOptions options2)
   {
      MidiOptions options = options2;
      if (options == null)
      {
         options = new MidiOptions(file);
      }

      filename = file.getFileName();

      SettingProperties prop = SettingProperties.getInstance();
      boolean isColored = prop.getBooleanProperty(Piano.KEYBOARD_COLORED, false);
      ColoredKeyManager man = ColoredKeyManager.getInstance();
      Color[] colors = null;
      if (isColored)
      {
         colors = man.getColorsArray();
      }
      SetColors(colors, options.shadeColor, options.shade2Color);
      pen = Color.BLACK;

      List<MidiTrack> tracks = null;
      try
      {
         tracks = file.ChangeMidiNotes(options);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }

      if (tracks == null)
      {
         System.out.println("Can not read midi file. Must exit!");
         System.exit(1);
      }

      showNoteLetters = options.showNoteLetters;
      TimeSignature time = file.getTime();
      if (options.time != null)
      {
         time = options.time;
      }
      if (options.key == -1)
      {
         mainkey = GetKeySignature(tracks);
      }
      else
      {
         mainkey = new KeySignature(options.key);
      }

      numtracks = tracks.size();

      long lastStart = file.EndTime() + options.shifttime;

      /*
       * Create all the music symbols (notes, rests, vertical bars, and clef
       * changes). The symbols variable contains a list of music symbols for
       * each track. The list does not include the left-side Clef and key
       * signature symbols. Those can only be calculated when we create the
       * staffs.
       */
      List<MusicSymbol>[] symbols = new ArrayList[numtracks];
      for (int tracknum = 0; tracknum < numtracks; tracknum++)
      {
         MidiTrack track = tracks.get(tracknum);
         ClefMeasures clefs = new ClefMeasures(track.getNotes(), time.getMeasure());
         List<ChordSymbol> chords = CreateChords(track.getNotes(), mainkey, time, clefs);
         symbols[tracknum] = CreateSymbols(chords, clefs, time, lastStart);
      }

      List<LyricSymbol>[] lyrics = null;
      if (options.showLyrics)
      {
         lyrics = GetLyrics(tracks);
      }

      /* Vertically align the music symbols */
      SymbolWidths widths = new SymbolWidths(symbols, lyrics);
      AlignSymbols(symbols, widths);

      staffs = CreateStaffs(symbols, mainkey, options, time.getMeasure());
      CreateAllBeamedChords(symbols, time);
      if (lyrics != null)
      {
         AddLyricsToStaffs(staffs, lyrics);
      }

      /*
       * After making chord pairs, the stem directions can change, which affects
       * the staff height. Re-calculate the staff height.
       */
      for (Staff staff : staffs)
      {
         staff.CalculateHeight(iSizes);
      }

      setBackground(Color.WHITE);
      initPageDivider();
   }

   /** Get the best key signature given the midi notes in all the tracks. */
   private KeySignature GetKeySignature(List<MidiTrack> tracks)
   {
      List<Integer> notenums = new ArrayList<Integer>();
      for (MidiTrack track : tracks)
      {
         for (MidiNote note : track.getNotes())
         {
            notenums.add(note.getNoteNumber());
         }
      }
      return KeySignature.Guess(notenums, iSizes);
   }

   /**
    * Create the chord symbols for a single track.
    *
    * @param midinotes
    *           The Midinotes in the track.
    * @param key
    *           The Key Signature, for determining sharps/flats.
    * @param time
    *           The Time Signature, for determining the measures.
    * @param clefs
    *           The clefs to use for each measure.
    * @ret An array of ChordSymbols
    */
   private List<ChordSymbol> CreateChords(List<MidiNote> midinotes, KeySignature key,
         TimeSignature time, ClefMeasures clefs)
   {

      int i = 0;
      List<ChordSymbol> chords = new ArrayList<ChordSymbol>();
      List<MidiNote> notegroup = new ArrayList<MidiNote>(12);
      int len = midinotes.size();

      while (i < len)
      {

         long starttime = midinotes.get(i).getStartTime();
         Clef clef = clefs.GetClef(starttime);

         /*
          * Group all the midi notes with the same start time into the notes
          * list.
          */
         notegroup.clear();
         notegroup.add(midinotes.get(i));
         i++;
         while (i < len && midinotes.get(i).getStartTime() == starttime)
         {
            notegroup.add(midinotes.get(i));
            i++;
         }

         /*
          * Create a single chord from the group of midi notes with the same
          * start time.
          */
         ChordSymbol chord = null;
         try
         {
            chord = new ChordSymbol(notegroup, key, time, clef, this, iSizes);
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
         chords.add(chord);
      }

      return chords;
   }

   /**
    * Given the chord symbols for a track, create a new symbol list that
    * contains the chord symbols, vertical bars, rests, and clef changes. Return
    * a list of symbols (ChordSymbol, BarSymbol, RestSymbol, ClefSymbol)
    */
   private List<MusicSymbol> CreateSymbols(List<ChordSymbol> chords, ClefMeasures clefs,
         TimeSignature time, long lastStart)
   {

      List<MusicSymbol> symbols = new ArrayList<MusicSymbol>();
      symbols = AddBars(chords, time, lastStart);
      symbols = AddRests(symbols, time);
      symbols = AddClefChanges(symbols, clefs);

      return symbols;
   }

   /**
    * Add in the vertical bars delimiting measures. Also, add the time signature
    * symbols.
    */
   private List<MusicSymbol> AddBars(List<ChordSymbol> chords, TimeSignature time, long lastStart)
   {

      List<MusicSymbol> symbols = new ArrayList<MusicSymbol>();

      TimeSigSymbol timesig = new TimeSigSymbol(time.getNumerator(), time.getDenominator(), iSizes);
      symbols.add(timesig);

      /* The starttime of the beginning of the measure */
      int measuretime = 0;

      int i = 0;
      while (i < chords.size())
      {
         if (measuretime <= chords.get(i).getStartTime())
         {
            symbols.add(new BarSymbol(measuretime, iSizes));
            measuretime += time.getMeasure();
         }
         else
         {
            symbols.add(chords.get(i));
            i++;
         }
      }

      /* Keep adding bars until the last StartTime (the end of the song) */
      while (measuretime < lastStart)
      {
         symbols.add(new BarSymbol(measuretime, iSizes));
         measuretime += time.getMeasure();
      }

      /* Add the final vertical bar to the last measure */
      symbols.add(new BarSymbol(measuretime, iSizes));
      return symbols;
   }

   @Override
   public void print(Graphics g)
   {
      super.print(g);
   }

   /**
    * Add rest symbols between notes. All times below are measured in pulses.
    */
   private List<MusicSymbol> AddRests(List<MusicSymbol> symbols, TimeSignature time)
   {
      long prevtime = 0;

      List<MusicSymbol> result = new ArrayList<MusicSymbol>(symbols.size());

      for (MusicSymbol symbol : symbols)
      {
         long starttime = symbol.getStartTime();
         RestSymbol[] rests = GetRests(time, prevtime, starttime);
         if (rests != null)
         {
            for (RestSymbol r : rests)
            {
               result.add(r);
            }
         }

         result.add(symbol);

         /* Set prevtime to the end time of the last note/symbol. */
         if (symbol instanceof ChordSymbol)
         {
            ChordSymbol chord = (ChordSymbol) symbol;
            prevtime = Math.max(chord.getEndTime(), prevtime);
         }
         else
         {
            prevtime = Math.max(starttime, prevtime);
         }
      }
      return result;
   }

   /**
    * Return the rest symbols needed to fill the time interval between start and
    * end. If no rests are needed, return nil.
    */
   private RestSymbol[] GetRests(TimeSignature time, long prevtime, long starttime)
   {
      RestSymbol[] result;
      RestSymbol r1, r2;

      if (starttime - prevtime < 0)
         return null;

      NoteDuration dur = time.GetNoteDuration(starttime - prevtime);
      switch (dur)
      {
         case Whole:
         case Half:
         case Quarter:
         case Eighth:
            r1 = new RestSymbol(prevtime, dur, iSizes);
            result = new RestSymbol[] { r1 };
            return result;

         case DottedHalf:
            r1 = new RestSymbol(prevtime, NoteDuration.Half, iSizes);
            r2 = new RestSymbol(prevtime + time.getQuarter() * 2, NoteDuration.Quarter, iSizes);
            result = new RestSymbol[] { r1, r2 };
            return result;

         case DottedQuarter:
            r1 = new RestSymbol(prevtime, NoteDuration.Quarter, iSizes);
            r2 = new RestSymbol(prevtime + time.getQuarter(), NoteDuration.Eighth, iSizes);
            result = new RestSymbol[] { r1, r2 };
            return result;

         case DottedEighth:
            r1 = new RestSymbol(prevtime, NoteDuration.Eighth, iSizes);
            r2 = new RestSymbol(prevtime + time.getQuarter() / 2, NoteDuration.Sixteenth, iSizes);
            result = new RestSymbol[] { r1, r2 };
            return result;

         default:
            return null;
      }
   }

   /**
    * The current clef is always shown at the beginning of the staff, on the
    * left side. However, the clef can also change from measure to measure. When
    * it does, a Clef symbol must be shown to indicate the change in clef. This
    * function adds these Clef change symbols. This function does not add the
    * main Clef Symbol that begins each staff. That is done in the Staff()
    * contructor.
    */
   private List<MusicSymbol> AddClefChanges(List<MusicSymbol> symbols, ClefMeasures clefs)
   {

      List<MusicSymbol> result = new ArrayList<MusicSymbol>(symbols.size());
      Clef prevclef = clefs.GetClef(0);
      for (MusicSymbol symbol : symbols)
      {
         /* A BarSymbol indicates a new measure */
         if (symbol instanceof BarSymbol)
         {
            Clef clef = clefs.GetClef(symbol.getStartTime());
            if (clef != prevclef)
            {
               result.add(new ClefSymbol(clef, symbol.getStartTime() - 1, true, iSizes));
            }
            prevclef = clef;
         }
         result.add(symbol);
      }
      return result;
   }

   /**
    * Notes with the same start times in different staffs should be vertically
    * aligned. The SymbolWidths class is used to help vertically align symbols.
    *
    * First, each track should have a symbol for every starttime that appears in
    * the Midi File. If a track doesn't have a symbol for a particular
    * starttime, then add a "blank" symbol for that time.
    *
    * Next, make sure the symbols for each start time all have the same width,
    * across all tracks. The SymbolWidths class stores - The symbol width for
    * each starttime, for each track - The maximum symbol width for a given
    * starttime, across all tracks.
    *
    * The method SymbolWidths.GetExtraWidth() returns the extra width needed for
    * a track to match the maximum symbol width for a given starttime.
    */
   private static void AlignSymbols(List<MusicSymbol>[] allsymbols, SymbolWidths widths)
   {

      for (int track = 0; track < allsymbols.length; track++)
      {
         List<MusicSymbol> symbols = allsymbols[track];
         List<MusicSymbol> result = new ArrayList<MusicSymbol>();

         int i = 0;

         /*
          * If a track doesn't have a symbol for a starttime, add a blank
          * symbol.
          */
         for (long start : widths.getStartTimes())
         {

            /* BarSymbols are not included in the SymbolWidths calculations */
            while (i < symbols.size() && (symbols.get(i) instanceof BarSymbol)
                  && symbols.get(i).getStartTime() <= start)
            {
               result.add(symbols.get(i));
               i++;
            }

            if (i < symbols.size() && symbols.get(i).getStartTime() == start)
            {

               while (i < symbols.size() && symbols.get(i).getStartTime() == start)
               {

                  result.add(symbols.get(i));
                  i++;
               }
            }
            else
            {
               result.add(new BlankSymbol(start, 0));
            }
         }

         /*
          * For each starttime, increase the symbol width by
          * SymbolWidths.GetExtraWidth().
          */
         i = 0;
         while (i < result.size())
         {
            if (result.get(i) instanceof BarSymbol)
            {
               i++;
               continue;
            }
            long start = result.get(i).getStartTime();
            int extra = widths.GetExtraWidth(track, start);
            result.get(i).setWidth(result.get(i).getWidth() + extra);

            /* Skip all remaining symbols with the same starttime. */
            while (i < result.size() && result.get(i).getStartTime() == start)
            {
               i++;
            }
         }
         allsymbols[track] = result;
      }
   }

   /**
    * Find 2, 3, 4, or 6 chord symbols that occur consecutively (without any
    * rests or bars in between). There can be BlankSymbols in between.
    *
    * The startIndex is the index in the symbols to start looking from.
    *
    * Store the indexes of the consecutive chords in chordIndexes. Store the
    * horizontal distance (pixels) between the first and last chord. If we
    * failed to find consecutive chords, return false.
    */
   private static HorizontalDistance FindConsecutiveChords(List<MusicSymbol> symbols,
         int startIndex, int[] chordIndexes)
   // TODO horizDistance should be ref
   {

      int i = startIndex;
      int numChords = chordIndexes.length;

      while (true)
      {
         int horizDistance = 0;

         /* Find the starting chord */
         while (i < symbols.size() - numChords)
         {
            if (symbols.get(i) instanceof ChordSymbol)
            {
               ChordSymbol c = (ChordSymbol) symbols.get(i);
               if (c.getStem() != null)
               {
                  break;
               }
            }
            i++;
         }
         if (i >= symbols.size() - numChords)
         {
            chordIndexes[0] = -1;
            return new HorizontalDistance(false);
         }
         chordIndexes[0] = i;
         boolean foundChords = true;
         for (int chordIndex = 1; chordIndex < numChords; chordIndex++)
         {
            i++;
            int remaining = numChords - 1 - chordIndex;
            while ((i < symbols.size() - remaining) && (symbols.get(i) instanceof BlankSymbol))
            {
               horizDistance += symbols.get(i).getWidth();
               i++;
            }
            if (i >= symbols.size() - remaining)
            {
               return new HorizontalDistance(false);
            }
            if (!(symbols.get(i) instanceof ChordSymbol))
            {
               foundChords = false;
               break;
            }
            chordIndexes[chordIndex] = i;
            horizDistance += symbols.get(i).getWidth();
         }
         if (foundChords)
         {
            return new HorizontalDistance(true, horizDistance);
         }

         /* Else, start searching again from index i */
      }
   }

   /**
    * Connect chords of the same duration with a horizontal beam. numChords is
    * the number of chords per beam (2, 3, 4, or 6). if startBeat is true, the
    * first chord must start on a quarter note beat.
    */
   private static void CreateBeamedChords(List<MusicSymbol>[] allsymbols, TimeSignature time,
         int numChords, boolean startBeat)
   {
      int[] chordIndexes = new int[numChords];
      ChordSymbol[] chords = new ChordSymbol[numChords];

      for (List<MusicSymbol> symbols : allsymbols)
      {
         int startIndex = 0;
         while (true)
         {
            HorizontalDistance found = FindConsecutiveChords(symbols, startIndex, chordIndexes);

            if (!found.getRetVal())
            {
               break;
            }

            int horizDistance = found.getDistance();
            for (int i = 0; i < numChords; i++)
            {
               chords[i] = (ChordSymbol) symbols.get(chordIndexes[i]);
            }

            if (ChordSymbol.CanCreateBeam(chords, time, startBeat))
            {
               ChordSymbol.CreateBeam(chords, horizDistance);
               startIndex = chordIndexes[numChords - 1] + 1;
            }
            else
            {
               startIndex = chordIndexes[0] + 1;
            }

            /*
             * What is the value of startIndex here? If we created a beam, we
             * start after the last chord. If we failed to create a beam, we
             * start after the first chord.
             */
         }
      }
   }

   /**
    * Connect chords of the same duration with a horizontal beam.
    *
    * We create beams in the following order: - 6 connected 8th note chords, in
    * 3/4, 6/8, or 6/4 time - Triplets that start on quarter note beats - 3
    * connected chords that start on quarter note beats (12/8 time only) - 4
    * connected chords that start on quarter note beats (4/4 or 2/4 time only) -
    * 2 connected chords that start on quarter note beats - 2 connected chords
    * that start on any beat
    */
   private static void CreateAllBeamedChords(List<MusicSymbol>[] allsymbols, TimeSignature time)
   {
      if ((time.getNumerator() == 3 && time.getDenominator() == 4)
            || (time.getNumerator() == 6 && time.getDenominator() == 8)
            || (time.getNumerator() == 6 && time.getDenominator() == 4))
      {

         CreateBeamedChords(allsymbols, time, 6, true);
      }
      CreateBeamedChords(allsymbols, time, 3, true);
      CreateBeamedChords(allsymbols, time, 4, true);
      CreateBeamedChords(allsymbols, time, 2, true);
      CreateBeamedChords(allsymbols, time, 2, false);
   }

   /**
    * Given MusicSymbols for a track, create the staffs for that track. Each
    * Staff has a maxmimum width of PageWidth (800 pixels). Also, measures
    * should not span multiple Staffs.
    */
   private List<Staff> CreateStaffsForTrack(List<MusicSymbol> symbols, int measurelen,
         KeySignature key, MidiOptions options, int track, int totaltracks)
   {
      int keysigWidth = iSizes.KeySignatureWidth(key);
      int startindex = 0;
      List<Staff> thestaffs = new ArrayList<Staff>(symbols.size() / 50);

      while (startindex < symbols.size())
      {
         /*
          * startindex is the index of the first symbol in the staff. endindex
          * is the index of the last symbol in the staff.
          */
         int endindex = startindex;
         int width = keysigWidth;
         int maxwidth;

         maxwidth = iSizes.getNotePageWidth();

         while (endindex < symbols.size() && width + symbols.get(endindex).getWidth() < maxwidth)
         {
            width += symbols.get(endindex).getWidth();

            endindex++;
         }
         endindex--;

         /*
          * There's 3 possibilities at this point: 1. We have all the symbols in
          * the track. The endindex stays the same.
          *
          * 2. We have symbols for less than one measure. The endindex stays the
          * same.
          *
          * 3. We have symbols for 1 or more measures. Since measures cannot
          * span multiple staffs, we must make sure endindex does not occur in
          * the middle of a measure. We count backwards until we come to the end
          * of a measure.
          */

         if (endindex == symbols.size() - 1)
         {
            /* endindex stays the same */
         }
         else if (symbols.get(startindex).getStartTime()
               / measurelen == symbols.get(endindex).getStartTime() / measurelen)
         {
            /* endindex stays the same */
         }
         else
         {
            long endmeasure = symbols.get(endindex + 1).getStartTime() / measurelen;
            while (symbols.get(endindex).getStartTime() / measurelen == endmeasure)
            {
               endindex--;
            }
         }
         width = iSizes.getNotePageWidth();
         Staff staff = new Staff(symbols.subList(startindex, endindex + 1), key, options, track,
               totaltracks, iSizes);
         thestaffs.add(staff);
         startindex = endindex + 1;
      }
      return thestaffs;
   }

   /**
    * Given all the MusicSymbols for every track, create the staffs for the
    * sheet music. There are two parts to this:
    *
    * - Get the list of staffs for each track. The staffs will be stored in
    * trackstaffs as:
    *
    * trackstaffs.get(0] = { Staff0, Staff1, Staff2, ... } for track 0
    * trackstaffs.get(1] = { Staff0, Staff1, Staff2, ... } for track 1
    * trackstaffs.get(2] = { Staff0, Staff1, Staff2, ... } for track 2
    *
    * - Store the Staffs in the staffs list, but interleave the tracks as
    * follows:
    *
    * staffs = { Staff0 for track 0, Staff0 for track1, Staff0 for track2,
    * Staff1 for track 0, Staff1 for track1, Staff1 for track2, Staff2 for track
    * 0, Staff2 for track1, Staff2 for track2, ... }
    */
   private List<Staff> CreateStaffs(List<MusicSymbol>[] allsymbols, KeySignature key,
         MidiOptions options, int measurelen)
   {

      List<Staff>[] trackstaffs = new ArrayList[allsymbols.length];
      int totaltracks = trackstaffs.length;

      for (int track = 0; track < totaltracks; track++)
      {
         List<MusicSymbol> symbols = allsymbols[track];
         trackstaffs[track] =
               CreateStaffsForTrack(symbols, measurelen, key, options, track, totaltracks);
      }

      /* Update the EndTime of each Staff. EndTime is used for playback */
      for (List<Staff> list : trackstaffs)
      {
         for (int i = 0; i < list.size() - 1; i++)
         {
            list.get(i).setEndTime(list.get(i + 1).getStartTime());
         }
      }

      /* Interleave the staffs of each track into the result array. */
      int maxstaffs = 0;
      for (int i = 0; i < trackstaffs.length; i++)
      {
         if (maxstaffs < trackstaffs[i].size())
         {
            maxstaffs = trackstaffs[i].size();
         }
      }
      List<Staff> result = new ArrayList<Staff>(maxstaffs * trackstaffs.length);
      for (int i = 0; i < maxstaffs; i++)
      {
         for (List<Staff> list : trackstaffs)
         {
            if (i < list.size())
            {
               result.add(list.get(i));
            }
         }
      }
      return result;
   }

   /** Get the lyrics for each track */
   private static List<LyricSymbol>[] GetLyrics(List<MidiTrack> tracks)
   {
      boolean hasLyrics = false;
      List<LyricSymbol>[] result = new ArrayList[tracks.size()];
      for (int tracknum = 0; tracknum < tracks.size(); tracknum++)
      {
         MidiTrack track = tracks.get(tracknum);
         if (track.getLyrics() == null)
         {
            continue;
         }
         hasLyrics = true;
         result[tracknum] = new ArrayList<LyricSymbol>();
         for (MidiEvent ev : track.getLyrics())
         {
            String text =
                  new String(ev.Value) + String.valueOf(0) + String.valueOf(ev.Value.length);
            LyricSymbol sym = new LyricSymbol(ev.StartTime, text);
            result[tracknum].add(sym);
         }
      }
      if (!hasLyrics)
      {
         return null;
      }
      else
      {
         return result;
      }
   }

   /** Add the lyric symbols to the corresponding staffs */
   static void AddLyricsToStaffs(List<Staff> staffs, List<LyricSymbol>[] tracklyrics)
   {
      for (Staff staff : staffs)
      {
         List<LyricSymbol> lyrics = tracklyrics[staff.getTrack()];
         staff.AddLyrics(lyrics);
      }
   }

   /** Change the note colors for the sheet music, and redraw. */
   private void SetColors(Color[] newcolors, Color newshade, Color newshade2)
   {
      if (NoteColors == null)
      {
         NoteColors = new Color[12];
         for (int i = 0; i < 12; i++)
         {
            NoteColors[i] = Color.BLACK;
         }
      }
      if (newcolors != null)
      {
         for (int i = 0; i < 12; i++)
         {
            NoteColors[i] = newcolors[i];
         }
      }
      else
      {
         for (int i = 0; i < 12; i++)
         {
            NoteColors[i] = Color.BLACK;
         }
      }
      shadeBrush = newshade;
      shade2Brush = newshade2;
   }

   /** Get the color for a given note number */
   public Color NoteColor(int number)
   {
      return NoteColors[NoteScale.FromNumber(number)];
   }

   /** Get the shade brush */
   public Color getShadeBrush()
   {
      return shadeBrush;
   }

   /** Get the shade2 brush */
   public Color getShade2Brush()
   {
      return shade2Brush;
   }

   /** Get whether to show note letters or not */
   public int getShowNoteLetters()
   {
      return showNoteLetters;
   }

   /** Get the main key signature */
   public KeySignature getMainKey()
   {
      return mainkey;
   }

   /**
    * Set the size of the notes, large or small. Smaller notes means more notes
    * per staff.
    *
    * @param insets
    */

   /**
    * Draw the SheetMusic. Scale the graphics by the current zoom factor. Get
    * the vertical start and end points of the clip area. Only draw Staffs which
    * lie inside the clip area.
    */

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      setPreferredSize(new Dimension(iPageDivider.getPageWidth(), iPageDivider.getTotalHeight()));
      int x = 0;
      int y = 0;
      for (SheetMusicPage page : iPageDivider.getPages())
      {
         g.translate(x, y);
         page.paintPage(g, iCurrentPlayPosition, iScroller);
         g.translate(x, -y);
         y += iSizes.getPageHeight();
      }
   }

   /** Write the MIDI filename at the top of the page */
   private void DrawTitle(Graphics g)
   {
      int leftmargin = 20;
      int topmargin = 20;
      String title = new File(filename).getAbsolutePath();
      title = title.replace(".mid", "").replace("_", " ");
      Font font = new Font("Arial", Font.BOLD, 10);
      g.translate(leftmargin, topmargin);
      g.setColor(Color.BLACK);
      g.setFont(font);
      g.drawString(title, 0, 0);
      g.translate(-leftmargin, -topmargin);
   }

   /**
    * Print the given page of the sheet music. Page numbers start from 1. A
    * staff should fit within a single page, not be split across two pages. If
    * the sheet music has exactly 2 tracks, then two staffs should fit within a
    * single page, and not be split across two pages.
    *
    * @param insets
    */
   public void DoPrint(Graphics g, int pagenumber)
   {
      int leftmargin = 20;
      int topmargin = 20;
      int rightmargin = 20;
      int bottommargin = 20;

      float scale = (getWidth() - leftmargin - rightmargin) / iSizes.getPageWidth();
      int viewPageHeight = (int) ((getHeight() - topmargin - bottommargin) / scale);

      int ypos = iSizes.getTitleHeight();
      int pagenum = 1;
      int staffnum = 0;

      if (numtracks == 2 && (staffs.size() % 2) == 0)
      {
         /* Skip the staffs until we reach the given page number */
         while (staffnum + 1 < staffs.size() && pagenum < pagenumber)
         {
            int heights = staffs.get(staffnum).getHeight() + staffs.get(staffnum + 1).getHeight();
            if (ypos + heights >= viewPageHeight)
            {
               pagenum++;
               ypos = 0;
            }
            else
            {
               ypos += heights;
               staffnum += 2;
            }
         }
         /* Print the staffs until the height reaches viewPageHeight */
         if (pagenum == 1)
         {
            DrawTitle(g);
            ypos = iSizes.getTitleHeight();
         }
         else
         {
            ypos = 0;
         }
         for (; staffnum + 1 < staffs.size(); staffnum += 2)
         {
            int heights = staffs.get(staffnum).getHeight() + staffs.get(staffnum + 1).getHeight();

            if (ypos + heights >= viewPageHeight)
               break;

            g.translate(leftmargin, topmargin + ypos);
            staffs.get(staffnum).Draw(g, pen, iCurrentPlayPosition, iScroller);
            g.translate(-leftmargin, -(topmargin + ypos));
            ypos += staffs.get(staffnum).getHeight();
            g.translate(leftmargin, topmargin + ypos);
            staffs.get(staffnum + 1).Draw(g, pen, iCurrentPlayPosition, iScroller);
            g.translate(-leftmargin, -(topmargin + ypos));
            ypos += staffs.get(staffnum + 1).getHeight();
         }
      }

      else
      {
         /* Skip the staffs until we reach the given page number */
         while (staffnum < staffs.size() && pagenum < pagenumber)
         {
            if (ypos + staffs.get(staffnum).getHeight() >= viewPageHeight)
            {
               pagenum++;
               ypos = 0;
            }
            else
            {
               ypos += staffs.get(staffnum).getHeight();
               staffnum++;
            }
         }

         /* Print the staffs until the height reaches viewPageHeight */
         if (pagenum == 1)
         {
            DrawTitle(g);
            ypos = iSizes.getTitleHeight();
         }
         else
         {
            ypos = 0;
         }
         for (; staffnum < staffs.size(); staffnum++)
         {
            if (ypos + staffs.get(staffnum).getHeight() >= viewPageHeight)
               break;

            g.translate(leftmargin, topmargin + ypos);
            staffs.get(staffnum).Draw(g, pen, iCurrentPlayPosition, iScroller);
            g.translate(-leftmargin, -(topmargin + ypos));
            ypos += staffs.get(staffnum).getHeight();
         }
      }

      /* Draw the page number */
      Font font = new Font("Arial", 10, Font.BOLD);
      g.setFont(font);
      g.setColor(Color.BLACK);
      g.drawString("" + pagenumber, iSizes.getPageWidth() - leftmargin,
            topmargin + viewPageHeight - 12);
   }

   /**
    * Return the number of pages needed to print this sheet music. A staff
    * should fit within a single page, not be split across two pages. If the
    * sheet music has exactly 2 tracks, then two staffs should fit within a
    * single page, and not be split across two pages.
    */
   public int GetTotalPages()
   {
      int num = 1;
      int currheight = iSizes.getTitleHeight();

      if (numtracks == 2 && (staffs.size() % 2) == 0)
      {
         for (int i = 0; i < staffs.size(); i += 2)
         {
            int heights = staffs.get(i).getHeight() + staffs.get(i + 1).getHeight();
            if (currheight + heights > iSizes.getPageHeight())
            {
               num++;
               currheight = heights;
            }
            else
            {
               currheight += heights;
            }
         }
      }
      else
      {
         for (Staff staff : staffs)
         {
            if (currheight + staff.getHeight() > iSizes.getPageHeight())
            {
               num++;
               currheight = staff.getHeight();
            }
            else
            {
               currheight += staff.getHeight();
            }
         }
      }
      return num;
   }

   /**
    * Shade all the chords played at the given pulse time. Loop through all the
    * staffs and call staff.Shade(). If scrollGradually is true, scroll
    * gradually (smooth scrolling) to the shaded notes.
    */
   public void resetPlayPosition()
   {
      setPlayPositionTime(-999);
      return;
   }

   @Override
   public String toString()
   {
      String result = "SheetMusic staffs=" + staffs.size() + "\n";
      for (Staff staff : staffs)
      {
         result += staff.toString();
      }
      result += "End SheetMusic\n";
      return result;
   }

   public void zoomNotes(double d)
   {
      iSizes.setZoom(d);
      repaint();
   }

   public void changeZoom(double d)
   {
      zoomNotes(d);

      setPreferredSize(new Dimension(iPageDivider.getPageWidth(), iPageDivider.getTotalHeight()));

      init((String) null, (MidiOptions) null);
   }

   public SheetMusicSizes getSizes()
   {
      return iSizes;
   }

   public PageDivider initPageDivider()
   {
      iPageDivider = new PageDivider(staffs, iSizes, pen, iSongTitle);
      return iPageDivider;
   }

   public void setSongTitle(String songTitle)
   {
      iSongTitle = songTitle;
   }

   public PageDivider getPageDivider()
   {
      return iPageDivider;
   }

   public void setPlayPositionTime(long currentPulseTime)
   {
      iCurrentPlayPosition = currentPulseTime;
      repaint();
   }

   public void setScroller(Scroller scroller)
   {
      iScroller = scroller;
   }

}
