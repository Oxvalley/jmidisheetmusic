package core.midi.sheet.music;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * FileChooserDemo2.java requires these files:
 *   ImageFileView.java
 *   ImageFilter.java
 *   ImagePreview.java
 *   Utils.java
 *   images/jpgIcon.gif (required by ImageFileView.java)
 *   images/gifIcon.gif (required by ImageFileView.java)
 *   images/tiffIcon.gif (required by ImageFileView.java)
 *   images/pngIcon.png (required by ImageFileView.java)
 */
public class FileChooserDemo2 extends JPanel implements ActionListener
{
   /**
    *
    */
   private static final long serialVersionUID = 5491557563616811288L;
   static private String newline = "\n";
   private JTextArea log;
   private JFileChooser fc;

   public FileChooserDemo2()
   {
      super(new BorderLayout());

      // Create the log first, because the action listener
      // needs to refer to it.
      log = new JTextArea(5, 20);
      log.setMargin(new Insets(5, 5, 5, 5));
      log.setEditable(false);
      JScrollPane logScrollPane = new JScrollPane(log);

      JButton sendButton = new JButton("Attach...");
      sendButton.addActionListener(this);

      add(sendButton, BorderLayout.PAGE_START);
      add(logScrollPane, BorderLayout.CENTER);
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      // Set up the file chooser.
      if (fc == null)
      {
         fc = new JFileChooser();

         // Add a custom file filter
         fc.addChoosableFileFilter(new MidiFileFilter());
      }

      // Show it.
      int returnVal = fc.showDialog(FileChooserDemo2.this, "Select");

      // Process the results.
      if (returnVal != JFileChooser.APPROVE_OPTION)
      {
         JOptionPane.showMessageDialog(this, "Attachment cancelled by user."
               + newline, "", JOptionPane.INFORMATION_MESSAGE);
      }
      log.setCaretPosition(log.getDocument().getLength());

      // Reset the file chooser for the next time it's shown.
      fc.setSelectedFile(null);
   }
}
