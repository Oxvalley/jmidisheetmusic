package core.midi.sheet.music;

import java.util.Calendar;

import javax.swing.ImageIcon;

import common.utils.AppInfoInterface;

public class AppInfo implements AppInfoInterface
{
   static AppInfoInterface mInstance = null;
   
   private final String APPLICATION_NAME = "JMidiSheetMusic";
   private final String APPLICATION_VERSION = "3.1";
   private final String APPLICATION_COPYWRITE_TEXT = "(C) Oxvalley Software " + Calendar.getInstance().get(Calendar.YEAR);
   public static final String APPLICATION_ICON_PATH = "images/NotePair.png";
   private final String JDK_VERSION = "1.6";

   @Override
   public String getJdkVersion()
   {
      return JDK_VERSION;
   }

   @Override
   public String getApplicationName()
   {
      return APPLICATION_NAME;
   }

   @Override
   public String getVersion()
   {
      return APPLICATION_VERSION;
   }
   
   
   @Override
   public ImageIcon getIcon()
   {
      return new ImageIcon(APPLICATION_ICON_PATH);
   }

   @Override
   public String getCopywrightText()
   {
      return APPLICATION_COPYWRITE_TEXT;
   }
   
   public static AppInfoInterface getInstance(){
      if (mInstance == null)
      {
         mInstance = new AppInfo();
      }
      return mInstance;
   }
   
   private AppInfo(){
      
   }
}
