package core.midi.sheet.music;

public class HighLowPair
{

    private int high;

    public int getHigh()
    {
        return high;
    }

    public void setHigh(int high)
    {
        this.high = high;
    }

    public int getLow()
    {
        return low;
    }

    public void setLow(int low)
    {
        this.low = low;
    }

    private int low;

    public HighLowPair(int high, int low)
    {
        this.high = high;
        this.low = low;
    }

}
