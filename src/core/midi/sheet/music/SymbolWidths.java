package core.midi.sheet.music;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

/**
 * @class SymbolWidths The SymbolWidths class is used to vertically align notes in different tracks
 *        that occur at the same time (that have the same starttime). This is done by the following:
 *        - Store a list of all the start times. - Store the width of symbols for each start time,
 *        for each track. - Store the maximum width for each start time, across all tracks. - Get
 *        the extra width needed for each track to match the maximum width for that start time.
 *
 *        See method SheetMusic.AlignSymbols(), which uses this class.
 */

public class SymbolWidths
{

    /** Array of maps (starttime -> symbol width), one per track */
    private List<Hashtable<Long, Integer>> widths;

    /** Map of starttime -> maximum symbol width */
    private Hashtable<Long, Integer> maxwidths;

    /** An array of all the starttimes, in all tracks */
    private ArrayList<Long> starttimes;

    /**
     * Initialize the symbol width maps, given all the symbols in all the tracks, plus the lyrics in
     * all tracks.
     */
    public SymbolWidths(List<MusicSymbol>[] tracks, List<LyricSymbol>[] tracklyrics)
    {

        /* Get the symbol widths for all the tracks */

        widths = new ArrayList<Hashtable<Long, Integer>>();
        for (int track = 0; track < tracks.length; track++)
        {
            widths.add(GetTrackWidths(tracks[track]));
        }
        maxwidths = new Hashtable<Long, Integer>();

        /* Calculate the maximum symbol widths */
        for (Hashtable<Long, Integer> dict : widths)
        {
            for (long time : dict.keySet())
            {
                if (!maxwidths.containsKey(time) || (maxwidths.get(time) < dict.get(time)))
                {

                    maxwidths.put(time, dict.get(time));
                }
            }
        }

        if (tracklyrics != null)
        {
            for (List<LyricSymbol> lyrics : tracklyrics)
            {
                if (lyrics == null)
                {
                    continue;
                }
                for (LyricSymbol lyric : lyrics)
                {
                    int width = lyric.getMinWidth();
                    long time = lyric.getStartTime();
                    if (!maxwidths.containsKey(time) || (maxwidths.get(time) < width))
                    {

                        maxwidths.put(time, width);
                    }
                }
            }
        }

        /* Store all the start times to the starttime array */
        starttimes = new ArrayList<Long>(maxwidths.keySet());
        Collections.sort(starttimes);
    }

    /** Create a table of the symbol widths for each starttime in the track. */
    private static Hashtable<Long, Integer> GetTrackWidths(List<MusicSymbol> symbols)
    {
        Hashtable<Long, Integer> widths = new Hashtable<Long, Integer>();

        for (MusicSymbol m : symbols)
        {
            long start = m.getStartTime();
            int w = m.getMinWidth();

            if (m instanceof BarSymbol)
            {
                continue;
            }
            else if (widths.containsKey(start))
            {
                widths.put(start, widths.get(start) + w);
            }
            else
            {
                widths.put(start, w);
            }
        }
        return widths;
    }

    /**
     * Given a track and a start time, return the extra width needed so that the symbols for that
     * start time align with the other tracks.
     */
    public int GetExtraWidth(int track, long start)
    {
        if (!widths.get(track).containsKey(start))
        {
            return maxwidths.get(start);
        }
        else
        {
            return maxwidths.get(start) - widths.get(track).get(start);
        }
    }

    /** Return an array of all the start times in all the tracks */
    public Long[] getStartTimes()
    {
       Long[] integerArray = new Long[starttimes.size()];
       starttimes.toArray(integerArray);

       return integerArray;
    }

}
