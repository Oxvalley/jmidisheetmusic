package core.midi.sheet.music;

import java.util.ArrayList;
import java.util.List;

/**
 * @class ClefMeasures The ClefMeasures class is used to report what Clef (Treble or Bass) a given
 *        measure uses.
 */
public class ClefMeasures
{
    private List<Clef> clefs;
    /** The clefs used for each measure (for a single track) */
    private int measure;

    /** The length of a measure, in pulses */

    /**
     * Given the notes in a track, calculate the appropriate Clef to use for each measure. Store the
     * result in the clefs list.
     *
     * @param notes
     *            The midi notes
     * @param measurelen
     *            The length of a measure, in pulses
     */
    public ClefMeasures(List<MidiNote> notes, int measurelen)
    {
        measure = measurelen;
        Clef mainclef = MainClef(notes);
        int nextmeasure = measurelen;
        int pos = 0;
        Clef clef = mainclef;

        clefs = new ArrayList<Clef>();

        while (pos < notes.size())
        {
            /* Sum all the notes in the current measure */
            int sumnotes = 0;
            int notecount = 0;
            while (pos < notes.size() && notes.get(pos).getStartTime() < nextmeasure)
            {
                sumnotes += notes.get(pos).getNoteNumber();
                notecount++;
                pos++;
            }
            if (notecount == 0)
                notecount = 1;

            /* Calculate the "average" note in the measure */
            int avgnote = sumnotes / notecount;
            if (avgnote == 0)
            {
                /*
                 * This measure doesn't contain any notes. Keep the previous clef.
                 */
            }
            else if (avgnote >= WhiteNote.BottomTreble.Number())
            {
                clef = Clef.Treble;
            }
            else if (avgnote <= WhiteNote.TopBass.Number())
            {
                clef = Clef.Bass;
            }
            else
            {
                /*
                 * The average note is between G3 and F4. We can use either the treble or bass clef.
                 * Use the "main" clef, the clef that appears most for this track.
                 */
                clef = mainclef;
            }

            clefs.add(clef);
            nextmeasure += measurelen;
        }
        clefs.add(clef);
    }

    /** Given a time (in pulses), return the clef used for that measure. */
    public Clef GetClef(long l)
    {

        /* If the time exceeds the last measure, return the last measure */
        if (l / measure >= clefs.size())
        {
            return clefs.get(clefs.size() - 1);
        }
        else
        {
            return clefs.get((int) (l / measure));
        }
    }

    /**
     * Calculate the best clef to use for the given notes. If the average note is below Middle C,
     * use a bass clef. Else, use a treble clef.
     */
    private static Clef MainClef(List<MidiNote> notes)
    {
        int middleC = WhiteNote.MiddleC.Number();
        int total = 0;
        for (MidiNote m : notes)
        {
            total += m.getNoteNumber();
        }
        if (notes.size() == 0)
        {
            return Clef.Treble;
        }
        else if (total / notes.size() >= middleC)
        {
            return Clef.Treble;
        }
        else
        {
            return Clef.Bass;
        }
    }
}
