package core.midi.sheet.music;

import javax.sound.midi.ShortMessage;

public interface ShortMessageInterface
{
   ShortMessage getShortMessage();
   String toStringShortMessage();
   double getStartTime();
}
