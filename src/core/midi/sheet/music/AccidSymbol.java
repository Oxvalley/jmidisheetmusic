package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * @class AccidSymbol An accidental (accid) symbol represents a sharp, flat, or
 *        natural accidental that is displayed at a specific position (note and
 *        clef).
 */
public class AccidSymbol extends MusicSymbol
{
   private Accid accid;
   /** The accidental (sharp, flat, natural) */
   private WhiteNote whitenote;
   /** The white note where the symbol occurs */
   private Clef clef;
   /** Which clef the symbols is in */
   private int width;

   /** Width of symbol */

   /**
    * Create a new AccidSymbol with the given accidental, that is displayed at
    * the given note in the given clef.
    */
   public AccidSymbol(Accid accid, WhiteNote note, Clef clef, SheetMusicSizes sizes)
   {
      iSizes = sizes;
      this.accid = accid;
      this.whitenote = note;
      this.clef = clef;
      width = getMinWidth();
   }

   /** Return the white note this accidental is displayed at */
   public WhiteNote getNote()
   {
      return whitenote;
   }

   /**
    * Get the time (in pulses) this symbol occurs at. Not used. Instead, the
    * StartTime of the ChordSymbol containing this AccidSymbol is used.
    */
   @Override
   public long getStartTime()
   {
      return -1;
   }

   /** Get the minimum width (in pixels) needed to draw this symbol */
   @Override
   public int getMinWidth()
   {
      if (iSizes == null)
      {
         return 15;
      }
      else
      {
         return 3 * iSizes.getNoteHeight() / 2;
      }

   }

   /**
    * Get/Set the width (in pixels) of this symbol. The width is set in
    * SheetMusic.AlignSymbols() to vertically align symbols.
    */
   @Override
   public int getWidth()
   {
      return width;
   }

   @Override
   public void setWidth(int value)
   {
      width = value;
   }

   /**
    * Get the number of pixels this symbol extends above the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getAboveStaff()
   {
      return GetAboveStaff();
   }

   int GetAboveStaff()
   {
      int dist = WhiteNote.Top(clef).Dist(whitenote) * iSizes.getNoteHeight() / 2;
      if (accid == Accid.Sharp || accid == Accid.Natural)
         dist -= iSizes.getNoteHeight();
      else if (accid == Accid.Flat)
         dist -= 3 * iSizes.getNoteHeight() / 2;

      if (dist < 0)
         return -dist;
      else
         return 0;
   }

   /**
    * Get the number of pixels this symbol extends below the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getBelowStaff()
   {
      return GetBelowStaff();
   }

   private int GetBelowStaff()
   {
      int dist = WhiteNote.Bottom(clef).Dist(whitenote) * iSizes.getNoteHeight() / 2
            + iSizes.getNoteHeight();
      if (accid == Accid.Sharp || accid == Accid.Natural)
         dist += iSizes.getNoteHeight();

      if (dist > 0)
         return dist;
      else
         return 0;
   }

   /**
    * Draw the symbol.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   @Override
   public void Draw(Graphics g, Color pen, int ytop)
   {
      try
      {
         /* Align the symbol to the right */
         g.translate(getWidth() - getMinWidth(), 0);

         /* Store the y-pixel value of the top of the whitenote in ynote. */

         if (WhiteNote.Top(clef) == null)
         {
            System.out.println("WhiteNote.Top(clef) is null");
         }

         if (clef == null)
         {
            System.out.println("clef is null");
         }

         if (iSizes == null)
         {
            System.out.println("iSizes is null");
         }

         int ynote = ytop + WhiteNote.Top(clef).Dist(whitenote) * iSizes.getNoteHeight() / 2;

         if (accid == Accid.Sharp)
            DrawSharp(g, pen, ynote);
         else if (accid == Accid.Flat)
            DrawFlat(g, pen, ynote);
         else if (accid == Accid.Natural)
            DrawNatural(g, pen, ynote);

         g.translate(-(getWidth() - getMinWidth()), 0);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Draw a sharp symbol.
    *
    * @param ynote
    *           The pixel location of the top of the accidental's note.
    */
   public void DrawSharp(Graphics g, Color pen, int ynote)
   {

      /* Draw the two vertical lines */
      int ystart = ynote - iSizes.getNoteHeight();
      int yend = ynote + 2 * iSizes.getNoteHeight();
      int x = iSizes.getNoteHeight() / 2;
      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      g2.setColor(pen);
      g2.drawLine(x, ystart + 2, x, yend);
      x += iSizes.getNoteHeight() / 2;
      g2.drawLine(x, ystart, x, yend - 2);

      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THICK_SCORE_MUSIC_LINE));

      /* Draw the slightly upwards horizontal lines */
      int xstart = iSizes.getNoteHeight() / 2 - iSizes.getNoteHeight() / 4;
      int xend = iSizes.getNoteHeight() + iSizes.getNoteHeight() / 4;
      ystart = ynote + iSizes.getLineWidth();
      yend = ystart - iSizes.getLineWidth() - iSizes.getLineSpace() / 4;
      g2.drawLine(xstart, ystart, xend, yend);
      ystart += iSizes.getLineSpace();
      yend += iSizes.getLineSpace();
      g2.drawLine(xstart, ystart, xend, yend);
      ThickGraphics.resetStrokeThickness(g2);

   }

   /**
    * Draw a flat symbol.
    *
    * @param ynote
    *           The pixel location of the top of the accidental's note.
    * @param iSizes
    */
   public void DrawFlat(Graphics g, Color pen, int ynote)
   {
      int x = iSizes.getLineSpace() / 4;

      /* Draw the vertical line */

      g.setColor(pen);

      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

      g2.drawLine(x, ynote - iSizes.getNoteHeight() - iSizes.getNoteHeight() / 2, x,
            ynote + iSizes.getNoteHeight());

      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

      /*
       * Draw 3 bezier curves. All 3 curves start and stop at the same points.
       * Each subsequent curve bulges more and more towards the topright corner,
       * making the curve look thicker towards the top-right.
       */
      PolygonExt pol = new PolygonExt();

      // pol.addPoint(x, ynote + iSizes.getLineSpace() / 4);
      // pol.addPoint((int) (x + iSizes.getLineSpace() / 1.46), (int) (ynote -
      // iSizes.getLineSpace() / 3.7));
      // pol.addPoint((int) (x + iSizes.getLineSpace() / 1), ynote -
      // iSizes.getLineSpace() / 3);
      // pol.addPoint((int) (x + iSizes.getLineSpace() / 1.03), (int) (ynote +
      // iSizes.getLineSpace() / 2.18));
      // pol.addPoint(x, ynote + iSizes.getLineSpace() + iSizes.getLineWidth() +
      // 1);
      // pol.addPoint((int)(x*1.04), ynote + iSizes.getLineSpace() +
      // iSizes.getLineWidth() + 1);
      // pol.addPoint((int) (x + iSizes.getLineSpace() / 0.94), (int) (ynote +
      // iSizes.getLineSpace() / 2.01));
      // pol.addPoint((int) (x + iSizes.getLineSpace() / 1), ynote -
      // iSizes.getLineSpace() / 3);

      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * -0.07),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.1));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.49),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.4));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.95),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.27));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 1.03),
            (int) Math.round(ynote + iSizes.getLineSpace() * 0.20));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.73),
            (int) Math.round(ynote + iSizes.getLineSpace() * 0.74));
      pol.addPoint(Math.round(x + iSizes.getLineSpace() * 0),
            (int) Math.round(ynote + iSizes.getLineSpace() * 1.05));
      pol.addPoint(Math.round(x + iSizes.getLineSpace() * 0),
            (int) Math.round(ynote + iSizes.getLineSpace() * 1.35));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.71),
            (int) Math.round(ynote + iSizes.getLineSpace() * 1.07));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 1.10),
            (int) Math.round(ynote + iSizes.getLineSpace() * 0.655));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 1.25),
            (int) Math.round(ynote + iSizes.getLineSpace() * 0.24));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 1.22),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.54));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.73),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.61));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * 0.46),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.55));
      pol.addPoint((int) Math.round(x + iSizes.getLineSpace() * -0.07),
            (int) Math.round(ynote + iSizes.getLineSpace() * -0.1));

      g2.fillPolygon(pol);

      // pol.reset();
      // pol.addPoint(x, ynote + iSizes.getLineSpace() / 4);
      // pol.addPoint(x + iSizes.getLineSpace() / 2, ynote -
      // iSizes.getLineSpace() / 2);
      // pol.addPoint(x + iSizes.getLineSpace() + iSizes.getLineSpace() / 4,
      // ynote + iSizes.getLineSpace() / 3 - iSizes.getLineSpace() / 4);
      // pol.addPoint(x, ynote + iSizes.getLineSpace() + iSizes.getLineWidth() +
      // 1);
      //
      // pol.drawPolygon(g2);
      //
      // pol.reset();
      // pol.addPoint(x, ynote + iSizes.getLineSpace() / 4);
      // pol.addPoint(x + iSizes.getLineSpace() / 2, ynote -
      // iSizes.getLineSpace() / 2);
      // pol.addPoint(x + iSizes.getLineSpace() + iSizes.getLineSpace() / 2,
      // ynote + iSizes.getLineSpace() / 3 - iSizes.getLineSpace() / 2);
      // pol.addPoint(x, ynote + iSizes.getLineSpace() + iSizes.getLineWidth() +
      // 1);
      // pol.drawPolygon(g2);

   }

   /**
    * Draw a natural symbol.
    *
    * @param ynote
    *           The pixel location of the top of the accidental's note.
    * @param iSizes
    */
   public void DrawNatural(Graphics g, Color pen, int ynote)
   {

      /* Draw the two vertical lines */
      int ystart = ynote - iSizes.getLineSpace() - iSizes.getLineWidth();
      int yend = ynote + iSizes.getLineSpace() + iSizes.getLineWidth();
      int x = iSizes.getLineSpace() / 2;

      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

      g2.setColor(pen);
      g2.drawLine(x, ystart, x, yend);
      x += iSizes.getLineSpace() - iSizes.getLineSpace() / 4;
      ystart = ynote - iSizes.getLineSpace() / 4;
      yend = ynote + 2 * iSizes.getLineSpace() + iSizes.getLineWidth() - iSizes.getLineSpace() / 4;
      g2.drawLine(x, ystart, x, yend);

      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THICK_SCORE_MUSIC_LINE));
      /* Draw the slightly upwards horizontal lines */
      int xstart = iSizes.getLineSpace() / 2;
      int xend = xstart + iSizes.getLineSpace() - iSizes.getLineSpace() / 4;
      ystart = ynote + iSizes.getLineWidth();
      yend = ystart - iSizes.getLineWidth() - iSizes.getLineSpace() / 4;
      g2.drawLine(xstart, ystart, xend, yend);
      ystart += iSizes.getLineSpace();
      yend += iSizes.getLineSpace();
      g2.drawLine(xstart, ystart, xend, yend);

      ThickGraphics.resetStrokeThickness(g2);
   }

   @Override
   public String toString()
   {
      return String.format("AccidSymbol accid=" + accid + " whitenote=" + whitenote + " clef="
            + clef + " width=" + width);
   }

}
