package core.midi.sheet.music;

import java.awt.Polygon;

public class PolygonExt extends Polygon
{

    /**
    * 
    */
   private static final long serialVersionUID = -1814002831836106983L;

   @Override
    public String toString()
    {
        String cords = "";

        for (int i = 0; i < npoints; i++)
        {
            cords += xpoints[i] + ", " + ypoints[i] + " | ";
        }
        return "[ " + cords.substring(0, cords.length() - 2) + "]";

    }

}
