package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/* @class Staff
 * The Staff is used to draw a single Staff (a row of measures) in the
 * SheetMusic Control. A Staff needs to draw
 * - The Clef
 * - The key signature
 * - The horizontal lines
 * - A list of MusicSymbols
 * - The left and right vertical lines
 *
 * The height of the Staff is determined by the number of pixels each
 * MusicSymbol extends above and below the staff.
 *
 * The vertical lines (left and right sides) of the staff are joined
 * with the staffs above and below it, with one exception.
 * The last track is not joined with the first track.
 */

public class Staff
{
   private List<MusicSymbol> symbolsList;
   /** The music symbols in this staff */
   private List<LyricSymbol> lyrics;
   /** The lyrics to display (can be null) */
   private int ytop;
   /** The y pixel of the top of the staff */
   private ClefSymbol clefsym;
   /** The left-side Clef symbol */
   private AccidSymbol[] keys;
   /** The key signature symbols */
   private boolean showMeasures;
   /** If true, show the measure numbers */
   private int keysigWidth;
   /** The width of the clef and key signature */
   private int width;
   /** The width of the staff in pixels */
   private int height;
   /** The height of the staff in pixels */
   private int tracknum;
   /** The track this staff represents */
   private int totaltracks;
   /** The total number of tracks */
   private long starttime;
   /** The time (in pulses) of first symbol */
   private long endtime;
   /** The time (in pulses) of last symbol */
   private int measureLength;
   private SheetMusicSizes iSizes;

   /** The time (in pulses) of a measure */

   /**
    * Create a new staff with the given list of music symbols, and the given key
    * signature. The clef is determined by the clef of the first chord symbol.
    * The track number is used to determine whether to join this left/right
    * vertical sides with the staffs above and below. The SheetMusicOptions are
    * used to check whether to display measure numbers or not.
    */
   public Staff(List<MusicSymbol> symbols, KeySignature key,
         MidiOptions options, int tracknum, int totaltracks,
         SheetMusicSizes sizes)
   {

      iSizes = sizes;
      keysigWidth = sizes.KeySignatureWidth(key);
      this.tracknum = tracknum;
      this.totaltracks = totaltracks;
      showMeasures = (options.showMeasures && tracknum == 0);
      measureLength = options.time.getMeasure();
      Clef clef = FindClef(symbols);

      clefsym = new ClefSymbol(clef, 0, false, sizes);
      keys = key.GetSymbols(clef);
      this.symbolsList = symbols;
      CalculateWidth(sizes.getNotePageWidth());
      CalculateHeight(sizes);
      CalculateStartEndTime();
      FullJustify(sizes);
   }

   /** Return the width of the staff */
   public int getWidth()
   {
      return width;
   }

   /** Return the height of the staff */
   public int getHeight()
   {
      return height;
   }

   /** Return the track number of this staff (starting from 0 */
   public int getTrack()
   {
      return tracknum;
   }

   /**
    * Return the starting time of the staff, the start time of the first symbol.
    * This is used during playback, to automatically scroll the music while
    * playing.
    */
   public long getStartTime()
   {
      return starttime;
   }

   /**
    * Return the ending time of the staff, the endtime of the last symbol. This
    * is used during playback, to automatically scroll the music while playing.
    */
   public long getEndTime()
   {
      return endtime;
   }

   public void setEndTime(long value)
   {
      endtime = value;
   }

   /**
    * Find the initial clef to use for this staff. Use the clef of the first
    * ChordSymbol.
    */
   private static Clef FindClef(List<MusicSymbol> list)
   {
      for (MusicSymbol m : list)
      {
         if (m instanceof ChordSymbol)
         {
            ChordSymbol c = (ChordSymbol) m;
            return c.getClef();
         }
      }
      return Clef.Treble;
   }

   /**
    * Calculate the height of this staff. Each MusicSymbol contains the number
    * of pixels it needs above and below the staff. Get the maximum values above
    * and below the staff.
    *
    * @param sizes
    */
   public void CalculateHeight(SheetMusicSizes sizes)
   {
      int above = 0;
      int below = 0;

      for (MusicSymbol s : symbolsList)
      {
         above = Math.max(above, s.getAboveStaff());
         below = Math.max(below, s.getBelowStaff());
      }
      above = Math.max(above, clefsym.getAboveStaff());
      below = Math.max(below, clefsym.getBelowStaff());
      ytop = above + sizes.getNoteHeight();
      height = sizes.getNoteHeight() * 5 + ytop + below;
      if (showMeasures || lyrics != null)
      {
         height += sizes.getNoteHeight() * 3 / 2;
      }

      /*
       * Add some extra vertical space between the last track and first track.
       */
      if (tracknum == totaltracks - 1)
         height += sizes.getNoteHeight() * 3;
   }

   /**
    * Calculate the width of this staff
    *
    * @param pagewidth
    */
   private void CalculateWidth(int pagewidth)
   {
      width = pagewidth;
      return;
   }

   /** Calculate the start and end time of this staff. */
   private void CalculateStartEndTime()
   {
      starttime = endtime = 0;
      if (symbolsList.size() == 0)
      {
         return;
      }
      starttime = symbolsList.get(0).getStartTime();
      for (MusicSymbol m : symbolsList)
      {
         if (endtime < m.getStartTime())
         {
            endtime = m.getStartTime();
         }
         if (m instanceof ChordSymbol)
         {
            ChordSymbol c = (ChordSymbol) m;
            if (endtime < c.getEndTime())
            {
               endtime = c.getEndTime();
            }
         }
      }
   }

   /**
    * Full-Justify the symbols, so that they expand to fill the whole staff.
    *
    * @param pageWidth
    * @param sizes
    */
   private void FullJustify(SheetMusicSizes sizes)
   {

      int totalwidth = keysigWidth;
      int totalsymbols = 0;
      int i = 0;

      while (i < symbolsList.size())
      {
         long start = symbolsList.get(i).getStartTime();
         totalsymbols++;
         totalwidth += symbolsList.get(i).getWidth();
         i++;
         while (i < symbolsList.size() && symbolsList.get(i).getStartTime() == start)
         {
            totalwidth += symbolsList.get(i).getWidth();
            i++;
         }
      }

      int extrawidth = (sizes.getNotePageWidth() - totalwidth - 1)
            / totalsymbols;
      if (extrawidth > sizes.getNoteHeight() * 2)
      {
         extrawidth = sizes.getNoteHeight() * 2;
      }
      i = 0;
      while (i < symbolsList.size())
      {
         long start = symbolsList.get(i).getStartTime();
         symbolsList.get(i).setWidth(symbolsList.get(i).getWidth() + extrawidth);
         i++;
         while (i < symbolsList.size() && symbolsList.get(i).getStartTime() == start)
         {
            i++;
         }
      }
   }

   /**
    * Add the lyric symbols that occur within this staff. Set the x-position of
    * the lyric symbol.
    */
   public void AddLyrics(List<LyricSymbol> tracklyrics)
   {
      if (tracklyrics == null)
      {
         return;
      }
      lyrics = new ArrayList<LyricSymbol>();
      int xpos = 0;
      int symbolindex = 0;
      for (LyricSymbol lyric : tracklyrics)
      {
         if (lyric.getStartTime() < starttime)
         {
            continue;
         }
         if (lyric.getStartTime() > endtime)
         {
            break;
         }
         /* Get the x-position of this lyric */
         while (symbolindex < symbolsList.size()
               && symbolsList.get(symbolindex).getStartTime() < lyric
                     .getStartTime())
         {
            xpos += symbolsList.get(symbolindex).getWidth();
            symbolindex++;
         }
         lyric.setX(xpos);
         if (symbolindex < symbolsList.size()
               && (symbolsList.get(symbolindex) instanceof BarSymbol))
         {
            lyric.setX(lyric.getX() + iSizes.getNoteWidth());
         }
         lyrics.add(lyric);
      }
      if (lyrics.size() == 0)
      {
         lyrics = null;
      }
   }

   /** Draw the lyrics */
   private void DrawLyrics(Graphics g, Color pen)
   {
      /* Skip the left side Clef symbol and key signature */
      int xpos = keysigWidth;
      int ypos = height - iSizes.getNoteHeight() * 2;

      for (LyricSymbol lyric : lyrics)
      {
         g.setColor(pen);
         g.drawString(lyric.getText(),
               xpos + lyric.getX(), ypos);
      }
   }

   /** Draw the measure numbers for each measure */
   private void DrawMeasureNumbers(Graphics g, Color pen)
   {

      /* Skip the left side Clef symbol and key signature */
      int xpos = keysigWidth;
      int ypos = height - iSizes.getNoteHeight() * 3 / 2;

      for (MusicSymbol s : symbolsList)
      {
         if (s instanceof BarSymbol)
         {
            long measure = 1 + s.getStartTime() / measureLength;
            g.setColor(pen);
            g.drawString("" + measure,
                  xpos + iSizes.getNoteWidth(), ypos);
         }
         xpos += s.getWidth();
      }
   }

   /** Draw the lyrics */

   /**
    * Draw the five horizontal lines of the staff
    *
    * @param sizes
    */
   private void DrawHorizLines(Graphics g, Color pen)
   {
      int line = 1;
      int y = ytop - iSizes.getLineWidth();
      g.setColor(pen);
      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      for (line = 1; line <= 5; line++)
      {
         g2.drawLine(iSizes.getLeftMargin(), y, width - 1, y);
         y += iSizes.getLineWidth() + iSizes.getLineSpace();
      }
      ThickGraphics.resetStrokeThickness(g2);
      g.setColor(Color.BLACK);

   }

   /**
    * Draw the vertical lines at the far left and far right sides.
    *
    * @param sizes
    */
   private void DrawEndLines(Graphics g, Color pen)
   {

      /*
       * Draw the vertical lines from 0 to the height of this staff, including
       * the space above and below the staff, with two exceptions: - If this is
       * the first track, don't start above the staff. Start exactly at the top
       * of the staff (ytop - LineWidth) - If this is the last track, don't end
       * below the staff. End exactly at the bottom of the staff.
       */
      g.setColor(pen);
      int ystart, yend;
      if (tracknum == 0)
         ystart = ytop - iSizes.getLineWidth();
      else
         ystart = 0;

      if (tracknum == (totaltracks - 1))
         yend = ytop + 4 * iSizes.getNoteHeight();
      else
         yend = height;

      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      g2.drawLine(iSizes.getLeftMargin(), ystart, iSizes.getLeftMargin(), yend);

      g2.drawLine(width - 1, ystart, width - 1, yend);
      ThickGraphics.resetStrokeThickness(g2);
   }

   /**
    * Draw this staff. Only draw the symbols inside the clip area
    *
    *
    * @param sizes
    *
    * @param insets
    */
   public void Draw(Graphics g, Color pen, long currentTimePosition,
         ScrollerInterface scroller)
   {
      int xpos = iSizes.getLeftMargin();

      /* Draw the left side Clef symbol */
      g.translate(xpos, 0);
      clefsym.Draw(g, pen, ytop);
      g.translate(-xpos, 0);
      xpos += clefsym.getWidth();

      /* Draw the key signature */
      for (AccidSymbol a : keys)
      {
         g.translate(xpos, 0);
         a.Draw(g, pen, ytop);
         g.translate(-xpos, 0);
         xpos += a.getWidth();
      }

      /*
       * Draw the actual notes, rests, bars. Draw the symbols one after another,
       * using the symbol width to determine the x position of the next symbol.
       */

      for (MusicSymbol s : symbolsList)
      {
         g.translate(xpos, 0);

         if (!(s instanceof BarSymbol))
         {
            if (currentTimePosition == s.getStartTime())
            {
               Color oldColor = g.getColor();
               g.setColor(Color.GREEN);
               g.fillRect(0, 0, s.getWidth(), this.getHeight());
               g.setColor(oldColor);
               if (scroller != null)
               {
                  Graphics2D g2 = (Graphics2D) g;
                  Rectangle rect = new Rectangle((int) g2.getTransform()
                        .getTranslateX(), (int) g2.getTransform()
                        .getTranslateY(), s.getWidth()*4, this.getHeight());

                  scroller.scrollRectToVisible(rect);
               }
            }
         }

         s.Draw(g, pen, ytop);
         g.translate(-xpos, 0);
         xpos += s.getWidth();
      }
      DrawHorizLines(g, pen);
      DrawEndLines(g, pen);

      if (showMeasures)
      {
         DrawMeasureNumbers(g, pen);
      }
      if (lyrics != null)
      {
         DrawLyrics(g, pen);
      }

   }


   @Override
   public String toString()
   {
      String result = "Staff clef=" + clefsym.toString() + "\n";
      result += "  Keys:\n";
      for (AccidSymbol a : keys)
      {
         result += "    " + a.toString() + "\n";
      }
      result += "  Symbols:\n";
      for (MusicSymbol s : keys)
      {
         result += "    " + s.toString() + "\n";
      }
      for (MusicSymbol m : symbolsList)
      {
         result += "    " + m.toString() + "\n";
      }
      result += "End Staff\n";
      return result;
   }

}
