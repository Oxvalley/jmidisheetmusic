package core.midi.sheet.music;

public interface IMidiPlayer
{

   public void DoStop();

   public boolean shouldBeStopped();
   
}
