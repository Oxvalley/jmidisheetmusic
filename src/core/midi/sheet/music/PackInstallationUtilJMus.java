package core.midi.sheet.music;

import java.util.List;

import common.utils.AppInfoInterface;
import common.utils.PackInstallationUtil;

public class PackInstallationUtilJMus extends PackInstallationUtil
{

   public PackInstallationUtilJMus(String appName, String version, String jdkVersion,
         String appNameVersion, String mainClassNAme, boolean packSource)
   {
      super(appName, version, jdkVersion, appNameVersion, mainClassNAme, packSource);
   }

   public static void main(String[] args)
   {
      boolean packSource = false;
      if (args.length > 0)
      {
         if (args[0].equals("src"))
         {
            packSource = true;
         }
      }
      
      AppInfoInterface appInfo = AppInfo.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      String jdkVersion = appInfo.getJdkVersion();
      String appNameVersion = "JMusic_v_" + getVersionNoDots(version);
      String mainClass = "core.midi.sheet.music.JMidiSheetMusic";
      PackInstallationUtil pi = new PackInstallationUtilJMus(name, version, jdkVersion,
            appNameVersion, mainClass, packSource);
      pi.execute();
   }

   @Override
   protected void copyFilesAndFolders()
   {
      super.copyFilesAndFolders();
      AppInfoInterface appInfo = AppInfo.getInstance();
      copyFolder("images");
      copyFolder("midi");
      copyFolder("songs");
      copyFile("run JMidiSheetMusic.exe", "run " + getVersionNoDots(appInfo.getVersion()) + ".exe");
      copyFile("images/NotePair.png");
      copyFile("help.rtf");
   }

   @Override
   protected List<String> getProjectDependendies()
   {
      List<String> list = super.getProjectDependendies();
      list.add("../Common Project");
      return list;
   }
}
