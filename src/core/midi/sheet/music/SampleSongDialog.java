package core.midi.sheet.music;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

/**
 * @class SampleSongDialog The SampleSongDialog is used to select one of the 50+
 *        sample midi songs that ship with MidiSheetMusic.
 * 
 *        The method ShowDialog() returns DialogResult.OK/Cancel. The method
 *        GetSong() returns the selected song.
 */
public class SampleSongDialog extends AbstractJDialogExt
{

   /**
    *
    */
   private static final long serialVersionUID = 481501440349645776L;
   private JListExt songView;
   /** The list of songs **/
   /** The ok button */
   private JButton ok;
   /** The cancel button */
   private JButton cancel;

   /**
    * Create a new SampleSongDialog. Call the ShowDialog() method to display the
    * dialog.
    */
   public SampleSongDialog()
   {

      setResizable(false);
      setModal(true);
      setLayout(null);

      /* Create the dialog box */
      setTitle("Choose a Sample MIDI Song");
      Font font = getFont();

      if (font != null)
      {
         setFont(new Font(font.getFamily(), Font.PLAIN,
               (int) (font.getSize() * 1.4)));
      }

      JList imagelist = new JList();
      imagelist.add(new JLabel(new ImageIcon("images/SmallNotePair.png")));

      songView = new JListExt();
      songView.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

      String[] songs = new String[] { "Bach__Invention_No._13",
            "Bach__Minuet_in_G_major", "Bach__Musette_in_D_major",
            "Bach__Prelude_in_C_major", "Beethoven__Fur_Elise",
            "Beethoven__Minuet_in_G_major", "Beethoven__Moonlight_Sonata",
            "Beethoven__Sonata_Pathetique_2nd_Mov",
            "Bizet__Habanera_from_Carmen", "Borodin__Polovstian_Dance",
            "Brahms__Hungarian_Dance_No._5",
            "Brahms__Waltz_No._15_in_A-flat_major",
            "Brahms__Waltz_No._9_in_D_minor",
            "Chopin__Minute_Waltz_Op._64_No._1_in_D-flat_major",
            "Chopin__Nocturne_Op._9_No._1_in_B-flat_minor",
            "Chopin__Nocturne_Op._9_No._2_in_E-flat_major",
            "Chopin__Nocturne_in_C_minor",
            "Chopin__Prelude_Op._28_No._20_in_C_minor",
            "Chopin__Prelude_Op._28_No._4_in_E_minor",
            "Chopin__Prelude_Op._28_No._6_in_B_minor",
            "Chopin__Prelude_Op._28_No._7_in_A_major",
            "Chopin__Waltz_Op._64_No._2_in_Csharp_minor",
            "Clementi__Sonatina_Op._36_No._1", "Easy_Songs__Brahms_Lullaby",
            "Easy_Songs__Greensleeves", "Easy_Songs__Jingle_Bells",
            "Easy_Songs__Silent_Night",
            "Easy_Songs__Twinkle_Twinkle_Little_Star",
            "Field__Nocturne_in_B-flat_major", "Grieg__Canon_Op._38_No._8",
            "Grieg__Peer_Gynt_Morning", "Handel__Sarabande_in_D_minor",
            "Liadov__Prelude_Op._11_in_B_minor", "MacDowelll__To_a_Wild_Rose",
            "Massenet__Elegy_in_E_minor",
            "Mendelssohn__Venetian_Boat_Song_Op._19b_No._6",
            "Mendelssohn__Wedding_March", "Mozart__Aria_from_Don_Giovanni",
            "Mozart__Eine_Kleine_Nachtmusik",
            "Mozart__Fantasy_No._3_in_D_minor", "Mozart__Minuet_from_Don_Juan",
            "Mozart__Rondo_Alla_Turca", "Mozart__Sonata_K.545_in_C_major",
            "Offenbach__Barcarolle_from_The_Tales_of_Hoffmann",
            "Pachelbel__Canon_in_D_major", "Prokofiev__Peter_and_the_Wolf",
            "Puccini__O_Mio_Babbino_Caro",
            "Rebikov__Valse_Melancolique_Op._2_No._3", "Saint-Saens__The_Swan",
            "Satie__Gnossienne_No._1", "Satie__Gymnopedie_No._1",
            "Schubert__Impromptu_Op._90_No._4_in_A-flat_major",
            "Schubert__Moment_Musicaux_No._1_in_C_major",
            "Schubert__Moment_Musicaux_No._3_in_F_minor",
            "Schubert__Serenade_in_D_minor",
            "Schumann__Scenes_From_Childhood_Op._15_No._12",
            "Schumann__The_Happy_Farmer", "Strauss__The_Blue_Danube_Waltz",
            "Tchaikovsky__Album_for_the_Young_-_Old_French_Song",
            "Tchaikovsky__Album_for_the_Young_-_Polka",
            "Tchaikovsky__Album_for_the_Young_-_Waltz",
            "Tchaikovsky__Nutcracker_-_Dance_of_the_Reed_Flutes",
            "Tchaikovsky__Nutcracker_-_Dance_of_the_Sugar_Plum_Fairies",
            "Tchaikovsky__Nutcracker_-_March_of_the_Toy_Soldiers",
            "Tchaikovsky__Nutcracker_-_Waltz_of_the_Flowers",
            "Tchaikovsky__Swan_Lake", "Verdi__La_Donna_e_Mobile" };

      for (String name : songs)
      {
         /* Change "Bach__Minuet_in_G" to "Bach: Minuet in G" */
         String song = name;
         song = song.replace("__", ": ");
         song = song.replace("_", " ");
         songView.addElement(song);
      }

      songView.addMouseListener(new MouseAdapter()
      {
         @Override
         public void mouseClicked(MouseEvent evt)
         {
            if (evt.getClickCount() == 2)
            {
               OKclicked();
            }
         }
      });

      // Use model with JList
      JScrollPane scrollPane = new JScrollPane();
      scrollPane.getViewport().add(songView);
      scrollPane.setBounds(20, 20, 630, 215);
      add(scrollPane);

      /* Create the OK and Cancel buttons */
      ok = new JButton();
      add(ok);
      ok.setText("OK");
      ok.setBounds(670, 35, 90, 20);
      ok.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            OKclicked();
         }
      });

      cancel = new JButton();
      add(cancel);
      cancel.setText("Cancel");
      cancel.setBounds(670, 80, 90, 20);
      cancel.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            setOKClicked(false);
            Dispose();
         }
      });

      setSize(780, 285);
   }

   /**
    * When the dialog is resized, adjust the size of the songView. Change the
    * position of the ok/cancel buttons relative to the right side of the
    * dialog.
    */
   // protected void OnResize(EventArgs e) {
   // int labelheight = Font.Height * 2;
   // base.OnResize(e);
   // if (songView != null) {
   // songView.Size = new Dimension(ClientSize.getWidth() - labelheight*4,
   // ClientSize.Height - labelheight);
   // }
   // if (ok != null) {
   // ok.setLocation(new Point(ClientSize.getWidth() - labelheight*3,
   // labelheight));
   // }
   // if (cancel != null) {
   // cancel.setLocation(new Point(ClientSize.getWidth() - labelheight*3,
   // labelheight*2 + labelheight/4));
   // }
   // }

   /** Get the currently selected song */
   public String GetSong()
   {
      String song = (String) songView.getSelectedValue();
      if (song == null)
      {
         song = (String) songView.getModel().getElementAt(0);
      }
      song = song.replace(": ", "__");
      song = song.replace(" ", "_");
      return song;
   }

   private void OKclicked()
   {
      if (songView.getSelectedValue() != null)
      {
         setOKClicked(true);
         Dispose();
      }
   }
}
