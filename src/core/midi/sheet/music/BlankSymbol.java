package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @class BlankSymbol The Blank symbol is a music symbol that doesn't draw
 *        anything. This symbol is used for alignment purposes, to align notes
 *        in different staffs which occur at the same time.
 */
public class BlankSymbol extends MusicSymbol
{
   private long starttime;
   private int width;

   /** Create a new BlankSymbol with the given starttime and width */
   public BlankSymbol(long start, int width)
   {
      this.starttime = start;
      this.width = width;
   }

   /**
    * Get the time (in pulses) this symbol occurs at. This is used to determine
    * the measure this symbol belongs to.
    */
   @Override
   public long getStartTime()
   {
      return starttime;
   }

   /** Get the minimum width (in pixels) needed to draw this symbol */
   @Override
   public int getMinWidth()
   {
      return 0;
   }

   /**
    * Get/Set the width (in pixels) of this symbol. The width is set in
    * SheetMusic.AlignSymbols() to vertically align symbols.
    */
   @Override
   public int getWidth()
   {
      return width;
   }

   @Override
   public void setWidth(int value)
   {
      width = value;
   }

   /**
    * Get the number of pixels this symbol extends above the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getAboveStaff()
   {
      return 0;
   }

   /**
    * Get the number of pixels this symbol extends below the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getBelowStaff()
   {
      return 0;
   }

   /**
    * Draw nothing.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   @Override
   public void Draw(Graphics g, Color pen, int ytop)
   {
   }

   @Override
   public String toString()
   {
      return String.format("BlankSymbol starttime=" + starttime + " width=" + width);
   }
}
