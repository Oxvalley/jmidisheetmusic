package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * @class TimeSigSymbol A TimeSigSymbol represents the time signature at the
 *        beginning of the staff. We use pre-made images for the numbers,
 *        instead of drawing strings.
 */

public class TimeSigSymbol extends MusicSymbol
{
   private static BufferedImage[] images;
   /** The images for each number */
   private int numerator;
   /** The numerator */
   private int denominator;
   /** The denominator */
   private int width;
   /** The width in pixels */
   private boolean candraw;

   /** True if we can draw the time signature */

   /**
    * Create a new TimeSigSymbol
    * 
    * @param sizes
    */
   public TimeSigSymbol(int numer, int denom, SheetMusicSizes sizes)
   {
      iSizes = sizes;
      numerator = numer;
      denominator = denom;
      LoadImages();
      if (numer >= 0 && numer < images.length && images[numer] != null && denom >= 0
            && denom < images.length && images[numer] != null)
      {
         candraw = true;
      }
      else
      {
         candraw = false;
      }
      width = getMinWidth();
   }

   /** Load the images into memory. */
   private static void LoadImages()
   {
      if (images == null)
      {
         images = new BufferedImage[13];
         for (int i = 0; i < 13; i++)
         {
            images[i] = null;
         }
         images[2] = ImageHelper.loadImage("images/two.png");
         images[3] = ImageHelper.loadImage("images/three.png");
         images[4] = ImageHelper.loadImage("images/four.png");
         images[6] = ImageHelper.loadImage("images/six.png");
         images[8] = ImageHelper.loadImage("images/eight.png");
         images[9] = ImageHelper.loadImage("images/nine.png");
         images[12] = ImageHelper.loadImage("images/twelve.png");
      }
   }

   /** Get the time (in pulses) this symbol occurs at. */
   @Override
   public long getStartTime()
   {
      return -1;
   }

   /** Get the minimum width (in pixels) needed to draw this symbol */
   @Override
   public int getMinWidth()
   {
      if (candraw)
         return images[2].getWidth() * iSizes.getNoteHeight() * 2 / images[2].getHeight();
      else
         return 0;

   }

   /**
    * Get/Set the width (in pixels) of this symbol. The width is set in
    * SheetMusic.AlignSymbols() to vertically align symbols.
    */
   @Override
   public int getWidth()
   {
      return width;
   }

   @Override
   public void setWidth(int value)
   {
      width = value;
   }

   /**
    * Get the number of pixels this symbol extends above the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getAboveStaff()
   {
      return 0;
   }

   /**
    * Get the number of pixels this symbol extends below the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getBelowStaff()
   {
      return 0;
   }

   /**
    * Draw the symbol.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   @Override
   public void Draw(Graphics g, Color pen, int ytop)
   {
      if (!candraw)
         return;

      g.translate(getWidth() - getMinWidth(), 0);
      BufferedImage numer = images[numerator];
      BufferedImage denom = images[denominator];

      /* Scale the BufferedImage width to match the height */
      int imgheight = iSizes.getNoteHeight() * 2;
      int imgwidth = numer.getWidth() * imgheight / numer.getHeight();
      g.drawImage(numer, 0, ytop, imgwidth, imgheight, null);
      g.drawImage(denom, 0, ytop + iSizes.getNoteHeight() * 2, imgwidth, imgheight, null);
      g.translate(-(getWidth() - getMinWidth()), 0);
   }

   @Override
   public String toString()
   {
      return String.format("TimeSigSymbol numerator=" + numerator + " denominator=" + denominator);
   }
}
