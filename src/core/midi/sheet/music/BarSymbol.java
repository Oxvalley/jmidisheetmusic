package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * @class BarSymbol The BarSymbol represents the vertical bars which delimit measures. The starttime
 *        of the symbol is the beginning of the new measure.
 */
public class BarSymbol extends MusicSymbol
{
    private int starttime;
    private int width;

    /** Create a BarSymbol. The starttime should be the beginning of a measure. */
    public BarSymbol(int starttime, SheetMusicSizes sizes)
    {
        iSizes = sizes;
        this.starttime = starttime;
        width = getMinWidth();
    }

    /**
     * Get the time (in pulses) this symbol occurs at. This is used to determine the measure this
     * symbol belongs to.
     */
    @Override
    public long getStartTime()
    {
        return starttime;
    }

    /** Get the minimum width (in pixels) needed to draw this symbol */
    @Override
    public int getMinWidth()
    {
        return 2 * iSizes.getLineSpace();
    }

    /**
     * Get/Set the width (in pixels) of this symbol. The width is set in SheetMusic.AlignSymbols()
     * to vertically align symbols.
     */
    @Override
    public int getWidth()
    {
        return width;
    }

    @Override
    public void setWidth(int value)
    {
        width = value;
    }

    /**
     * Get the number of pixels this symbol extends above the staff. Used to determine the minimum
     * height needed for the staff (Staff.FindBounds).
     */
    @Override
    public int getAboveStaff()
    {
        return 0;
    }

    /**
     * Get the number of pixels this symbol extends below the staff. Used to determine the minimum
     * height needed for the staff (Staff.FindBounds).
     */
    @Override
    public int getBelowStaff()
    {
        return 0;
    }

    /**
     * Draw a vertical bar.
     *
     * @param ytop
     *            The ylocation (in pixels) where the top of the staff starts.
     */
    @Override
    public void Draw(Graphics g, Color pen, int ytop)
    {
       Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
             iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

        int y = ytop;
        int yend = y + (int) ((iSizes.getLineSpace()+ iSizes.getLineWidth()) * 3.8);
        g2.setColor(pen);
        g2.drawLine(iSizes.getNoteWidth() / 2, y, iSizes.getNoteWidth() / 2, yend);

        ThickGraphics.resetStrokeThickness(g2);

    }

    @Override
    public String toString()
    {
        return String.format("BarSymbol starttime="+starttime+" width="+width);
    }
}
