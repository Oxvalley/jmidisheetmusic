package core.midi.sheet.music;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class PageDivider
{

   private List<SheetMusicPage> iPages;
   public static int STAFFS_PER_PAGE =10; // TODO modula with number of staffs
   private SheetMusicSizes iSizes;
   private List<Staff> iStaffs;
   private Color iPen;
   private String iSongTitle;

   public PageDivider(List<Staff> staffs, SheetMusicSizes sizes, Color pen,String songTitle)
   {
      iStaffs = staffs;
      iSizes = sizes;
      iPen = pen;
      iSongTitle = songTitle;
     
      iPages = new ArrayList<SheetMusicPage>();

      // Divide into pages
      List<Staff> onePageStaffList = new ArrayList<Staff>();

      for (int counter = 0; counter < staffs.size(); counter++)
      {
         if ((counter % STAFFS_PER_PAGE) == 0)
         {
            if (onePageStaffList.size() > 0)
            {
               iPages.add(new SheetMusicPage(onePageStaffList, sizes, pen,songTitle,
                     iPages.size() + 1));
            }
            onePageStaffList = new ArrayList<Staff>();
         }

         Staff staff = staffs.get(counter);
         onePageStaffList.add(staff);
      }

      if (onePageStaffList.size() > 0)
      {
         iPages.add(new SheetMusicPage(onePageStaffList, sizes, pen,songTitle, iPages
               .size() + 1));
      }
      
      for (SheetMusicPage page : iPages)
      {
         page.setPageCount(iPages.size());
      }
   }

   public List<SheetMusicPage> getPages()
   {
      return iPages;
   }

   public int getTotalHeight()
   {
      int retVal = 0;
      for (SheetMusicPage page : iPages)
      {
         retVal += page.getHeight();
      }

      return retVal;
   }

   public int getPageWidth()
   {
      return iSizes.getPageWidth();
   }

   public PageDivider clone(SheetMusicSizes sizes)
   {
      // TODO staffs must be copied. They contain a lotof symbols with old SheetMusicSizes
      return new PageDivider(iStaffs, sizes, iPen, iSongTitle);
   }
}
