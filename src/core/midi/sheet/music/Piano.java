package core.midi.sheet.music;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan Java port Copyright(c) 2012 Lars Svensson
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License version 2.
 *
 * This program is distributed : the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JPanel;

import common.properties.SettingProperties;

/**
 * @class Piano
 *
 *        The Piano Control is the panel at the top that displays the piano, and
 *        highlights the piano notes during playback. The main methods are:
 *
 *        SetMidiFile() - Set the Midi file to use for shading. The Midi file is
 *        needed to determine which notes to shade.
 *
 *        ShadeNotes() - Shade notes on the piano that occur at a given pulse
 *        time.
 *
 */

public class Piano extends JPanel
{
   public static final String KEYBOARD_COLORED = "keyboard.colored";
   /**
    *
    */
   private static final long serialVersionUID = 3374674094717489552L;
   public static int KeysPerOctave = 7;
   public static int MaxOctave = 7;

   private static int WhiteKeyWidth;
   /** Width of a single white key */
   private static int WhiteKeyHeight;
   /** Height of a single white key */
   private static int BlackKeyWidth;
   /** Width of a single black key */
   private static int BlackKeyHeight;
   /** Height of a single black key */
   private static int margin;
   /** The top/left margin to the piano */
   private static int BlackBorder;
   /** The width of the black border around the keys */

   private static int[] blackKeyOffsets;
   /** The x pixles of the black keys */

   private boolean useTwoColors;
   /** If true, use two colors for highlighting */
   private List<MidiNote> notes;
   /** The Midi notes for shading */
   private int maxShadeDuration;
   /** The maximum duration we'll shade a note for */
   private int showNoteLetters;
   /** Display the letter for each piano note */
   private Graphics graphics;
   /** The graphics for shading the notes */
   private Color gray2Pen;
   private Color gray1Pen;
   private Color gray3Pen;
   public JSheetMusicWindow Parent;
   private Color shadeRightHandPen;
   private Color shadeLeftHandPen;
   private int ypos;
   private int KeysDrawStartX = 0;
   private int KeysDrawStartY = 0;
   private ColoredKeyManager man;
   private boolean isColored;

   public static Color Ckey = new Color(239, 100, 107);
   public static Color Dkey = new Color(252, 153, 96);
   public static Color Ekey = new Color(237, 236, 70);
   public static Color Fkey = new Color(90, 220, 60);
   public static Color Gkey = new Color(59, 184, 214);
   public static Color Akey = new Color(59, 74, 201);
   public static Color Bkey = new Color(182, 79, 205);
   // public static Color[] keyColors = { Ckey, Dkey, Ekey, Fkey, Gkey, Akey,
   // Bkey };
   public static ColoredKey CKey = new ColoredKey(255, 0, 0, true); // #FF0000
   public static ColoredKey CSharpKey = new ColoredKey(255, 96, 0, false);// #FF6000
   public static ColoredKey DKey = new ColoredKey(255, 154, 0, true);// #FF9A00
   public static ColoredKey DSharpKey = new ColoredKey(255, 205, 0, false);// #FFCD00
   public static ColoredKey EKey = new ColoredKey(254, 255, 0, true);// #FEFF00
   public static ColoredKey FKey = new ColoredKey(186, 232, 6, true);// #BAE806
   public static ColoredKey FSharpKey = new ColoredKey(46, 206, 12, false);// #2ECE0C
   public static ColoredKey GKey = new ColoredKey(12, 153, 206, true);// #0C99CE
   public static ColoredKey GSharpKey = new ColoredKey(12, 91, 206, false);// #0C5BCE
   public static ColoredKey AKey = new ColoredKey(31, 12, 206, true);// #1F0CCE
   public static ColoredKey ASharpKey = new ColoredKey(128, 12, 206, false); // #800CCE
   public static ColoredKey BKey = new ColoredKey(206, 12, 130, true); // #CE0C82
   public static ColoredKey[] keyColors2 = { CKey, CSharpKey, DKey, DSharpKey, EKey, FKey,
      FSharpKey, GKey, GSharpKey, AKey, ASharpKey, BKey };

   /** Create a new Piano. */
   public Piano()
   {
      this((Integer) null, (Integer) null);
      man = ColoredKeyManager.getInstance();
      man.init(keyColors2);
   }

   public Piano(Integer xPosition, Integer yPosition)
   {
      double screenwidth = JSheetMusicWindow.getScreenDimensions().getWidth();
      screenwidth = screenwidth * 95 / 100;
      WhiteKeyWidth = (int) (screenwidth / (2.0 + KeysPerOctave * MaxOctave));
      if (WhiteKeyWidth % 2 != 0)
      {
         WhiteKeyWidth--;
      }

      margin = 0;
      BlackBorder = WhiteKeyWidth / 2;
      WhiteKeyHeight = WhiteKeyWidth * 5;
      BlackKeyWidth = WhiteKeyWidth / 2;
      BlackKeyHeight = WhiteKeyHeight * 5 / 9;

      int Width = margin * 2 + BlackBorder * 2 + WhiteKeyWidth * KeysPerOctave * MaxOctave;
      int Height = margin * 2 + BlackBorder * 3 + WhiteKeyHeight;
      ypos = Height;

      if (yPosition != null)
      {
         KeysDrawStartY = yPosition.intValue();
         ypos = KeysDrawStartY - Height;
      }

      if (xPosition != null)
      {
         KeysDrawStartX = xPosition.intValue();
      }

      setSize(Width, ypos);

      if (blackKeyOffsets == null)
      {
         blackKeyOffsets = new int[] { WhiteKeyWidth - BlackKeyWidth / 2 - 1,
            WhiteKeyWidth + BlackKeyWidth / 2 - 1, 2 * WhiteKeyWidth - BlackKeyWidth / 2,
            2 * WhiteKeyWidth + BlackKeyWidth / 2, 4 * WhiteKeyWidth - BlackKeyWidth / 2 - 1,
            4 * WhiteKeyWidth + BlackKeyWidth / 2 - 1, 5 * WhiteKeyWidth - BlackKeyWidth / 2,
            5 * WhiteKeyWidth + BlackKeyWidth / 2, 6 * WhiteKeyWidth - BlackKeyWidth / 2,
            6 * WhiteKeyWidth + BlackKeyWidth / 2 };
      }
      // gray1Pen = new Color(16, 16, 16);
      // gray2Pen = new Color(90, 90, 90);
      // gray3Pen = new Color(135, 135, 135);
      // shade1Pen = new Color(210, 205, 220);
      // shade2Pen = new Color(150, 200, 220);
      gray1Pen = new Color(16, 16, 16);
      gray2Pen = new Color(90, 90, 90);
      gray3Pen = new Color(135, 135, 135);

      showNoteLetters = -1;
      setBackground(Color.LIGHT_GRAY);
   }

   @Override
   public int getHeight()
   {
      return super.getHeight() + KeysDrawStartY;
   }

   @Override
   public int getWidth()
   {
      return super.getWidth() + KeysDrawStartX;
   }

   /**
    * Set the MidiFile to use. Save the list of midi notes. Each midi note
    * includes the note Number and StartTime (in pulses), so we know which notes
    * to shade given the current pulse time.
    */
   public void SetMidiFile(MidiFileJavax midifile, MidiOptions options)
   {
      if (midifile == null)
      {
         notes = null;
         useTwoColors = false;
         return;
      }

      List<MidiTrack> tracks = null;
      try
      {
         tracks = midifile.ChangeMidiNotes(options);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      MidiTrack track = MidiFileJavax.CombineToSingleTrack(tracks);
      notes = track.getNotes();

      maxShadeDuration = midifile.getTime().getQuarter() * 2;

      /*
       * We want to know which track the note came from. Use the 'channel' field
       * to store the track.
       */
      for (int tracknum = 0; tracknum < tracks.size(); tracknum++)
      {
         for (MidiNote note : tracks.get(tracknum).getNotes())
         {
            note.setChannel(tracknum);
         }
      }

      /*
       * When we have exactly two tracks, we assume this is a piano song, and we
       * use different colors for highlighting the left hand and right hand
       * notes.
       */
      useTwoColors = false;
      if (tracks.size() == 2)
      {
         useTwoColors = true;
      }

      showNoteLetters = options.showNoteLetters;
      this.invalidate();
   }

   /** Draw the outline of a 12-note (7 white note) piano octave */
   private void DrawOctaveOutline(Graphics g)
   {
      int right = WhiteKeyWidth * KeysPerOctave;

      // Draw the bounding rectangle, from C to B

      g.setColor(gray1Pen);
      g.drawLine(0, 0, 0, WhiteKeyHeight);
      g.drawLine(right, 0, right, WhiteKeyHeight);
      g.drawLine(0, 0, right, 0);
      g.drawLine(0, WhiteKeyHeight, right, WhiteKeyHeight);
      g.setColor(gray3Pen);
      g.drawLine(right - 1, 0, right - 1, WhiteKeyHeight);
      g.drawLine(1, 0, 1, WhiteKeyHeight);

      // Draw the line between E and F
      g.setColor(gray1Pen);
      g.drawLine(3 * WhiteKeyWidth, 0, 3 * WhiteKeyWidth, WhiteKeyHeight);
      g.setColor(gray3Pen);
      g.drawLine(3 * WhiteKeyWidth - 1, 0, 3 * WhiteKeyWidth - 1, WhiteKeyHeight);
      g.drawLine(3 * WhiteKeyWidth + 1, 0, 3 * WhiteKeyWidth + 1, WhiteKeyHeight);

      // Draw the sides/bottom of the black keys
      for (int i = 0; i < 10; i += 2)
      {
         int x1 = blackKeyOffsets[i];
         int x2 = blackKeyOffsets[i + 1];

         g.setColor(gray1Pen);
         g.drawLine(x1, 0, x1, BlackKeyHeight);
         g.drawLine(x2, 0, x2, BlackKeyHeight);
         g.drawLine(x1, BlackKeyHeight, x2, BlackKeyHeight);
         g.setColor(gray2Pen);
         g.drawLine(x1 - 1, 0, x1 - 1, BlackKeyHeight + 1);
         g.drawLine(x2 + 1, 0, x2 + 1, BlackKeyHeight + 1);
         g.drawLine(x1 - 1, BlackKeyHeight + 1, x2 + 1, BlackKeyHeight + 1);
         g.setColor(gray3Pen);
         g.drawLine(x1 - 2, 0, x1 - 2, BlackKeyHeight + 2);
         g.drawLine(x2 + 2, 0, x2 + 2, BlackKeyHeight + 2);
         g.drawLine(x1 - 2, BlackKeyHeight + 2, x2 + 2, BlackKeyHeight + 2);
      }

      // Draw the bottom-half of the white keys
      for (int i = 1; i < KeysPerOctave; i++)
      {
         if (i == 3)
         {
            continue; // we draw the line between E and F above
         }
         g.setColor(gray1Pen);
         g.drawLine(i * WhiteKeyWidth, BlackKeyHeight, i * WhiteKeyWidth, WhiteKeyHeight);
         g.setColor(gray2Pen);
         g.drawLine(i * WhiteKeyWidth - 1, BlackKeyHeight + 1, i * WhiteKeyWidth - 1,
               WhiteKeyHeight);
         g.setColor(gray3Pen);
         g.drawLine(i * WhiteKeyWidth + 1, BlackKeyHeight + 1, i * WhiteKeyWidth + 1,
               WhiteKeyHeight);
      }

   }

   /** Draw an outline of the piano for 7 octaves */
   private void DrawOutline(Graphics g)
   {
      for (int octave = 0; octave < MaxOctave; octave++)
      {
         g.translate(octave * WhiteKeyWidth * KeysPerOctave, 0);
         DrawOctaveOutline(g);
         g.translate(-(octave * WhiteKeyWidth * KeysPerOctave), 0);
      }
   }

   /* Draw the Black keys */
   private void DrawBlackKeys(Graphics g, boolean isPlayedNow)
   {
      for (int octave = 0; octave < MaxOctave; octave++)
      {
         g.translate(octave * WhiteKeyWidth * KeysPerOctave, 0);
         for (int i = 0; i < 10; i += 2)
         {
            int x1 = blackKeyOffsets[i];
            colorBlackKey(g, x1, 0, BlackKeyWidth, BlackKeyHeight, man.getBlackKeyColor(i / 2),
                  isPlayedNow);
            g.setColor(gray3Pen);
            g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                  BlackKeyHeight / 8);
         }
         g.translate(-(octave * WhiteKeyWidth * KeysPerOctave), 0);
      }
   }

   public void colorBlackKey(Graphics g, int x1, int y1, int x2, int y2, Color brush,
         boolean isPlayedNow)
   {
      if (isColored)
      {
         if (isPlayedNow)
         {
            g.setColor(brush);
            g.fillRect(x1, y1, x2, y2 / 2);
         }
         else
         {
            g.setColor(gray1Pen);
            g.fillRect(x1, y1, x2, y2 / 2);
            g.setColor(brush);
            g.fillRect(x1, y2 / 2, x2, y2 / 2);
         }
      }
      else
      {
         if (isPlayedNow)
         {
            g.setColor(brush);
         }
         else
         {
            g.setColor(gray1Pen);
         }
         g.fillRect(x1, y1, x2, y2);
      }

      g.setColor(gray3Pen);
      g.fillRect(x1 + 1, y2 - y2 / 8, x2 - 2, y2 / 8);
   }

   /*
    * Draw the black border area surrounding the piano keys. Also, draw gray
    * outlines at the bottom of the white keys.
    */
   private void DrawBlackBorder(Graphics g)
   {
      int PianoWidth = WhiteKeyWidth * KeysPerOctave * MaxOctave;
      g.setColor(gray1Pen);
      g.fillRect(margin, margin, PianoWidth + BlackBorder * 2, BlackBorder - 2);
      g.fillRect(margin, margin, BlackBorder, WhiteKeyHeight + BlackBorder * 3);
      g.fillRect(margin, margin + BlackBorder + WhiteKeyHeight, BlackBorder * 2 + PianoWidth,
            BlackBorder * 2);
      g.fillRect(margin + BlackBorder + PianoWidth, margin, BlackBorder,
            WhiteKeyHeight + BlackBorder * 3);
      g.setColor(gray2Pen);
      g.drawLine(margin + BlackBorder, margin + BlackBorder - 1, margin + BlackBorder + PianoWidth,
            margin + BlackBorder - 1);

      g.translate(margin + BlackBorder, margin + BlackBorder);

      // Draw the gray bottoms of the white keys
      for (int i = 0; i < KeysPerOctave * MaxOctave; i++)
      {
         g.fillRect(i * WhiteKeyWidth + 1, WhiteKeyHeight + 2, WhiteKeyWidth - 2, BlackBorder / 2);
      }
      g.translate(-(margin + BlackBorder), -(margin + BlackBorder));
   }

   /** Draw the note letters underneath each white note */
   private void DrawNoteLetters(Graphics g)
   {
      String[] letters = { "C", "D", "E", "F", "G", "A", "B" };
      String[] numbers = { "1", "3", "5", "6", "8", "10", "12" };
      String[] names;
      if (showNoteLetters == MidiOptions.NoteNameLetter)
      {
         names = letters;
      }
      else if (showNoteLetters == MidiOptions.NoteNameFixedNumber)
      {
         names = numbers;
      }
      else
      {
         return;
      }
      g.translate(margin + BlackBorder, margin + BlackBorder);
      System.out.println("Draw");
      for (int octave = 0; octave < MaxOctave; octave++)
      {
         for (int i = 0; i < KeysPerOctave; i++)
         {
            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 8));
            g.drawString(names[i], (octave * KeysPerOctave + i) * WhiteKeyWidth + WhiteKeyWidth / 3,
                  WhiteKeyHeight + (BlackBorder * 3 / 4) - 2);
         }
      }
      g.translate(-(margin + BlackBorder), -(margin + BlackBorder));
   }

   /** Draw the Piano. */
   @Override
   public void paint(Graphics g)
   {
      SettingProperties prop = SettingProperties.getInstance();
      isColored = prop.getBooleanProperty(KEYBOARD_COLORED, false);
      g.translate(margin + BlackBorder + KeysDrawStartX, margin + BlackBorder + KeysDrawStartY);
      if (isColored)
      {
         int j = 0;
         for (int octave = 0; octave < MaxOctave; octave++)
         {
            for (int i = 0; i < KeysPerOctave; i++)
            {
               g.setColor(man.getWhiteKeyColor(i));
               g.fillRect(0 + j * WhiteKeyWidth, 0, WhiteKeyWidth, WhiteKeyHeight);
               j++;
            }
         }
      }
      else
      {
         g.setColor(Color.WHITE);
         g.fillRect(0, 0, WhiteKeyWidth * KeysPerOctave * MaxOctave, WhiteKeyHeight);
      }

      DrawBlackKeys(g, false);
      DrawOutline(g);
      g.translate(-(margin + BlackBorder), -(margin + BlackBorder));
      DrawBlackBorder(g);
      if (showNoteLetters != MidiOptions.NoteNameNone)
      {
         DrawNoteLetters(g);
      }
   }

   /*
    * Shade the given note with the given brush. We only draw notes from
    * notenumber 24 to 96. (Middle-C is 60).
    */
   private void ShadeOneNote(Graphics g, int notenumber, Color brush, boolean isPlayedNow)
   {
      int octave = notenumber / 12;
      int notescale = notenumber % 12;
      int leaveWhiteKey = 0;
      if (isPlayedNow && isColored)
      {
         leaveWhiteKey = 45;
      }

      octave -= 2;
      if (octave < 0 || octave >= MaxOctave)
         return;

      g.translate(octave * WhiteKeyWidth * KeysPerOctave, 0);
      int x1, x2, x3;

      int bottomHalfHeight = WhiteKeyHeight - (BlackKeyHeight + 3);

      /* notescale goes from 0 to 11, from C to B. */
      g.setColor(brush);
      switch (notescale)
      {
         case 0: /* C */
            x1 = 2;
            x2 = blackKeyOffsets[0] - 2;
            g.fillRect(x1, 0, x2 - x1, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);

            break;
         case 1: /* C# */
            x1 = blackKeyOffsets[0];
            x2 = blackKeyOffsets[1];
            colorBlackKey(g, x1, 0, x2 - x1, BlackKeyHeight, brush, isPlayedNow);

            if (brush == gray1Pen)
            {
               g.setColor(gray2Pen);
               g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                     BlackKeyHeight / 8);
            }
            break;
         case 2: /* D */
            x1 = WhiteKeyWidth + 2;
            x2 = blackKeyOffsets[1] + 3;
            x3 = blackKeyOffsets[2] - 2;
            g.fillRect(x2, 0, x3 - x2, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         case 3: /* D# */
            x1 = blackKeyOffsets[2];
            x2 = blackKeyOffsets[3];
            colorBlackKey(g, x1, 0, BlackKeyWidth, BlackKeyHeight, brush, isPlayedNow);
            if (brush == gray1Pen)
            {
               g.setColor(gray2Pen);
               g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                     BlackKeyHeight / 8);
            }
            break;
         case 4: /* E */
            x1 = WhiteKeyWidth * 2 + 2;
            x2 = blackKeyOffsets[3] + 3;
            x3 = WhiteKeyWidth * 3 - 1;
            g.fillRect(x2, 0, x3 - x2, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         case 5: /* F */
            x1 = WhiteKeyWidth * 3 + 2;
            x2 = blackKeyOffsets[4] - 2;
            x3 = WhiteKeyWidth * 4 - 2;
            g.fillRect(x1, 0, x2 - x1, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         case 6: /* F# */
            x1 = blackKeyOffsets[4];
            x2 = blackKeyOffsets[5];
            colorBlackKey(g, x1, 0, BlackKeyWidth, BlackKeyHeight, brush, isPlayedNow);
            if (brush == gray1Pen)
            {
               g.setColor(gray2Pen);
               g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                     BlackKeyHeight / 8);
            }
            break;
         case 7: /* G */
            x1 = WhiteKeyWidth * 4 + 2;
            x2 = blackKeyOffsets[5] + 3;
            x3 = blackKeyOffsets[6] - 2;
            g.fillRect(x2, 0, x3 - x2, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         case 8: /* G# */
            x1 = blackKeyOffsets[6];
            x2 = blackKeyOffsets[7];
            colorBlackKey(g, x1, 0, BlackKeyWidth, BlackKeyHeight, brush, isPlayedNow);
            if (brush == gray1Pen)
            {
               g.setColor(gray2Pen);
               g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                     BlackKeyHeight / 8);
            }
            break;
         case 9: /* A */
            x1 = WhiteKeyWidth * 5 + 2;
            x2 = blackKeyOffsets[7] + 3;
            x3 = blackKeyOffsets[8] - 2;
            g.fillRect(x2, 0, x3 - x2, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         case 10: /* A# */
            x1 = blackKeyOffsets[8];
            x2 = blackKeyOffsets[9];
            colorBlackKey(g, x1, 0, BlackKeyWidth, BlackKeyHeight, brush, isPlayedNow);
            if (brush == gray1Pen)
            {
               g.setColor(gray2Pen);
               g.fillRect(x1 + 1, BlackKeyHeight - BlackKeyHeight / 8, BlackKeyWidth - 2,
                     BlackKeyHeight / 8);
            }
            break;
         case 11: /* B */
            x1 = WhiteKeyWidth * 6 + 2;
            x2 = blackKeyOffsets[9] + 3;
            x3 = WhiteKeyWidth * KeysPerOctave - 1;
            g.fillRect(x2, 0, x3 - x2, BlackKeyHeight + 3);
            g.fillRect(x1, BlackKeyHeight + 3, WhiteKeyWidth - 3, bottomHalfHeight - leaveWhiteKey);
            break;
         default:
            break;
      }
      g.translate(-(octave * WhiteKeyWidth * KeysPerOctave), 0);
   }

   /**
    * Find the MidiNote with the startTime closest to the given time. Return the
    * index of the note. Use a binary search method.
    */
   private int FindClosestStartTime(long l)
   {
      int left = 0;
      int right = notes.size() - 1;

      while (right - left > 1)
      {
         int i = (right + left) / 2;
         if (notes.get(left).getStartTime() == l)
            break;
         else if (notes.get(i).getStartTime() <= l)
            left = i;
         else
            right = i;
      }
      while (left >= 1 && (notes.get(left - 1).getStartTime() == notes.get(left).getStartTime()))
      {
         left--;
      }
      return left;
   }

   /**
    * Return the next StartTime that occurs after the MidiNote at offset i, that
    * is also : the same track/channel.
    */
   private long NextStartTimeSameTrack(int i2)
   {
      int i = i2;
      long start = notes.get(i).getStartTime();
      long end = notes.get(i).getEndTime();
      int track = notes.get(i).getChannel();

      while (i < notes.size())
      {
         if (notes.get(i).getChannel() != track)
         {
            i++;
            continue;
         }
         if (notes.get(i).getStartTime() > start)
         {
            return notes.get(i).getStartTime();
         }
         end = Math.max(end, notes.get(i).getEndTime());
         i++;
      }
      return end;
   }

   /**
    * Return the next StartTime that occurs after the MidiNote at offset i. If
    * all the subsequent notes have the same StartTime, then return the largest
    * EndTime.
    */
   private long NextStartTime(int i2)
   {
      int i = i2;
      long start = notes.get(i).getStartTime();
      long end = notes.get(i).getEndTime();

      while (i < notes.size())
      {
         if (notes.get(i).getStartTime() > start)
         {
            return notes.get(i).getStartTime();
         }
         end = Math.max(end, notes.get(i).getEndTime());
         i++;
      }
      return end;
   }

   /**
    * Find the Midi notes that occur : the current time. Shade those notes on
    * the piano displayed. Un-shade the those notes played : the previous time.
    */
   public void ShadeNotes(long currentPulseTime, long currentPulseTime2)
   {
      if (notes == null || notes.size() == 0)
      {
         return;
      }
      graphics = getGraphics();
      graphics.translate(margin + BlackBorder, margin + BlackBorder);
      if (isColored)
      {
         shadeRightHandPen = Color.GREEN;
         shadeLeftHandPen = Color.WHITE;
      }
      else
      {
         shadeRightHandPen = Color.GREEN;
         shadeLeftHandPen = Color.BLUE;
      }

      /*
       * Loop through the Midi notes. Unshade notes where StartTime <=
       * prevPulseTime < next StartTime Shade notes where StartTime <=
       * currentPulseTime < next StartTime
       */
      int lastShadedIndex = FindClosestStartTime(currentPulseTime2 - maxShadeDuration * 2);
      for (int i = lastShadedIndex; i < notes.size(); i++)
      {
         long start = notes.get(i).getStartTime();
         long end = notes.get(i).getEndTime();
         int notenumber = notes.get(i).getNoteNumber();
         long nextStart = NextStartTime(i);
         long nextStartTrack = NextStartTimeSameTrack(i);
         end = Math.max(end, nextStartTrack);
         end = Math.min(end, start + maxShadeDuration - 1);

         /* If we've past the previous and current times, we're done. */
         if ((start > currentPulseTime2) && (start > currentPulseTime))
         {
            break;
         }

         /* If shaded notes are the same, we're done */
         if ((start <= currentPulseTime) && (currentPulseTime < nextStart)
               && (currentPulseTime < end) && (start <= currentPulseTime2)
               && (currentPulseTime2 < nextStart) && (currentPulseTime2 < end))
         {
            break;
         }

         /* If the note is : the current time, shade it */
         if ((start <= currentPulseTime) && (currentPulseTime < end))
         {
            if (useTwoColors)
            {
               if (notes.get(i).getChannel() == 1)
               {
                  ShadeOneNote(graphics, notenumber, shadeLeftHandPen, true);
               }
               else
               {
                  ShadeOneNote(graphics, notenumber, shadeRightHandPen, true);
               }
            }
            else
            {
               ShadeOneNote(graphics, notenumber, shadeLeftHandPen, true);
            }
         }

         /* If the note is : the previous time, un-shade it, draw it white. */
         else if ((start <= currentPulseTime2) && (currentPulseTime2 < end))
         {
            int num = notenumber % 12;
            SettingProperties prop = SettingProperties.getInstance();
            if (isColored)
            {
               ShadeOneNote(graphics, notenumber, man.getKeyColor(num), false);
            }
            else
            {
               ShadeOneNote(graphics, notenumber, Color.WHITE, false);
            }

         }
      }
      graphics.translate(-(margin + BlackBorder), -(margin + BlackBorder));
   }

   public static void main(String[] args)
   {
      JDialog dialog = new JDialog();
      Piano piano = new Piano(50, 500);
      dialog.add(piano);
      dialog.setSize(piano.getWidth() + 80, piano.getHeight());
      dialog.setVisible(true);
   }

   public void setShowNoteLetters(MidiOptions options)
   {
      showNoteLetters = options.showNoteLetters;
   }
}
