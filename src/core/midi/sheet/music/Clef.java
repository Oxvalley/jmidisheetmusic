package core.midi.sheet.music;

/** The possible clefs, Treble or Bass */
public enum Clef
{
    Treble, Bass
};
