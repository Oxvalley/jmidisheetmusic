package core.midi.sheet.music;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

/**
 * @class InstrumentDialog The InstrumentDialog is used to select what
 *        instrument to use for each track, when playing the music.
 */
public class InstrumentDialog extends AbstractJDialogExt
{

   /**
    * 
    */
   private static final long serialVersionUID = 7871643218547130318L;
   private JComboBox[] instrumentChoices;
   private MidiFileJavax iMidifile;

   /** The instruments to use per track */

   /**
    * Create a new Instrument Call the ShowDialog() method to display the
    */
   public InstrumentDialog(MidiFileJavax midifile)
   {

      iMidifile = midifile;
      /* Create the dialog box */
      setTitle("Choose Instruments For Each Track");
      Font font = getFont();

      int unit = 8;
      if (font != null)
      {
         setFont(new Font(font.getFamily(), Font.PLAIN, (int) (font.getSize() * 1.4)));
         unit = getFont().getSize();
      }
      int xstart = unit * 2;
      int ystart = unit * 2;
      int labelheight = unit * 2;
      int maxwidth = 0;

      ((java.awt.Frame) getOwner()).setIconImage(getToolkit().getImage("images/NotePair.ico"));
      setModal(true);
      setLayout(null);

      List<MidiTrack> tracks = midifile.getTracks();
      instrumentChoices = new JComboBox[tracks.size()];

      /*
       * For each midi track, create a label with the track number ("Track 2"),
       * and a ComboBox containing all the possible midi instruments. Add the
       * text "(default)" to the instrument specified in the midi file.
       */
      for (int i = 0; i < tracks.size(); i++)
      {
         int num = i + 1;
         JLabel label = new JLabel();
         add(label);
         label.setText("Track " + num);
         label.setHorizontalAlignment(SwingConstants.CENTER);
         // TODO Autosize...
         label.setBounds(xstart, ystart + i * labelheight, labelheight * 3, labelheight); // TODO
                                                                                          // adjust
                                                                                          // width
                                                                                          // and
                                                                                          // height
         maxwidth = Math.max(maxwidth, label.getWidth());
      }
      for (int i = 0; i < tracks.size(); i++)
      {
         instrumentChoices[i] = new JComboBox(new DefaultComboBoxModel());
         add(instrumentChoices[i]);
         instrumentChoices[i].setBounds(xstart + maxwidth * 3 / 2, ystart + i * (labelheight + 3),
               labelheight * 12, labelheight);

         for (int instr = 0; instr < MidiFileJavax.Instruments.length; instr++)
         {
            String name = MidiFileJavax.Instruments[instr];
            if (tracks.get(i).getInstrument() == instr)
            {
               name += " (default)";
            }
            instrumentChoices[i].addItem(name);
         }
         instrumentChoices[i].setSelectedIndex(tracks.get(i).getInstrument());
      }

      /* Create the "Set All To Piano" button */
      JButton allPiano = new JButton();
      add(allPiano);
      allPiano.setText("Set All To Piano");
      allPiano.setBounds(xstart + maxwidth * 3 / 2, ystart + tracks.size() * (labelheight + 3),
            labelheight * 10, labelheight);

      allPiano.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            SetAllPiano();

         }
      });

      /* Create the OK and Cancel buttons */
      int ypos = ystart + (tracks.size() + 3) * labelheight;
      JButton ok = new JButton();
      add(ok);
      ok.setText("OK");
      ok.setBounds(xstart + maxwidth * 3 / 2, ypos, labelheight * 2, labelheight);
      ok.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent e)
         {
            setOKClicked(true);
            Dispose();
         }

      });

      JButton cancel = new JButton();
      add(cancel);
      cancel.setText("Cancel");
      cancel.setBounds(xstart + maxwidth * 3 / 2 + labelheight * 2 + 20, ypos, labelheight * 3,
            labelheight);

      cancel.addActionListener(new ActionListener()
      {

         @Override
         public void actionPerformed(ActionEvent arg0)
         {
            setOKClicked(false);
            Dispose();
         }
      });

      setPreferredSize(new Dimension(
            (int) instrumentChoices[0].getLocation().getX() + instrumentChoices[0].getWidth() + 50,
            (int) cancel.getLocation().getY() + cancel.getHeight() + 50));

      pack();

   }

   /**
    * Display the Instrument Return DialogResult.OK if "OK" was clicked. Return
    * DialogResult.Cancel if "Cancel" was clicked.
    */
   @Override
   public boolean ShowDialog()
   {
      int[] oldInstruments = this.getInstruments();
      boolean result = super.ShowDialog();

      if (result == false)
      {
         /* If the user clicks 'Cancel', restore the old instruments */
         for (int i = 0; i < instrumentChoices.length; i++)
         {
            instrumentChoices[i].setSelectedIndex(oldInstruments[i]);
         }
      }
      return result;
   }

   /**
    * Set all the instrument choices to "Acoustic Grand Piano", unless the
    * instrument is Percussion (128).
    */
   private void SetAllPiano()
   {
      for (int i = 0; i < instrumentChoices.length; i++)
      {
         if (instrumentChoices[i].getSelectedIndex() != 128)
         {
            instrumentChoices[i].setSelectedIndex(0);
         }
      }
   }

   /** Get the instruments currently selected */
   public int[] getInstruments()
   {
      int[] result = new int[instrumentChoices.length];
      for (int i = 0; i < instrumentChoices.length; i++)
      {
         if (instrumentChoices[i].getSelectedIndex() == -1)
         {
            instrumentChoices[i].setSelectedIndex(0);
         }
         result[i] = instrumentChoices[i].getSelectedIndex();
      }
      return result;
   }

   /** Return true if all the default instruments are selected */
   public boolean isDefault()
   {
      boolean result = true;
      for (int i = 0; i < instrumentChoices.length; i++)
      {
         if (instrumentChoices[i].getSelectedIndex() == -1)
         {
            instrumentChoices[i].setSelectedIndex(0);
         }
         String name = (String) instrumentChoices[i].getSelectedItem();
         if (!name.contains("default"))
         {
            result = false;
         }
      }
      return result;
   }

}
