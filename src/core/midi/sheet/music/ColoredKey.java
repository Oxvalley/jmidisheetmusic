package core.midi.sheet.music;

import java.awt.Color;

public class ColoredKey extends Color
{
   private boolean iIsWhite;

   public ColoredKey(int r, int g, int b, boolean isWhite)
   {
      super(r, g, b);
      iIsWhite = isWhite;
   }

   public boolean isWhite()
   {
      return iIsWhite;
   }

}
