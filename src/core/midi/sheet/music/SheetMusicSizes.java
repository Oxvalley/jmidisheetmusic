package core.midi.sheet.music;

public class SheetMusicSizes
{

   public static int DEFAULT_NOTE_PAGE_WIDTH = 1200;

   public static final int THIN_SCORE_MUSIC_LINE = 7;

   public static final int THICK_SCORE_MUSIC_LINE = 14;

   public static final int DEFAULT_NOTE_PAGE_HEIGHT = (int) Math.round(DEFAULT_NOTE_PAGE_WIDTH / 21.0 * 29.6);

   /**
    * Master of all masters! All sizes relative to this
    */
   private int iPageWidth = DEFAULT_NOTE_PAGE_WIDTH;

   public int getPageInsetX()
   {
      return iPageWidth / 100;
   }

   public void setPageWidth(int pageWidth)
   {
      iPageWidth = pageWidth;
   }

   public int getPageInsetY()
   {
      return iPageWidth / 100;
   }

   public int getPageWidth()
   {
      return iPageWidth;
   }

   public int getLineSpace()
   {
      return (iPageWidth - getPageInsetX()) / 200;
   }

   public int getNotePageWidth()
   {
      return (int) (iPageWidth * 0.95);
   }

   public int getPenSize(int size)
   {
      return (int) Math.round(size / 6000.0 * iPageWidth);

   }

   public int getLeftMargin()
   {
      return getPageInsetX() * 2;
   }

   /** Get the width (in pixels) needed to display the key signature */
   public int KeySignatureWidth(KeySignature key)
   {
      ClefSymbol clefsym = new ClefSymbol(Clef.Treble, 0, false, this);
      int result = clefsym.getMinWidth();
      AccidSymbol[] keys = key.GetSymbols(Clef.Treble);
      for (AccidSymbol symbol : keys)
      {
         result += symbol.getMinWidth();
      }
      return result + getLeftMargin();
   }

   public int getLineWidth()
   {
      return getPenSize(THIN_SCORE_MUSIC_LINE);
   }

   public int getTitleHeight()
   {
      return getPenSize(THICK_SCORE_MUSIC_LINE);
   }

   public int getNoteHeight()
   {
      return getLineSpace() + getLineWidth();
   }

   public int getNoteWidth()
   {
      return 3 * getLineSpace() / 2;
   }
   
   public void setZoom(double value)
   {
     setPageWidth((int) Math.round(value * 76.0 + 400.0));
   }


   public int getStaffHeight()
   {
      return getLineSpace() * 4 + getLineWidth() * 5;
   }

    public int getPageHeight()
   {
      // Standard A4 21 cm wide, 29.6 cm heigh
      return (int) Math.round(iPageWidth / 21.0 * 29.6);
   }

}
