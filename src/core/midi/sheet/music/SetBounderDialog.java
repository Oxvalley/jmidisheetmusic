package core.midi.sheet.music;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class SetBounderDialog extends JDialog
{

   /**
    * 
    */
   private static final long serialVersionUID = 4529038061620899015L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;
   private JTextField textField_1;
   private JTextField textField_2;
   private JTextField textField_3;
   private JPanel panel;
   private JTextField textField_4;
   private JButton clearButton;
   private static double KONVERSION_NUMBER = 3.57;

   /**
    * Launch the application.
    */
   public static void main(String[] args)
   {
      try
      {
         SetBounderDialog dialog = new SetBounderDialog();
         dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
         dialog.setVisible(true);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Create the dialog.
    */
   public SetBounderDialog()
   {
      setTitle("Set Bounds from x, y, width & height, with factor " + KONVERSION_NUMBER);
      setBounds(100, 100, 650, 300);
      getContentPane().setLayout(new BorderLayout());
      contentPanel.setLayout(new FlowLayout());
      contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      getContentPane().add(contentPanel, BorderLayout.NORTH);
      {
         textField = new JTextField();
         textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
               recalculate();
            }
          });
         contentPanel.add(textField);
         textField.setColumns(10);
      }
      {
         textField_1 = new JTextField();
         textField_1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
               recalculate();
            }
          });
         textField_1.setColumns(10);
         contentPanel.add(textField_1);
      }
      {
         textField_2 = new JTextField();
         textField_2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
               recalculate();
            }
          });
         textField_2.setColumns(10);
         contentPanel.add(textField_2);
      }
      {
         textField_3 = new JTextField();
         textField_3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
               recalculate();
            }
          });
         textField_3.setColumns(10);
         contentPanel.add(textField_3);
      }
      {
         JPanel buttonPane = new JPanel();
         buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
         getContentPane().add(buttonPane, BorderLayout.SOUTH);
         {
            clearButton = new JButton("Clear");
            clearButton.addActionListener(new ActionListener()
            {
               
               @Override
               public void actionPerformed(ActionEvent arg0)
               {
                  textField.setText(null);
                  textField_1.setText(null);
                  textField_2.setText(null);
                  textField_3.setText(null);
                  recalculate();
                  textField.requestFocusInWindow();
               }
            });
            
            buttonPane.add(clearButton);
            getRootPane().setDefaultButton(clearButton);
         }
         {
            JButton closeButton = new JButton("Close");
            closeButton.addActionListener(new ActionListener()
            {
               
               @Override
               public void actionPerformed(ActionEvent arg0)
               {
                  setVisible(false);
                  System.exit(0);
               }
            });
            buttonPane.add(closeButton);
         }
      }
      {
         panel = new JPanel();
         getContentPane().add(panel, BorderLayout.CENTER);
         {
            textField_4 = new JTextField();
            textField_4.addFocusListener(new FocusAdapter() {
               @Override
               public void focusGained(FocusEvent arg0) {
                  clearButton.requestFocusInWindow();

               }
            });
            textField_4.setColumns(45);
            panel.add(textField_4);
         }
      }
      recalculate();
   }
   
   private void recalculate()
   {
      textField_4.setText(".setBounds("+ getCalc(textField)+ ", "+ getCalc(textField_1)+ ", "+ getCalc(textField_2)+ ", "+ getCalc(textField_3)+ ");");
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Clipboard clipboard = toolkit.getSystemClipboard();
      StringSelection strSel = new StringSelection(textField_4.getText());
      clipboard.setContents(strSel, null);
   }
   
   

   private static String getCalc(JTextField textFieldObj)
   {
      String value = textFieldObj.getText();
      if (value == null  || value.length() == 0 || !isDigit(value))
      {
         return "-1";
      }
      else{
         return String.valueOf(Math.round(Double.parseDouble(value)*KONVERSION_NUMBER/5)*5);
      }
   }

   private static boolean isDigit(String value)
   {
      try
      {
         Double.parseDouble(value);
      }
      catch (NumberFormatException e)
      {
         return false;
      }
      return true;
   }


}
