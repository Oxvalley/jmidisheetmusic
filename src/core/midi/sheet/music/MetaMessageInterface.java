package core.midi.sheet.music;

import javax.sound.midi.MetaMessage;

public interface MetaMessageInterface
{
   MetaMessage getMetaMessage();
   String toStringMetaMessage();
   double getStartTime();
}
