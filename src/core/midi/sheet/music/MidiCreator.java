package core.midi.sheet.music;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

public class MidiCreator extends ArrayList<MidiTrack>
{
   /**
     *
     */
   private static final long serialVersionUID = 1123498006838403839L;

   private static int transformMilliSecToTicks(double milliSec)
   {
      // The formula is 60000 / (BPM * PPQ) (milliseconds).
      // Where BPM is the tempo of the track (Beats Per Minute).
      // (i.e. a 120 BPM track would have a MIDI time of (60000 / (120 * 192))
      // or 2.604 ms for 1 tick.
      // (i.e. Fur Elise: Tempo = 1 100 000 ms /quarter note.
      // BPM= 60 000 000 / 1 100 000 = 54.55 BPM.
      // Track would have a MIDI time of (60 000 / (54.55 * 16)) = 68.74 ms for
      // 1 tick.

      return (int) Math.round(milliSec);

   }
//
   private final String iFilename;
//
   private Sequence sequence;
//
   public MidiCreator(String filename)
   {
      iFilename = filename;
   }

   private void createFile() throws IOException
   {

      // A file name was specified, so save the notes
      int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
      if (allowedTypes.length == 0)
      {
         System.err.println("No supported MIDI file types.");
      }
      else
      {
         MidiSystem.write(sequence, allowedTypes[0], new File(iFilename));
      }
   }

   public void createMidiFile(List<MidiTrack> tracks, int tempo, int velocity,
         float divisionType, int resolution)
   {
      // Calc factor to convert milliseconds to tick2
      // The formula is 60000 / (BPM * PPQ) (milliseconds).
      // Where BPM is the tempo of the track (Beats Per Minute).
      // (i.e. a 120 BPM track would have a MIDI time of (60000 / (120 * 192))
      // or 2.604 ms for 1 tick.
      // (i.e. Fur Elise: Tempo = 1 100 000 ms /quarter note.
      // Time Signature is 3/8
      // BPM= 60 000 000 / 1 100 000 * 8 / 4 (four is constant) = 109 BPM.
      // Track would have a MIDI time of (60 000 / (109 * 16)) = 34.40 ms for 1
      // tick.

      List<Long> starttimes = MidiFileJavax.getAllStartTimesSorted(tracks, false);

      try
      {
         sequence = new Sequence(divisionType, resolution); // 16 = ticks per
                                                            // beat

         for (int tracknum = 0; tracknum < tracks.size(); tracknum++)
         {
            Track track = sequence.createTrack(); // Begin with a new track
            MidiTrack track2 = tracks.get(tracknum);

            // set tempo
            MetaMessage mmessage = new MetaMessage();
            int l = tempo;
            mmessage.setMessage(0x51, new byte[] { (byte) (l / 65536),
                  (byte) (l % 65536 / 256), (byte) (l % 256) }, 3);
            track.add(new MidiEvent(mmessage, 0));

            // Create midifile
            for (MidiNote wt : track2.getNotes())
            {
               // Set the instrument on channel tracknum
               ShortMessage sm = new ShortMessage();
               sm.setMessage(ShortMessage.PROGRAM_CHANGE, 0,
                     track2.getInstrument(), 0);
               track.add(new MidiEvent(sm, wt.getChannel()));

               int startTick = transformMilliSecToTicks(wt.getStartTime());
               int endTick = transformMilliSecToTicks(wt.getEndTime());
               ShortMessage on = new ShortMessage();
               on.setMessage(ShortMessage.NOTE_ON, wt.getChannel(),
                     wt.getNoteNumber(), velocity);
               ShortMessage off = new ShortMessage();
               off.setMessage(ShortMessage.NOTE_OFF, wt.getChannel(),
                     wt.getNoteNumber(), velocity);
               track.add(new MidiEvent(on, startTick));
               track.add(new MidiEvent(off, endTick));
            }

            // Add events for startimes to know when to change display
            for (Long starttime : starttimes)
            {
               track.add(makeEvent(ShortMessage.CONTROL_CHANGE, 1, 127, 1,
                     transformMilliSecToTicks(starttime)));
            }

            for (ShortMessageInterface sm : track2.getShortMessages())
            {
               track.add(new MidiEvent(sm.getShortMessage(),
                     transformMilliSecToTicks(sm.getStartTime())));
            }

            for (MetaMessageInterface mm : track2.getMetaMessages())
            {
               track.add(new MidiEvent(mm.getMetaMessage(),
                     transformMilliSecToTicks(mm.getStartTime())));
            }

            // save midifile
            createFile();
         }
      }
      catch (InvalidMidiDataException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public static MidiEvent makeEvent(int comd, int chan, int one, int two,
         int tick)
   {
      MidiEvent event = null;
      try
      {
         ShortMessage a = new ShortMessage();
         a.setMessage(comd, chan, one, two);
         event = new MidiEvent(a, tick);
      } // close try
      catch (Exception e)
      {
      } // close catch

      return event;
   } // close makeEvent method

}
