package core.midi.sheet.music;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColoredKeyManager
{
   
   private static ColoredKeyManager iInstance;
   private List<ColoredKey> whiteList;
   private List<ColoredKey> blackList;
   private List<ColoredKey> allList;

   
   
   private ColoredKeyManager()
   {
      whiteList = new ArrayList<ColoredKey>();
      blackList = new ArrayList<ColoredKey>();
      allList = new ArrayList<ColoredKey>();
   }

   public static ColoredKeyManager getInstance(){
      if (iInstance == null)
      {
         iInstance = new ColoredKeyManager();
      }
      return iInstance;
   }

   public void init(ColoredKey[] colorArr)
   {
      for (ColoredKey coloredKey : colorArr)
      {
         allList.add(coloredKey);
         if (coloredKey.isWhite())
         {
            whiteList.add(coloredKey);
         }
         else
         {
            blackList.add(coloredKey);
         }
      }
   }

   public Color getKeyColor(int index)
   {
      if (allList.size()>index)
      {
         return allList.get(index);
      }
      return null;
   }

   public Color getWhiteKeyColor(int index)
   {
      return whiteList.get(index);
   }

   public Color getBlackKeyColor(int index)
   {
      return blackList.get(index);
   }

   public Color[] getColorsArray()
   {
      return allList.toArray(new Color[0]);
   }
}
