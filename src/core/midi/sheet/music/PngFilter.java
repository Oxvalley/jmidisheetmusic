package core.midi.sheet.music;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class PngFilter extends FileFilter
{

    // Accept all directories and all .mid .mid* files.
    @Override
    public boolean accept(File f)
    {
        if (f.isDirectory())
        {
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null)
        {
            if (extension.equals("png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    // The description of this filter
    @Override
    public String getDescription()
    {
        return "PNG Image Files";
    }
}
