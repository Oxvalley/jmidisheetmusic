package core.midi.sheet.music;

import javax.swing.JList;

public class JListExt extends JList
{
   /**
     *
     */
   private static final long serialVersionUID = 1L;
   SortedListModel model = new SortedListModel();

   JListExt()
   {
      setModel(model);
   }

   public void addElement(String element)
   {
      if (element.length() != 0)
      {
         model.add(element);
      }
   }

}
