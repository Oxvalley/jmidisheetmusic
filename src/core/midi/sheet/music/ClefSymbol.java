package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * @class ClefSymbol A ClefSymbol represents either a Treble or Bass Clef image.
 *        The clef can be either normal or small size. Normal size is used at
 *        the beginning of a new staff, on the left side. The small symbols are
 *        used to show clef changes within a staff.
 */

public class ClefSymbol extends MusicSymbol
{
   private static BufferedImage treble;
   /** The treble clef image */
   private static BufferedImage bass;
   /** The bass clef image */

   private long starttime;
   /** Start time of the symbol */
   private boolean smallsize;
   /** True if this is a small clef, false otherwise */
   private Clef clef;
   /** The clef, Treble or Bass */
   private int width;

   /** Create a new ClefSymbol, with the given clef, starttime, and size */
   public ClefSymbol(Clef clef, long l, boolean small, SheetMusicSizes sizes)
   {
      iSizes = sizes;
      this.clef = clef;
      this.starttime = l;
      smallsize = small;
      LoadImages();
      width = getMinWidth();
   }

   /** Load the Treble/Bass clef images into memory. */
   private static void LoadImages()
   {
      if (treble == null)
         treble = ImageHelper.loadImage("images/treble.png");

      if (bass == null)
         bass = ImageHelper.loadImage("images/bass.png");

   }

   /**
    * Get the time (in pulses) this symbol occurs at. This is used to determine
    * the measure this symbol belongs to.
    */
   @Override
   public long getStartTime()
   {
      return starttime;
   }

   /** Get the minimum width (in pixels) needed to draw this symbol */
   @Override
   public int getMinWidth()
   {

      if (smallsize)
         return iSizes.getNoteWidth() * 2;
      else
         return iSizes.getNoteWidth() * 3;

   }

   /**
    * Get/Set the width (in pixels) of this symbol. The width is set in
    * SheetMusic.AlignSymbols() to vertically align symbols.
    */
   @Override
   public int getWidth()
   {
      return width;
   }

   @Override
   public void setWidth(int value)
   {
      width = value;
   }

   /**
    * Get the number of pixels this symbol extends above the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getAboveStaff()
   {

      if (clef == Clef.Treble && !smallsize)
         return iSizes.getNoteHeight() * 2;
      else
         return 0;

   }

   /**
    * Get the number of pixels this symbol extends below the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getBelowStaff()
   {

      if (clef == Clef.Treble && !smallsize)
         return iSizes.getNoteHeight() * 2;
      else if (clef == Clef.Treble && smallsize)
         return iSizes.getNoteHeight();
      else
         return 0;
   }

   /**
    * Draw the symbol.
    *
    * @param ytop
    *           The y location (in pixels) where the top of the staff starts.
    */
   @Override
   public void Draw(Graphics g, Color pen, int ytop)
   {
      g.translate(getWidth() - getMinWidth(), 0);
      int y = ytop;
      BufferedImage image;
      int height;

      /*
       * Get the image, height, and top y pixel, depending on the clef and the
       * image size.
       */
      if (clef == Clef.Treble)
      {
         image = treble;
         if (smallsize)
         {
            height = iSizes.getStaffHeight() + iSizes.getStaffHeight() / 4;
         }
         else
         {
            height = 3 * iSizes.getStaffHeight() / 2 + iSizes.getNoteHeight() / 2;
            y = ytop - iSizes.getNoteHeight();
         }
      }
      else
      {
         image = bass;
         if (smallsize)
         {
            height = iSizes.getStaffHeight() - 3 * iSizes.getNoteHeight() / 2;
         }
         else
         {
            height = iSizes.getStaffHeight() - iSizes.getNoteHeight();
         }
      }

      /* Scale the image width to match the height */
      int imgwidth = image.getWidth() * height / image.getHeight();
      g.drawImage(image, 0, y, imgwidth, height, null);
      g.translate(-(getWidth() - getMinWidth()), 0);
   }

   @Override
   public String toString()
   {
      return String.format("ClefSymbol clef=" + clef + " small=" + smallsize + " width=" + width);
   }
}
