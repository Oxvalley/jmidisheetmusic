package core.midi.sheet.music;

/* Windows XP uses the MIXERCONTROL structs to set the Volume */
public class MIXERCONTROLDETAILS
{
    public int cbStruct;
    public int dwControlID;
    public int cChannels;
    public int item;
    public int cbDetails;
}
