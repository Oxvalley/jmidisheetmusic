package core.midi.sheet.music;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

public class ReadMidi
{
    public static final int NOTE_ON = 0x90;
    public static final int NOTE_OFF = 0x80;
    public static final String[] NOTE_NAMES =
        { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

    public static void main(String[] args) throws Exception
    {

        if (args == null || args.length == 0)
        {
            System.out.println("Usage: ReadMidi PathTpMidiFile");
            System.exit(1);
        }

        logMidiFileNotes(args[0]);
    }

    private static void logMidiFileNotes(String fileName)
        throws InvalidMidiDataException, IOException
    {
        System.err.println("Analysing file: " + fileName);
        Sequence sequence = MidiSystem.getSequence(new File(fileName));

        int trackNumber = 0;
        for (Track track : sequence.getTracks())
        {
            trackNumber++;
            System.out.println("Track " + trackNumber + ": size = " + track.size());
            System.out.println();
            for (int i = 0; i < track.size(); i++)
            {
                MidiEvent event = track.get(i);
                System.out.print("@" + event.getTick() + " ");
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage)
                {
                    ShortMessage sm = (ShortMessage) message;
                    System.out.print("Channel: " + sm.getChannel() + " ");
                    if (sm.getCommand() == NOTE_ON)
                    {
                        int key = sm.getData1();
                        int octave = (key / 12) - 1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        int velocity = sm.getData2();
                        System.out.println("Note on, " + noteName + octave + " key=" + key +
                            " velocity: " + velocity);
                    }
                    else if (sm.getCommand() == NOTE_OFF)
                    {
                        int key = sm.getData1();
                        int octave = (key / 12) - 1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        int velocity = sm.getData2();
                        System.out.println("Note off, " + noteName + octave + " key=" + key +
                            " velocity: " + velocity);
                    }
                    else
                    {
                        System.out.println("Command:" + sm.getCommand() + " " + sm.getData1() +
                            " " + sm.getData2());
                    }
                }
                else
                {
                    System.out.println("Other message: " + message.getClass());
                }
            }

            System.out.println();
        }
    }

    public static int getCommandsPerNoteOn(String fileName)
    {

        Sequence sequence = null;
        try
        {
            sequence = MidiSystem.getSequence(new File(fileName));
        }
        catch (InvalidMidiDataException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Map<String, Integer> commandPerNoteOn = new HashMap<String, Integer>();

        for (Track track : sequence.getTracks())
        {
            for (int i = 0; i < track.size(); i++)
            {
                MidiEvent event = track.get(i);
                String timeLabel = "@" + event.getTick();
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage)
                {
                    ShortMessage sm = (ShortMessage) message;
                    if (sm.getCommand() == 176 && sm.getData1() == 127 && sm.getData2() == 1)
                    {
                        Integer count = commandPerNoteOn.get(timeLabel);
                        if (count == null)
                        {
                            count = new Integer(1);
                        }
                        else
                        {
                            count = new Integer(count.intValue() + 1);
                        }
                        commandPerNoteOn.put(timeLabel, count);
                    }
                }
            }
        }

        Integer myCount = -1;
        if (commandPerNoteOn.size() > 0)
        {
            myCount = commandPerNoteOn.values().iterator().next();
        }

        return myCount.intValue();

    }

}