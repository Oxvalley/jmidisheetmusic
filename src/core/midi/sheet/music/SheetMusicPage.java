package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.List;

import javax.swing.JPanel;

public class SheetMusicPage extends JPanel
{

   /**
    *
    */
   private static final long serialVersionUID = 2610344527946827444L;
   private List<Staff> iStaffs;
   private SheetMusicSizes iSizes;
   private Color iPen;
   private String iSongTitle;
   private int iPageNumber;
   private int iPageCount;

   public SheetMusicPage(List<Staff> onePageStaffList, SheetMusicSizes sizes,
         Color pen, String songName, int pageNumber)
   {
      this.iStaffs = onePageStaffList;
      this.iSizes = sizes;
      this.iPen = pen;
      this.iSongTitle = songName;
      this.iPageNumber = pageNumber;

   }

   public void paintPage(Graphics g, long currentTimePosition, ScrollerInterface scroller)
   {
      Rectangle iClip = new Rectangle(0, 0, iSizes.getPageWidth(),
            iSizes.getPageHeight());

      // Paint a grey/blue background
      g.setColor(new Color(144,153,174));
      g.fillRect((int) iClip.getX(), (int) iClip.getY(),
            (int) iClip.getWidth(), (int) iClip.getHeight());

      paintNoteSheet(g,currentTimePosition, scroller);
   }

   private void paintNoteSheet(Graphics g, long currentTimePosition, ScrollerInterface scroller)
   {
      Rectangle iClip = new Rectangle(0, 0, iSizes.getPageWidth(),
            iSizes.getPageHeight());
      g.setColor(Color.WHITE);
      g.fillRect((int) iClip.getX() + iSizes.getPageInsetX(),
            (int) iClip.getY() + iSizes.getPageInsetY(), (int) iClip.getWidth()
                  - iSizes.getPageInsetX() * 2, (int) iClip.getHeight()
                  - iSizes.getPageInsetY() * 2);

      int ypos = 0;
      g.setColor(Color.BLACK);

      String fontName = "Blackadder ITC"; // TODO if this does not exist

      Font font = new Font(fontName, Font.PLAIN,
            Math.round(iSizes.getPageWidth() / 23));
      g.setFont(font);

      g.drawString(iSongTitle, iSizes.getLeftMargin(), (int) (iStaffs.get(0)
            .getHeight() / 1.1));

      font = new Font(fontName, Font.PLAIN, (int) Math.round(iSizes
            .getPageWidth() / 27.0));
      g.setFont(font);
      g.drawString(iPageNumber + "/" + iPageCount,
            (int) (iSizes.getPageWidth() * .9), (int) (iStaffs.get(0)
                  .getHeight() / 1.1));

      if (iStaffs.size() > 0)
      {
         ypos = iStaffs.get(0).getHeight();
      }

      for (Staff staff : iStaffs)
      {
         g.translate( 0, ypos);
         staff.Draw(g, iPen,currentTimePosition, scroller);
         g.translate( 0, -ypos);

         ypos += staff.getHeight();
      }
   }

   @Override
   public int getHeight()
   {
      return iSizes.getPageHeight();
   }

   public void setPageCount(int count)
   {
      iPageCount = count;
   }

   @Override
   public void paintComponent(Graphics g)
   {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      paintNoteSheet(g2d,-1, null);
   }

}
