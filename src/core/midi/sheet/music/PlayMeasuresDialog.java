package core.midi.sheet.music;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan
 * Java port   Copyright(c) 2012 Lars Svensson
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2.
 *
 *  This program is distributed : the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

/**
 * @class PlayMeasuresInLoop This class displays the dialog used for the "Play Measures in Loop"
 *        feature. It displays: - A checkbox to enable this feature - Two numeric spinboxes, to
 *        select the start and end measures.
 *
 *        When the user clicks OK: - IsEnabled() returns true if the "play in loop" feature is
 *        enabled. - GetStartMeasure() returns the start measure of the loop - GetEndMeasure()
 *        returns the end measure of the loop
 */
public class PlayMeasuresDialog extends JDialog
{

    /**
    *
    */
    private static final long serialVersionUID = -8189285312610476824L;
    private JDialog dialog;
    /** The dialog box */
    private JSpinner startMeasure;
    /** The starting measure */
    private JSpinner endMeasure;
    /** The ending measure */
    private JCheckBox enable;

    /** Whether to enable or not */

    /**
     * Create a new PlayMeasuresDialog. Call the ShowDialog() method to display the dialog.
     */
    public PlayMeasuresDialog(MidiFileJavax midifile)
    {
        long lastStart = midifile.EndTime();
        long lastMeasure = 1 + lastStart / midifile.getTime().getMeasure();

        /* Create the dialog box */
        dialog = new JDialog();
        dialog.setTitle("Play Selected Measures in a Loop");
        dialog.setResizable(false);
        ((java.awt.Frame) dialog.getOwner()).setIconImage(dialog.getToolkit().getImage(
                "images/NotePair.ico"));
        dialog.setModal(true);
        dialog.setLayout(null);

        Font font = dialog.getFont();
        dialog.setFont(new Font(font.getFamily(), Font.PLAIN, (int) (font.getSize() * 1.4)));
        int labelheight = dialog.getFont().getSize() * 2;
        int xpos = labelheight / 2;
        int ypos = labelheight / 2;

        enable = new JCheckBox();
        dialog.add(enable);
        enable.setText("Play Selected Measures in a Loop");
        enable.setSelected(false);
        enable.setLocation(new Point(xpos, ypos));
        enable.setSize(new Dimension(labelheight * 9, labelheight));

        ypos += labelheight * 3 / 2;

        JLabel label = new JLabel();
        dialog.add(enable);
        label.setText("Start Measure");
        label.setLocation(new Point(xpos, ypos));
        label.setSize(new Dimension(labelheight * 3, labelheight));

        xpos += labelheight * 4;

        startMeasure = new JSpinner();
        dialog.add(startMeasure);
        SpinnerModel model = new SpinnerNumberModel(1, // initial value
                1, // min
                lastMeasure, // max
                1); // step
        startMeasure.setModel(model);
        startMeasure.setLocation(new Point(xpos, ypos));
        startMeasure.setSize(new Dimension(labelheight * 2, labelheight));

        xpos = labelheight / 2;
        ypos += labelheight * 3 / 2;

        label = new JLabel();
        dialog.add(label);
        label.setText("End Measure");
        label.setLocation(new Point(xpos, ypos));
        label.setSize(new Dimension(labelheight * 3, labelheight));

        xpos += labelheight * 4;
        endMeasure = new JSpinner();
        dialog.add(endMeasure);
        SpinnerModel endmodel = new SpinnerNumberModel(lastMeasure, // initial value
                1, // min
                lastMeasure, // max
                1); // step
        startMeasure.setModel(endmodel);
        endMeasure.setLocation(new Point(xpos, ypos));
        endMeasure.setSize(new Dimension(labelheight * 2, labelheight));

        /* Create the OK and Cancel buttons */
        xpos = labelheight / 2;
        ypos += labelheight * 3 / 2;
        JButton ok = new JButton();
        dialog.add(ok);
        ok.setText("OK");
        ok.setLocation(new Point(xpos, ypos));

        dialog.setSize(new Dimension(labelheight * 10, labelheight * 8));

    }

    /**
     * Display the InstrumentDialog. This always returns DialogResult.OK.
     */
    public void ShowDialog()
    {
        dialog.setVisible(true);
    }

    /** Get the enabled value */
    public boolean IsEnabled()
    {
        return enable.isSelected();
    }

    /**
     * Get the start measure. Internally we count measures starting from 0, so decrement the value
     * by 1.
     */
    public long GetStartMeasure()
    {
        return  Math.round(Double.parseDouble(startMeasure.getModel().getValue().toString())) - 1;
    }

    /**
     * Get the end measure. Internally we count measures starting from 0, so decrement the value by
     * 1.
     */
    public int GetEndMeasure()
    {
        return Integer.parseInt(endMeasure.getModel().getValue().toString()) - 1;
    }

    public void Dispose()
    {
        dialog.dispose();
    }

}
