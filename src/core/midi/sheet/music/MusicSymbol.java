package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @class MusicSymbol The MusicSymbol class represents music symbols that can be displayed on a
 *        staff. This includes: - Accidental symbols: sharp, flat, natural - Chord symbols: single
 *        notes or chords - Rest symbols: whole, half, quarter, eighth - Bar symbols, the vertical
 *        bars which delimit measures. - Treble and Bass clef symbols - Blank symbols, used for
 *        aligning notes in different staffs
 */

public abstract class MusicSymbol
{

    protected SheetMusicSizes iSizes;

    /**
     * Get the time (in pulses) this symbol occurs at. This is used to determine the measure this
     * symbol belongs to.
     */
    public abstract long getStartTime();

    /** Get the minimum width (in pixels) needed to draw this symbol */
    public abstract int getMinWidth();

    /**
     * Get/Set the width (in pixels) of this symbol. The width is set in SheetMusic.AlignSymbols()
     * to vertically align symbols.
     */
    public abstract int getWidth();

    public abstract void setWidth(int width);

    /**
     * Get the number of pixels this symbol extends above the staff. Used to determine the minimum
     * height needed for the staff (Staff.FindBounds).
     */
    public abstract int getAboveStaff();

    /**
     * Get the number of pixels this symbol extends below the staff. Used to determine the minimum
     * height needed for the staff (Staff.FindBounds).
     */
    public abstract int getBelowStaff();

    /**
     * Draw the symbol.
     *
     * @param ytop
     *            The ylocation (in pixels) where the top of the staff starts.
     */
    public abstract void Draw(Graphics g, Color pen, int ytop);


}
