package core.midi.sheet.music;

/*
 * C# original Copyright (c) 2007-2012 Madhav Vaidyanathan Java port Copyright(c) 2012 Lars Svensson
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License version 2.
 *
 * This program is distributed : the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 */

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.util.List;

import javax.sound.midi.ControllerEventListener;
import javax.sound.midi.ShortMessage;
import javax.sound.sampled.FloatControl;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @class MidiPlayer
 *
 *        The MidiPlayer is the panel at the top used to play the sound of the midi file. It
 *        consists of:
 *
 *        - The Rewind button - The Play/Pause button - The Stop button - The Fast Forward button -
 *        The Playback speed bar - The Volume bar
 *
 *        The sound of the midi file depends on - The MidiOptions (taken from the menus) Which
 *        tracks are selected How much to transpose the keys by What instruments to use per track -
 *        The tempo (from the Speed bar) - The volume
 *
 *        The MidiFile.ChangeSound() method is used to create a new midi file with these options.
 *        The mciSendString() function is used for playing, pausing, and stopping the sound.
 *
 *        For shading the notes during playback, the method SheetMusic.ShadeNotes() is used. It
 *        takes the current 'pulse time', and determines which notes to shade.
 */
public class MidiPlayerPanel extends JPanel implements ControllerEventListener, IMidiPlayer
{
    /**
    *
    */
    private static final long serialVersionUID = 7233087565287379264L;

    private JButton rewindButton;
    /** The rewind button */
    private JButton playButton;
    /** The play/pause button */
    private JButton stopButton;
    /** The stop button */
    private JButton fastFwdButton;
    /** The fast forward button */
    private JSlider speedBar;
    /** The trackbar for controlling the playback speed */
    private JSlider volumeBar;
    /** The trackbar for controlling the volume */

    int playstate;
    /** The playing state of the Midi Player */
    static final int stopped = 1;
    /** Currently stopped */
    static final int playing = 2;
    /** Currently playing music */
    static final int paused = 3;
    /** Currently paused */
    static final int initStop = 4;
    /** Transitioning from playing to stop */
    static final int initPause = 5;
    /** Transitioning from playing to pause */

    MidiFileJavax midifile;
    /** The midi file to play */
    MidiOptions options;
    /** The sound options for playing the midi file */
    String tempSoundFile;
    /** The temporary midi filename currently being played */
    double pulsesPerMsec;
    /** The number of pulses per millisec */
    SheetMusic sheet;
    /** The sheet music to shade while playing */
    Piano piano;
    /** The piano to shade while playing */
    Date startTime;
    /** Absolute time when music started playing */
    double startPulseTime;
    /** Time (in pulses) when music started playing */
    long currentPulseTime;
    /** Time (in pulses) music is currently at */
    long prevPulseTime;
    /** Time (in pulses) music was last at */
    StringBuilder errormsg;
    /** Error messages from midi player */
    Process timidity;
    private JSlider zoomBar;
    private List<Long> iStarttimes;
    private int noteCounter;
    private int eventcount = 0;

    private MidiPlayer midiPlayer = null;

    private int lastData = -1;

    private int channelsCount;
    private static FloatControl volCtrl;

    /**
     * Create a new MidiPlayer, displaying the play/stop buttons, the speed bar, and volume bar. The
     * midifile and sheetmusic are initially null.
     *
     * @param piano2
     */
    public MidiPlayerPanel(Piano piano2)
    {
        piano = piano2;
        double screenwidth = JSheetMusicWindow.getScreenDimensions().getWidth();
        screenwidth = screenwidth * 95 / 100;
        this.setFont(new Font("Arial", Font.BOLD, 10));

        int buttonheight = this.getFont().getSize() * 3;
        int buttonwidth = this.getFont().getSize() * 3;
        int locationY = 20;

        this.midifile = null;
        this.options = null;
        this.sheet = null;
        playstate = stopped;
        startTime = new Date();
        startPulseTime = 0;
        currentPulseTime = 0;
        prevPulseTime = -10;
        errormsg = new StringBuilder(256);
        setLayout(null);

        /* Create the rewind button */
        rewindButton =
            ImageHelper.getJButtonWithImage("images/rewind.png", locationY, 20, buttonwidth,
                                            buttonheight, "Rewind");
        add(rewindButton);

        // TODO remove this line when rewind wants to work
        rewindButton.setVisible(false);

        rewindButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Rewind();
            }
        });

        /* Create the play button */
        playButton =
            ImageHelper.getJButtonWithImage("images/play.png", 65, locationY, buttonwidth,
                                            buttonheight, "Play");
        add(playButton);
        playButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                PlayPause();
            }
        });

        /* Create the stop button */
        stopButton =
            ImageHelper.getJButtonWithImage("images/stop.png", 110, locationY, buttonwidth,
                                            buttonheight, "Stop");
        add(stopButton);
        stopButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Stop();
            }
        });

        /* Create the fastFwd button */
        fastFwdButton =
            ImageHelper.getJButtonWithImage("images/fastforward.png", 155, locationY, buttonwidth,
                                            buttonheight, "Fast Forward");
        add(fastFwdButton);

        // TODO remove this line when rewind wants to work
        fastFwdButton.setVisible(false);

        fastFwdButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                FastForward();
            }
        });

        /* Create the Speed bar */
        JLabel speedLabel = new JLabel("Speed: ");
        add(speedLabel);
        speedLabel.setBounds(205, locationY, 50, buttonheight);

        speedBar = new JSlider();
        add(speedBar);
        speedBar.setMinimum(0);
        speedBar.setMaximum(200);
        speedBar.setValue(100);
        speedBar.setBounds(265, locationY - 5, 500, buttonheight + 10);
        speedBar.setToolTipText("Adjust the speed");
        speedBar.setMajorTickSpacing(10);
        speedBar.setMinorTickSpacing(5);
        speedBar.setPaintTicks(true);
        speedBar.setPaintLabels(true);

        speedBar.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                if (midiPlayer != null)
                {
                    midiPlayer.setTempoFactor(speedBar.getValue());
                }
            }
        });

        /* Create the Volume bar */
        JLabel volumeLabel1 =
            ImageHelper.getJLabelWithImage("images/volume.png", 780, locationY, buttonwidth,
                                           buttonheight, "Volume");
        add(volumeLabel1);

        volumeBar = new JSlider();
        add(volumeBar);
        volumeBar.setMinimum(0);
        volumeBar.setMaximum(200);
        volumeBar.setValue(100);
        volumeLabel1 = new JLabel();
        volumeBar.setBounds(820, locationY - 5, 500, buttonheight + 10);
        volumeBar.setToolTipText("Adjust the volume");
        volumeBar.setMajorTickSpacing(10);
        volumeBar.setMinorTickSpacing(5);
        volumeBar.setPaintTicks(true);
        volumeBar.setPaintLabels(true);

        volumeBar.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting())
                {
                    ChangeVolume();
                }
            }
        });

        /* Create the Zoom bar */
        JLabel zoomLabel = new JLabel("Zoom: ");
        add(zoomLabel);
        zoomLabel.setBounds(1335, locationY, 50, buttonheight);

        zoomBar = new JSlider();
        add(zoomBar);
        zoomBar.setMinimum(0);
        zoomBar.setMaximum(100);
        zoomBar.setValue(11);
        zoomBar.setBounds(1385, locationY - 5, 270, buttonheight + 10);
        zoomBar.setToolTipText("Zoom into the SheetMusic");
        zoomBar.setMajorTickSpacing(10);
        zoomBar.setMinorTickSpacing(5);
        zoomBar.setPaintTicks(true);
        zoomBar.setPaintLabels(true);
        zoomBar.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider source = (JSlider) e.getSource();
                ChangeZoom();
                zoomBar.setToolTipText("Zoom into the SheetMusic (" + source.getValue() + ")");
            }
        });

        setSize(piano.getWidth(), locationY + buttonheight + locationY);
        tempSoundFile = "";
    }

    /**
     * The MidiFile and/or SheetMusic has changed. Stop any playback sound, and store the current
     * midifile and sheet music.
     */
    public void SetMidiFile(MidiFileJavax midifile2, MidiOptions opt, SheetMusic s)
    {

        /*
         * If we're paused, and using the same midi file, redraw the highlighted notes.
         */
        if ((midifile2 == midifile && midifile != null && playstate == paused))
        {
            options = opt;
            sheet = s;
            sheet.resetPlayPosition();

            /*
             * We have to wait some time (200 msec) for the sheet music to scroll and redraw, before
             * we can re-shade.
             */
            final Timer redrawTimer = new Timer(200, null);
            redrawTimer.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    ReShade(redrawTimer);

                }
            });
        }
        else
        {
            midifile = midifile2;
            options = opt;
            sheet = s;
        }
        // this.DeleteSoundFile();
    }

    /**
     * If we're paused, reshade the sheet music and piano.
     *
     * @param sender
     */
    private void ReShade(Timer sender)
    {
        if (playstate == paused)
        {
            sheet.resetPlayPosition();
            piano.ShadeNotes(currentPulseTime, prevPulseTime);
        }
        Timer redrawTimer = sender;
        redrawTimer.stop();
    }

    /** Delete the temporary midi sound file */
    public void DeleteSoundFile()
    {
        if (tempSoundFile.equals(""))
        {
            return;
        }
        File soundfile = new File(tempSoundFile);
        soundfile.delete();
        tempSoundFile = "";
    }

    /**
     * Return the number of tracks selected in the MidiOptions. If the number of tracks is 0, there
     * is no sound to play.
     */
    private int numberTracks()
    {
        int count = 0;
        for (int i = 0; i < options.tracks.length; i++)
        {
            if (options.tracks[i] && !options.mute[i])
            {
                count += 1;
            }
        }
        return count;
    }

    private void createTempo()
    {
        double inverse_tempo = 1.0 / midifile.getTime().getTempo();
        double inverse_tempo_scaled = inverse_tempo * speedBar.getValue() / 100.0;
        options.tempo = (int) (1.0 / inverse_tempo_scaled);
        pulsesPerMsec = midifile.getTime().getQuarter() * (1000.0 / options.tempo);
    }

    /** Stop playing the MIDI music */
    private void StopSound()
    {
        midiPlayer.stopPlaying();
    }

    /**
     * The callback for the play/pause button (a single button). If we're stopped or pause, then
     * play the midi file. If we're currently playing, then initiate a pause. (The actual pause is
     * done when the timer is invoked).
     */
    private void PlayPause()
    {
        if (midifile == null || sheet == null || numberTracks() == 0)
        {
            return;
        }

        if (playstate == playing)
        {
            playstate = paused;
            changePlayButtonImage();
            midiPlayer.stopPlaying();
            return;
        }

        if (playstate == paused)
        {
            startPulseTime = currentPulseTime;
            options.pauseTime = currentPulseTime - options.shifttime;
            midiPlayer.continuePlaying();
            playstate = playing;
            changePlayButtonImage();
            return;
        }

        if (playstate == stopped)
        {
            /*
             * The startPulseTime is the pulse time of the midi file when we first start playing the
             * music. It's used during shading.
             */
            if (options.playMeasuresInLoop)
            {
                /*
                 * If we're playing measures in a loop, make sure the currentPulseTime is somewhere
                 * inside the loop measures.
                 */
                long measure = currentPulseTime / midifile.getTime().getMeasure();
                if ((measure < options.playMeasuresInLoopStart) ||
                    (measure > options.playMeasuresInLoopEnd))
                {
                    currentPulseTime =
                        options.playMeasuresInLoopStart * midifile.getTime().getMeasure();
                }
                startPulseTime = currentPulseTime;
                options.pauseTime = currentPulseTime - options.shifttime;
            }
        }
        else
        {
            options.pauseTime = 0;
            startPulseTime = options.shifttime;
            currentPulseTime = options.shifttime;
            prevPulseTime = options.shifttime - midifile.getTime().getQuarter();
        }

        // Start to play from the beginning
        createTempo();

        String fileFolder = MidiFileJavax.getMidiFolder();
        String filePath = fileFolder + "/" + midifile.getFileNameNoTag() + ".mid";
        MidiCreator mCrea = new MidiCreator(filePath);
        mCrea.createMidiFile(midifile.getTracks(), options.tempo, volumeBar.getValue(),
                             options.DivisionType, options.Resolution);

        midiPlayer = MidiPlayer.getInstance();
        midiPlayer.init(filePath, speedBar.getValue(), this);
        channelsCount = ReadMidi.getCommandsPerNoteOn(filePath);
        initPlay();
        midiPlayer.startPlaying();
        while (midiPlayer.getSequencer() == null)
        {
        }

        playstate = playing;
        changePlayButtonImage();

        return;

    }

    private void initPlay()
    {
        iStarttimes = MidiFileJavax.getAllStartTimesSorted(midifile.getTracks(), false);
        noteCounter = 0;
        eventcount = 0;
        WaitForSong wait = new WaitForSong(this);
        wait.start();

    }

    private void changePlayButtonImage()
    {
        if (playstate == playing)
        {
            changeButtonImage(playButton, "images/pause.png");
            playButton.setToolTipText("Pause");
        }
        else
        {
            changeButtonImage(playButton, "images/play.png");
            playButton.setToolTipText("Play");
        }
    }

    private static void changeButtonImage(JButton button, String imagefilePath)
    {
        int width = button.getWidth();
        int height = button.getHeight();

        button.setIcon(ImageHelper.loadIcon(imagefilePath, width, height));

    }

    private static void setVolume(int value)
    {
        if (volCtrl != null)
        {
            volCtrl.setValue(value);
        }
    }

    /**
     * The callback for the Stop button. If playing, initiate a stop and wait for the timer to
     * finish. Then do the actual stop.
     */
    public void Stop()
    {
        if (midifile == null || sheet == null || playstate == stopped)
        {
            return;
        }

        if (playstate == paused || playstate == playing)
        {
            DoStop();
        }
    }

    /**
     * Perform the actual stop, by stopping the sound, removing any shading, and clearing the state.
     */
    @Override
    public void DoStop()
    {
        playstate = stopped;
        StopSound();

        sheet.resetPlayPosition();
        piano.ShadeNotes(-999, prevPulseTime);

        startPulseTime = 0;
        currentPulseTime = 0;
        prevPulseTime = 0;
        changeButtonImage(playButton, "images/play.png");
        playButton.setToolTipText("Play");
        // midiPlayer.interrupt();
        return;
    }

    /**
     * Rewind the midi music back one measure. The music must be in the paused state. When we resume
     * in playPause, we start at the currentPulseTime. So to rewind, just decrease the
     * currentPulseTime, and re-shade the sheet music.
     */
    private void Rewind()
    {
        if (midifile == null || sheet == null || playstate != paused)
        {
            return;
        }
        /* Remove any highlighted notes */
        sheet.resetPlayPosition();
        piano.ShadeNotes(-10, currentPulseTime);

        prevPulseTime = currentPulseTime;
        currentPulseTime -= midifile.getTime().getMeasure();
        if (currentPulseTime < options.shifttime)
        {
            currentPulseTime = options.shifttime;
        }
        sheet.resetPlayPosition();
        piano.ShadeNotes(currentPulseTime, prevPulseTime);
        // TODO do later: We should add also add
        // sequencer.setTickposition(somethingElse);
        // and move index for events somewhat backward
    }

    /**
     * Fast forward the midi music by one measure. The music must be in the paused/stopped state.
     * When we resume in playPause, we start at the currentPulseTime. So to fast forward, just
     * increase the currentPulseTime and re-shade the sheet music.
     */
    private void FastForward()
    {
        if (midifile == null || sheet == null)
        {
            return;
        }
        if (playstate != paused && playstate != stopped)
        {
            return;
        }
        playstate = paused;

        /* Remove any highlighted notes */
        sheet.resetPlayPosition();
        piano.ShadeNotes(-10, currentPulseTime);

        prevPulseTime = currentPulseTime;
        currentPulseTime += midifile.getTime().getMeasure();
        if (currentPulseTime > midifile.getTotalPulses())
        {
            currentPulseTime -= midifile.getTime().getMeasure();
        }
        sheet.resetPlayPosition();
        piano.ShadeNotes(currentPulseTime, prevPulseTime);

        // TODO do later: We should add also add
        // sequencer.setTickposition(somethingElse);
        // and move index for events somewhat forward

    }

    /**
     * The callback for the timer. If the midi is still playing, update the currentPulseTime and
     * shade the sheet music. If a stop or pause has been initiated (by someone clicking the stop or
     * pause button), then stop the timer.
     */
    void TimerCallback()
    {
        if (midifile == null || sheet == null)
        {
            playstate = stopped;
            return;
        }
        else if (playstate == stopped || playstate == paused)
        {
            /* This case should never happen */
            return;
        }
        else if (playstate == initStop)
        {
            return;
        }
        else if (playstate == playing)
        {
            long msec = new Date().getTime() - startTime.getTime();
            prevPulseTime = currentPulseTime;
            currentPulseTime = (int) Math.round(startPulseTime + msec * pulsesPerMsec);

            /* If we're playing in a loop, stop and restart */
            if (options.playMeasuresInLoop)
            {
                double nearEndTime = currentPulseTime + pulsesPerMsec * 10;
                int measure = (int) (nearEndTime / midifile.getTime().getMeasure());
                if (measure > options.playMeasuresInLoopEnd)
                {
                    RestartPlayMeasuresInLoop();
                    return;
                }
            }

            /* Stop if we've reached the end of the song */
            if (currentPulseTime > midifile.getTotalPulses())
            {
                DoStop();
                return;
            }

            sheet.resetPlayPosition();
            piano.ShadeNotes(currentPulseTime, prevPulseTime);
            return;
        }
        else if (playstate == initPause)
        {
            long msec = new Date().getTime() - startTime.getTime();

            StopSound();

            prevPulseTime = currentPulseTime;
            currentPulseTime = (int) Math.round(startPulseTime + msec * pulsesPerMsec);
            sheet.resetPlayPosition();
            piano.ShadeNotes(currentPulseTime, prevPulseTime);
            playstate = paused;
            changeButtonImage(playButton, "images/pause.png");
            playButton.setToolTipText("Play");
            return;
        }
    }

    /**
     * The "Play Measures in a Loop" feature is enabled, and we've reached the last measure. Stop
     * the sound, unshade the music, and then start playing again.
     */
    private void RestartPlayMeasuresInLoop()
    {
        playstate = stopped;
        StopSound();

        sheet.resetPlayPosition();
        piano.ShadeNotes(-10, prevPulseTime);
        currentPulseTime = -1;
        prevPulseTime = 0;
        try
        {
            Thread.sleep(300);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        PlayPause();
    }

    /**
     * Callback for volume bar. Adjust the volume if the midi sound is currently playing.
     */
    private void ChangeVolume()
    {
        if (playstate == playing)
        {
            setVolume(volumeBar.getValue());
        }
    }

    /**
     * Callback for volume bar. Adjust the volume if the midi sound is currently playing.
     */
    private void ChangeZoom()
    {
        sheet.changeZoom(zoomBar.getValue());
        firePropertyChange("Zoom", true, false);
    }

    @Override
    public void controlChange(ShortMessage arg0)
    {
        if (arg0.getData2() == 1 && lastData != 3)
        {
            // We get two events for every actual event
            eventcount++;
            if (eventcount % channelsCount == 0)
            {
                // Note is starting
                currentPulseTime = iStarttimes.get(noteCounter++);
                sheet.setPlayPositionTime(currentPulseTime);
                piano.ShadeNotes(currentPulseTime, prevPulseTime);
                prevPulseTime = currentPulseTime;
            }
        }
        else
        {
            // Note is ending
            // System.out.println("Ending note");
        }

        lastData = arg0.getData2();

    }

    /**
     * Should only be stopped if status == playing but not isRunning (= have played all song)
     */
    @Override
    public boolean shouldBeStopped()
    {

        if (playstate == playing)
        {
            return !midiPlayer.isRunning();
        }
        else
        {
            // Should not be stopped now, maybe just paused
            return false;
        }

    }

   public void setTempo(int value)
   {
      speedBar.setValue(value);
   }

public int getZoomValue()
{
    return zoomBar.getValue();
}
}
