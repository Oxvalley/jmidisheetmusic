package core.midi.sheet.music;

import javax.swing.JButton;

public class JButtonExt extends JButton
{

    /**
    *
    */
    private static final long serialVersionUID = 1L;
    private int mTag;

    public int getTag()
    {
        return mTag;
    }

    public void setTag(int tag)
    {
        mTag = tag;
    }

}
