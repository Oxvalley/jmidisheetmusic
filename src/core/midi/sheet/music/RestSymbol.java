package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

/* @class RestSymbol
 * A Rest symbol represents a rest - whole, half, quarter, or eighth.
 * The Rest symbol has a starttime and a duration, just like a regular
 * note.
 */
public class RestSymbol extends MusicSymbol
{
   private long starttime;
   /** The starttime of the rest */
   private NoteDuration duration;
   /** The rest duration (eighth, quarter, half, whole) */
   private int width;

   /** The width in pixels */

   /**
    * Create a new rest symbol with the given start time and duration
    * 
    * @param iSizes
    */
   public RestSymbol(long prevtime, NoteDuration dur, SheetMusicSizes sizes)
   {
      iSizes = sizes;
      starttime = prevtime;
      duration = dur;
      width = getMinWidth();
   }

   /**
    * Get the time (in pulses) this symbol occurs at. This is used to determine
    * the measure this symbol belongs to.
    */
   @Override
   public long getStartTime()
   {
      return starttime;
   }

   /**
    * Get/Set the width (in pixels) of this symbol. The width is set in
    * SheetMusic.AlignSymbols() to vertically align symbols.
    */
   @Override
   public int getWidth()
   {
      return width;
   }

   @Override
   public void setWidth(int value)
   {
      width = value;
   }

   /** Get the minimum width (in pixels) needed to draw this symbol */
   @Override
   public int getMinWidth()
   {
      return 2 * iSizes.getNoteHeight() + iSizes.getNoteHeight() / 2;
   }

   /**
    * Get the number of pixels this symbol extends above the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getAboveStaff()
   {
      return 0;
   }

   /**
    * Get the number of pixels this symbol extends below the staff. Used to
    * determine the minimum height needed for the staff (Staff.FindBounds).
    */
   @Override
   public int getBelowStaff()
   {
      return 0;
   }

   /**
    * Draw the symbol.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   @Override
   public void Draw(Graphics g, Color pen, int ytop)
   {
      /* Align the rest symbol to the right */
      g.translate(getWidth() - getMinWidth(), 0);
      g.translate(iSizes.getNoteHeight() / 2, 0);

      if (duration == NoteDuration.Whole)
      {
         DrawWhole(g, pen, ytop);
      }
      else if (duration == NoteDuration.Half)
      {
         DrawHalf(g, pen, ytop);
      }
      else if (duration == NoteDuration.Quarter)
      {
         DrawQuarter(g, pen, ytop);
      }
      else if (duration == NoteDuration.Eighth)
      {
         DrawEighth(g, pen, ytop);
      }
      g.translate(-iSizes.getNoteHeight() / 2, 0);
      g.translate(-(getWidth() - getMinWidth()), 0);
   }

   /**
    * Draw a whole rest symbol, a rectangle below a staff line.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   public void DrawWhole(Graphics g, Color pen, int ytop)
   {
      int y = ytop + iSizes.getNoteHeight();
      g.setColor(pen);
      g.fillRect(0, y, iSizes.getNoteWidth(), iSizes.getNoteHeight() / 2);
   }

   /**
    * Draw a half rest symbol, a rectangle above a staff line.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    */
   public void DrawHalf(Graphics g, Color pen, int ytop)
   {
      int y = ytop + iSizes.getNoteHeight() + iSizes.getNoteHeight() / 2;
      g.setColor(pen);
      g.fillRect(0, y, iSizes.getNoteWidth(), iSizes.getNoteHeight() / 2);
   }

   /**
    * Draw a quarter rest symbol.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    * @param iSizes
    */
   public void DrawQuarter(Graphics g, Color pen, int ytop)
   {
      int y = ytop + iSizes.getNoteHeight() / 2;
      int x = 2;
      int xend = x + 2 * iSizes.getNoteHeight() / 3;

      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      g2.setColor(pen);
      g2.drawLine(x, y, xend - 1, y + iSizes.getNoteHeight() - 1);

      y = ytop + iSizes.getNoteHeight() + 1;
      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THICK_SCORE_MUSIC_LINE));
      g2.drawLine(xend - 2, y, x, y + iSizes.getNoteHeight());

      y = ytop + iSizes.getNoteHeight() * 2 - 1;
      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      g2.drawLine(0, y, xend + 2, y + iSizes.getNoteHeight());

      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THICK_SCORE_MUSIC_LINE));
      if (iSizes.getNoteHeight() == 6)
      {
         g2.drawLine(xend, y + 1 + 3 * iSizes.getNoteHeight() / 4, x / 2,
               y + 1 + 3 * iSizes.getNoteHeight() / 4);
      }
      else
      { /* NoteHeight == 8 */
         g2.drawLine(xend, y + 3 * iSizes.getNoteHeight() / 4, x / 2,
               y + 3 * iSizes.getNoteHeight() / 4);
      }

      g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));
      g2.drawLine(0, y + 2 * iSizes.getNoteHeight() / 3 + 1, xend - 1,
            y + 3 * iSizes.getNoteHeight() / 2);

      ThickGraphics.resetStrokeThickness(g2);
   }

   /**
    * Draw an eighth rest symbol.
    *
    * @param ytop
    *           The ylocation (in pixels) where the top of the staff starts.
    * @param iSizes
    */
   public void DrawEighth(Graphics g, Color pen, int ytop)
   {
      int y = ytop + iSizes.getNoteHeight() - 1;
      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

      g2.setColor(Color.BLACK);
      g2.fill(new Ellipse2D.Double(0, y + 1, iSizes.getLineSpace() - 1, iSizes.getLineSpace() - 1));
      g2.setColor(pen);
      g2.drawLine((iSizes.getLineSpace() - 2) / 2, y + iSizes.getLineSpace() - 1,
            3 * iSizes.getLineSpace() / 2, y + iSizes.getLineSpace() / 2);
      g2.drawLine(3 * iSizes.getLineSpace() / 2, y + iSizes.getLineSpace() / 2,
            3 * iSizes.getLineSpace() / 4, y + iSizes.getNoteHeight() * 2);

      ThickGraphics.resetStrokeThickness(g2);
   }

   @Override
   public String toString()
   {
      return String.format(
            "RestSymbol starttime=" + starttime + " duration=" + duration + " width=" + width);
   }

}
