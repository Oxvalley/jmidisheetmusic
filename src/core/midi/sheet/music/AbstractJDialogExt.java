package core.midi.sheet.music;

import javax.swing.JDialog;

public abstract class AbstractJDialogExt extends JDialog
{
    /**
    * 
    */
   private static final long serialVersionUID = -1220259589241393908L;
   protected boolean ok_clicked = false;

    public boolean ShowDialog()
    {
        setVisible(true);
        return ok_clicked;
    }

    public AbstractJDialogExt()
    {
        setModal(true);
    }

    public void Dispose()
    {
        setVisible(false);
    }

    /**
     * Set it to true if OK is clicked. <br/>
     * Set to false if cancelled is clicked (default)
     *
     * @param value
     */
    public void setOKClicked(boolean value)
    {
        ok_clicked = value;
    }
}
