package core.midi.sheet.music;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 * @class Stem The Stem class is used by ChordSymbol to draw the stem portion of
 *        the chord. The stem has the following fields:
 *
 *        duration - The duration of the stem. direction - Either Up or Down
 *        side - Either left or right top - The topmost note in the chord bottom
 *        - The bottommost note in the chord end - The note position where the
 *        stem ends. This is usually six notes past the last note in the chord.
 *        For 8th/16th notes, the stem must extend even more.
 *
 *        The SheetMusic class can change the direction of a stem after it has
 *        been created. The side and end fields may also change due to the
 *        direction change. But other fields will not change.
 */

public class Stem
{
   public static int Up = 1; /* The stem points up */
   public static int Down = 2; /* The stem points down */
   public static int LeftSide = 1; /* The stem is to the left of the note */
   public static int RightSide = 2; /* The stem is to the right of the note */

   private NoteDuration duration;
   /** Duration of the stem. */
   private int direction;
   /** Up, Down, or None */
   private WhiteNote top;
   /** Topmost note in chord */
   private WhiteNote bottom;
   /** Bottommost note in chord */
   private WhiteNote end;
   /** Location of end of the stem */
   private boolean notesoverlap;
   /** Do the chord notes overlap */
   private int side;
   /** Left side or right side of note */

   private Stem pair;
   /**
    * If pair != null, this is a horizontal beam stem to another chord
    */
   private int width_to_pair;
   /** The width (in pixels) to the chord pair */
   private boolean receiver_in_pair;
   private SheetMusicSizes iSizes;

   /**
    * This stem is the receiver of a horizontal beam stem from another chord.
    */

   /** Get/Set the direction of the stem (Up or Down) */
   public int getDirection()
   {
      return direction;
   }

   public void setWidth(int value)
   {
      ChangeDirection(value);
   }

   /** Get the duration of the stem (Eigth, Sixteenth, ThirtySecond) */
   public NoteDuration getDuration()
   {
      return duration;
   }

   /**
    * Get the top note in the chord. This is needed to determine the stem
    * direction
    */
   public WhiteNote getTop()
   {
      return top;
   }

   /**
    * Get the bottom note in the chord. This is needed to determine the stem
    * direction
    */
   public WhiteNote getBottom()
   {
      return bottom;
   }

   /**
    * Get/Set the location where the stem ends. This is usually six notes past
    * the last note in the chord. See method CalculateEnd.
    */
   public WhiteNote getEnd()
   {
      return end;
   }

   public void setEnd(WhiteNote value)
   {
      end = value;
   }

   /**
    * Set this Stem to be the receiver of a horizontal beam, as part of a chord
    * pair. In Draw(), if this stem is a receiver, we don't draw a curvy stem,
    * we only draw the vertical line.
    */
   public boolean getReceiver()
   {
      return receiver_in_pair;
   }

   public void setReceiver(boolean value)
   {
      receiver_in_pair = value;
   }

   /**
    * Create a new stem. The top note, bottom note, and direction are needed for
    * drawing the vertical line of the stem. The duration is needed to draw the
    * tail of the stem. The overlap boolean is true if the notes in the chord
    * overlap. If the notes overlap, the stem must be drawn on the right side.
    *
    * @throws Exception
    */
   public Stem(WhiteNote bottom, WhiteNote top, NoteDuration duration, int direction,
         boolean overlap, SheetMusicSizes sizes) throws Exception
   {
      iSizes = sizes;
      this.top = top;
      this.bottom = bottom;
      this.duration = duration;
      this.direction = direction;
      this.notesoverlap = overlap;
      if (direction == Up || notesoverlap)
         side = RightSide;
      else
         side = LeftSide;
      end = CalculateEnd();
      pair = null;
      width_to_pair = 0;
      receiver_in_pair = false;
   }

   /**
    * Calculate the vertical position (white note key) where the stem ends
    *
    * @throws Exception
    */
   public WhiteNote CalculateEnd() // throws Exception
   {
      if (direction == Up)
      {
         WhiteNote w = top;
         w = w.add(6);
         if (duration == NoteDuration.Sixteenth)
         {
            w = w.add(2);
         }
         else if (duration == NoteDuration.ThirtySecond)
         {
            w = w.add(4);
         }
         return w;
      }
      else if (direction == Down)
      {
         WhiteNote w = bottom;
         w = w.add(-6);
         if (duration == NoteDuration.Sixteenth)
         {
            w = w.add(-2);
         }
         else if (duration == NoteDuration.ThirtySecond)
         {
            w = w.add(-4);
         }
         return w;
      }
      else
      {
         return null; /* Shouldn't happen */
      }
   }

   /**
    * Change the direction of the stem. This function is called by
    * ChordSymbol.MakePair(). When two chords are joined by a horizontal beam,
    * their stems must point in the same direction (up or down).
    *
    * @throws Exception
    */
   public void ChangeDirection(int newdirection) // throws Exception
   {
      direction = newdirection;
      if (direction == Up || notesoverlap)
         side = RightSide;
      else
         side = LeftSide;
      end = CalculateEnd();
   }

   /**
    * Pair this stem with another Chord. Instead of drawing a curvy tail, this
    * stem will now have to draw a beam to the given stem pair. The width (in
    * pixels) to this stem pair is passed as argument.
    */
   public void SetPair(Stem pair1, int width_to_pair1)
   {
      this.pair = pair1;
      this.width_to_pair = width_to_pair1;
   }

   /** Return true if this Stem is part of a horizontal beam. */
   public boolean isBeam()
   {
      return receiver_in_pair || (pair != null);
   }

   /**
    * Draw this stem.
    *
    * @param ytop
    *           The y location (in pixels) where the top of the staff starts.
    * @param topstaff
    *           The note at the top of the staff.
    * @param iSizes
    */
   public void Draw(Graphics g, Color pen, int ytop, WhiteNote topstaff)
   {
      if (duration == NoteDuration.Whole)
         return;

      DrawVerticalLine(g, pen, ytop, topstaff);
      if (duration == NoteDuration.Quarter || duration == NoteDuration.DottedQuarter
            || duration == NoteDuration.Half || duration == NoteDuration.DottedHalf
            || receiver_in_pair)
      {

         return;
      }

      if (pair != null)
         DrawHorizBarStem(g, pen, ytop, topstaff);
      else
         DrawCurvyStem(g, pen, ytop, topstaff);
   }

   /**
    * Draw the vertical line of the stem
    *
    * @param ytop
    *           The y location (in pixels) where the top of the staff starts.
    * @param topstaff
    *           The note at the top of the staff.
    * @param iSizes
    */
   private void DrawVerticalLine(Graphics g, Color pen, int ytop, WhiteNote topstaff)
   {
      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THIN_SCORE_MUSIC_LINE));

      int xstart;
      g2.setColor(pen);
      if (side == LeftSide)
         xstart = iSizes.getLineSpace() / 4 + 1;
      else
         xstart = iSizes.getLineSpace() / 4 + iSizes.getNoteWidth();

      if (direction == Up)
      {
         int y1 = ytop + topstaff.Dist(bottom) * iSizes.getNoteHeight() / 2
               + iSizes.getNoteHeight() / 4;

         int ystem = ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2;

         g2.drawLine(xstart, y1, xstart, ystem);
      }
      else if (direction == Down)
      {
         int y1 = ytop + topstaff.Dist(top) * iSizes.getNoteHeight() / 2 + iSizes.getNoteHeight();

         if (side == LeftSide)
            y1 -= (iSizes.getNoteHeight() / 1.7);
         else
            y1 -= (iSizes.getNoteHeight() / 1.7);

         int ystem =
               ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2 + iSizes.getNoteHeight();

         g2.drawLine(xstart, y1, xstart, ystem);
      }

      ThickGraphics.resetStrokeThickness(g2);
   }

   /**
    * Draw a curvy stem tail. This is only used for single chords, not chord
    * pairs.
    *
    * @param ytop
    *           The y location (in pixels) where the top of the staff starts.
    * @param topstaff
    *           The note at the top of the staff.
    * @param iSizes
    */
   private void DrawCurvyStem(Graphics g, Color pen, int ytop, WhiteNote topstaff)
   {

      Graphics2D g2 = ThickGraphics.setStrokeThickness(g, 1);
      g2.setColor(pen);

      int xstart = 0;
      if (side == LeftSide)
         xstart = iSizes.getLineSpace() / 4 + iSizes.getPenSize(1);
      else
         xstart = iSizes.getLineSpace() / 4 + iSizes.getNoteWidth();

      if (direction == Up)
      {
         int ystem = ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2 - iSizes.getPenSize(1);

         if (duration == NoteDuration.Eighth || duration == NoteDuration.DottedEighth
               || duration == NoteDuration.Triplet || duration == NoteDuration.Sixteenth
               || duration == NoteDuration.ThirtySecond)
         {

            Polygon pol = drawNoteFlagUp(g2, xstart, ystem);
         }
         ystem += iSizes.getNoteHeight();

         if (duration == NoteDuration.Sixteenth || duration == NoteDuration.ThirtySecond)
         {

            Polygon pol = drawNoteFlagUp(g2, xstart, ystem);
         }

         ystem += iSizes.getNoteHeight();
         if (duration == NoteDuration.ThirtySecond)
         {
            Polygon pol = drawNoteFlagUp(g2, xstart, ystem);
         }

      }

      else if (direction == Down)
      {
         int ystem = ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2 + iSizes.getNoteHeight()
               + iSizes.getPenSize(1);

         if (duration == NoteDuration.Eighth || duration == NoteDuration.DottedEighth
               || duration == NoteDuration.Triplet || duration == NoteDuration.Sixteenth
               || duration == NoteDuration.ThirtySecond)
         {
            Polygon pol = drawNoteFlagDown(g2, xstart, ystem);
         }
         ystem -= iSizes.getNoteHeight();

         if (duration == NoteDuration.Sixteenth || duration == NoteDuration.ThirtySecond)
         {
            Polygon pol = drawNoteFlagDown(g2, xstart, ystem);
         }

         ystem -= iSizes.getNoteHeight();
         if (duration == NoteDuration.ThirtySecond)
         {
            Polygon pol = drawNoteFlagDown(g2, xstart, ystem);
         }

      }
      ThickGraphics.resetStrokeThickness(g2);
   }

   private Polygon drawNoteFlagDown(Graphics2D g2, int xstart, int ystem)
   {
      PolygonExt pol = new PolygonExt();
      pol.addPoint(xstart, ystem);
      pol.addPoint(xstart, ystem - (int) Math.round(iSizes.getLineSpace() * .4));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .33),
            ystem - (int) Math.round(iSizes.getLineSpace() * .8));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            ystem - (int) Math.round(iSizes.getLineSpace() * 1.4));

      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            ystem - Math.round(iSizes.getLineSpace() * 2));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .83),
            (int) (ystem - Math.round(iSizes.getLineSpace() * 1.4)));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            (int) (ystem - Math.round(iSizes.getLineSpace() * .8)));

      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .33),
            ystem - (int) Math.round(iSizes.getLineSpace() * .2));
      g2.fillPolygon(pol);
      return pol;
   }

   private Polygon drawNoteFlagUp(Graphics2D g2, int xstart, int ystem)
   {
      // 8,18 | 8,20 | 10,22 | 12,25 | 12,28 | 13,25 | 12,22 | 10,19

      PolygonExt pol = new PolygonExt();
      pol.addPoint(xstart, ystem);
      pol.addPoint(xstart, ystem + (int) Math.round(iSizes.getLineSpace() * .4));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .33),
            ystem + (int) Math.round(iSizes.getLineSpace() * .8));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            ystem + (int) Math.round(iSizes.getLineSpace() * 1.4));

      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            ystem + Math.round(iSizes.getLineSpace() * 2));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .83),
            (int) (ystem + Math.round(iSizes.getLineSpace() * 1.4)));
      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .67),
            (int) (ystem + Math.round(iSizes.getLineSpace() * .8)));

      pol.addPoint(xstart + (int) Math.round(iSizes.getNoteHeight() * .33),
            ystem + (int) Math.round(iSizes.getLineSpace() * .2));
      g2.fillPolygon(pol);
      return pol;
   }

   /*
    * Draw a horizontal beam stem, connecting this stem with the Stem pair.
    *
    * @param ytop The y location (in pixels) where the top of the staff starts.
    *
    * @param topstaff The note at the top of the staff.
    */
   private void DrawHorizBarStem(Graphics g, Color pen, int ytop, WhiteNote topstaff)
   {
      Graphics2D g2 = ThickGraphics.setStrokeThickness(g,
            iSizes.getPenSize(SheetMusicSizes.THICK_SCORE_MUSIC_LINE));
      g2.setColor(pen);
      int xstart = 0;
      int xstart2 = 0;

      if (side == LeftSide)
         xstart = iSizes.getLineSpace() / 4 + 1;
      else if (side == RightSide)
         xstart = iSizes.getLineSpace() / 4 + iSizes.getNoteWidth();

      if (pair.side == LeftSide)
         xstart2 = iSizes.getLineSpace() / 4 + 1;
      else if (pair.side == RightSide)
         xstart2 = iSizes.getLineSpace() / 4 + iSizes.getNoteWidth();

      if (direction == Up)
      {
         int xend = width_to_pair + xstart2;
         int ystart = ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2;
         int yend = ytop + topstaff.Dist(pair.end) * iSizes.getNoteHeight() / 2;

         if (duration == NoteDuration.Eighth || duration == NoteDuration.DottedEighth
               || duration == NoteDuration.Triplet || duration == NoteDuration.Sixteenth
               || duration == NoteDuration.ThirtySecond)
         {

            g2.drawLine(xstart, ystart, xend, yend);
         }
         ystart += iSizes.getNoteHeight();
         yend += iSizes.getNoteHeight();

         /* A dotted eighth will connect to a 16th note. */
         if (duration == NoteDuration.DottedEighth)
         {
            int x = xend - iSizes.getNoteHeight();
            double slope = (yend - ystart) * 1.0 / (xend - xstart);
            int y = (int) (slope * (x - xend) + yend);

            g2.drawLine(x, y, xend, yend);
         }

         if (duration == NoteDuration.Sixteenth || duration == NoteDuration.ThirtySecond)
         {

            g2.drawLine(xstart, ystart, xend, yend);
         }
         ystart += iSizes.getNoteHeight();
         yend += iSizes.getNoteHeight();

         if (duration == NoteDuration.ThirtySecond)
         {
            g2.drawLine(xstart, ystart, xend, yend);
         }
      }

      else
      {
         int xend = width_to_pair + xstart2;
         int ystart =
               ytop + topstaff.Dist(end) * iSizes.getNoteHeight() / 2 + iSizes.getNoteHeight();
         int yend =
               ytop + topstaff.Dist(pair.end) * iSizes.getNoteHeight() / 2 + iSizes.getNoteHeight();

         if (duration == NoteDuration.Eighth || duration == NoteDuration.DottedEighth
               || duration == NoteDuration.Triplet || duration == NoteDuration.Sixteenth
               || duration == NoteDuration.ThirtySecond)
         {

            g2.drawLine(xstart, ystart, xend, yend);
         }
         ystart -= iSizes.getNoteHeight();
         yend -= iSizes.getNoteHeight();

         /* A dotted eighth will connect to a 16th note. */
         if (duration == NoteDuration.DottedEighth)
         {
            int x = xend - iSizes.getNoteHeight();
            double slope = (yend - ystart) * 1.0 / (xend - xstart);
            int y = (int) (slope * (x - xend) + yend);

            g2.drawLine(x, y, xend, yend);
         }

         if (duration == NoteDuration.Sixteenth || duration == NoteDuration.ThirtySecond)
         {

            g2.drawLine(xstart, ystart, xend, yend);
         }
         ystart -= iSizes.getNoteHeight();
         yend -= iSizes.getNoteHeight();

         if (duration == NoteDuration.ThirtySecond)
         {
            g2.drawLine(xstart, ystart, xend, yend);
         }
      }

   }

   @Override
   public String toString()
   {
      return String.format("Stem duration=" + duration + " direction=" + direction + " top="
            + top.toString() + " bottom=" + bottom.toString() + " end=" + end.toString() + ""
            + " overlap=" + notesoverlap + " side=" + side + " width_to_pair=" + width_to_pair
            + " receiver_in_pair=" + receiver_in_pair);
   }

}
