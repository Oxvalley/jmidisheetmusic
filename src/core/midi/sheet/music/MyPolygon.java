package core.midi.sheet.music;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


public class MyPolygon
{

    List<Point> iPointsList = new ArrayList<Point>();


   @Override
   public String toString()
   {
      String cords="";

      for (Point point : iPointsList)
    {

         cords += point.x + ", "+ point.y + " | ";
      }
       return "[ "+cords.substring(0, cords.length()-2)+ "]";

   }

public void addPoint(int x, int y)
{
    iPointsList.add(new Point(x,y));
}

public void drawPolygon(Graphics g)
{
    Point oldPoint = null;

    for (Point point : iPointsList)
  {
        if (oldPoint != null)
        {
            g.drawLine(oldPoint.x, oldPoint.y, point.x, point.y);
        }

        oldPoint = point;
    }
}

public void reset()
{
    iPointsList.clear();

}

}
