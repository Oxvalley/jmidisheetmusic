package core.midi.sheet.music;

public class WaitForSong extends Thread
{

   private IMidiPlayer imidiPlayer;

   public WaitForSong(IMidiPlayer midiPlayer)
   {
      imidiPlayer = midiPlayer;
   }

   @Override
   public void run()
   {
      waitASecond(1000);

      if (imidiPlayer != null)
      {
         while (!imidiPlayer.shouldBeStopped())
         {
            waitASecond(100);
         }
         imidiPlayer.DoStop();
      }

   }

   private static void waitASecond(int millis)
   {
      try
      {
         Thread.sleep(millis);
      }
      catch (InterruptedException e1)
      {
         e1.printStackTrace();
      }
   }

}
