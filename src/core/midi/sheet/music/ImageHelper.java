package core.midi.sheet.music;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class ImageHelper
{
   // This method returns a buffered image with the contents of an image
   public static BufferedImage toBufferedImage(Image image2)
   {
      Image image = image2;
      if (image instanceof BufferedImage)
      {
         return (BufferedImage) image;
      }

      // This code ensures that all the pixels in the image are loaded
      image = new ImageIcon(image).getImage();

      // Determine if the image has transparent pixels; for this method's
      // implementation, see e661 Determining If an Image Has Transparent Pixels
      boolean hasAlpha = hasAlpha(image);

      // Create a buffered image with a format that's compatible with the screen
      BufferedImage bimage = null;
      GraphicsEnvironment ge = GraphicsEnvironment
            .getLocalGraphicsEnvironment();
      try
      {
         // Determine the type of transparency of the new buffered image
         int transparency = Transparency.OPAQUE;
         if (hasAlpha)
         {
            transparency = Transparency.BITMASK;
         }

         // Create the buffered image
         GraphicsDevice gs = ge.getDefaultScreenDevice();
         GraphicsConfiguration gc = gs.getDefaultConfiguration();
         bimage = gc.createCompatibleImage(image.getWidth(null),
               image.getHeight(null), transparency);
      }
      catch (HeadlessException e)
      {
         // The system does not have a screen
      }

      if (bimage == null)
      {
         // Create a buffered image using the default color model
         int type = BufferedImage.TYPE_INT_RGB;
         if (hasAlpha)
         {
            type = BufferedImage.TYPE_INT_ARGB;
         }
         bimage = new BufferedImage(image.getWidth(null),
               image.getHeight(null), type);
      }

      // Copy image to buffered image
      Graphics g = bimage.createGraphics();

      // Paint the image onto the buffered image
      g.drawImage(image, 0, 0, image.getWidth(null), image.getHeight(null),
            null);
      g.dispose();

      return bimage;
   }

   // This method returns true if the specified image has transparent pixels
   public static boolean hasAlpha(Image image)
   {
      // If buffered image, the color model is readily available
      if (image instanceof BufferedImage)
      {
         BufferedImage bimage = (BufferedImage) image;
         return bimage.getColorModel().hasAlpha();
      }

      // Use a pixel grabber to retrieve the image's color model;
      // grabbing a single pixel is usually sufficient
      PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
      try
      {
         pg.grabPixels();
      }
      catch (InterruptedException e)
      {
      }

      // Get the image's color model
      ColorModel cm = pg.getColorModel();
      return cm.hasAlpha();
   }
   
	public static JButton getJButtonWithImage(String imagefilePath, int x,
			int y, int width, int height, String tooltip) {
		JButton button = new JButton();
		setImageOnJComponent(x,y,width,height,
				tooltip, button);
		button.setIcon(ImageHelper.loadIcon(imagefilePath, width,height));
		return button;
	}

	public static JLabel getJLabelWithImage(String imagefilePath,int x,
			int y, int width, int height, String tooltip) {
		JLabel label = new JLabel();
		setImageOnJComponent(x,y,width,height,
				tooltip, label);
		label.setIcon(ImageHelper.loadIcon(imagefilePath, width,height));
		return label;
	}

	public static void setImageOnJComponent(int x,
			int y, int width, int height, String tooltip, JComponent label) {
		label.setToolTipText(tooltip);
		label.setBounds(x,y,
				width, height);
	}

	public static ImageIcon loadImage(BufferedImage image, int width, int height) {
		BufferedImage scaledImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics2D = scaledImage.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
				RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
		graphics2D.drawImage(image, 0, 0, width, height, null);
		graphics2D.dispose();
		ImageIcon myIcon = new ImageIcon(scaledImage);
        return myIcon;
	
	}



   public static BufferedImage loadImage(String filenmame)
   {
      BufferedImage image;

      try
      {
         image = ImageIO.read(new File(filenmame));
      }
      catch (IOException ex)
      {
         JOptionPane.showMessageDialog(null, "Could not load image "
               + filenmame, null, JOptionPane.ERROR_MESSAGE);
         return null;
      }
      return image;
   }

   public static ImageIcon loadIcon(String filePath)
   {
      return loadIcon(filePath, 0, 0);
   }
   
   public static ImageIcon loadIcon(String filePath, int width, int height)
   {
      Image img1 = loadIconImage(filePath);
      Image img2 = null;
      if (width + height > 0)
      {
         img2 = img1.getScaledInstance(width, height, Image.SCALE_SMOOTH);
      }
      else
      {
         img2 = img1;
      }

      return new ImageIcon(img2);
   }

   public static Image loadIconImage(String filePath)
   {
      return Toolkit.getDefaultToolkit().createImage(
            loadImage(filePath).getSource());
   }
}
