package core.midi.sheet.music;

import javax.swing.JRadioButtonMenuItem;

public class JRadioButtonMenuItemExt extends JRadioButtonMenuItem
{

   /**
    *
    */
   private static final long serialVersionUID = 1L;
   private long mTag = -1;

   public JRadioButtonMenuItemExt(String text)
   {
      super(text);
   }

   public JRadioButtonMenuItemExt(String text, long tag)
   {
      super(text);
      setTag(tag);
   }

   public long getTag()
   {
      return mTag;
   }

   public void setTag(long tag)
   {
      this.mTag = tag;
   }

}
