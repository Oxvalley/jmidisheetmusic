package core.midi.sheet.music;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.rtf.RTFEditorKit;

class RTFViewer extends JFrame
{
   public static final String HELP_RTF = "help.rtf";
   /**
    * 
    */
   private static final long serialVersionUID = 8889636444122676350L;

   public RTFViewer(String filePath)
   {
      setTitle("RTF Text Application");
      setSize(400, 240);
      setBackground(Color.gray);
      getContentPane().setLayout(new BorderLayout());

      JPanel topPanel = new JPanel();
      topPanel.setLayout(new BorderLayout());
      getContentPane().add(topPanel, BorderLayout.CENTER);

      // Create an RTF editor window
      RTFEditorKit rtf = new RTFEditorKit();
      JEditorPane editor = new JEditorPane();
      editor.setEditorKit(rtf);
      editor.setBackground(Color.WHITE);

      // This text could be big so add a scroll pane
      JScrollPane scroller = new JScrollPane();
      scroller.getViewport().add(editor);
      topPanel.add(scroller, BorderLayout.CENTER);

      // Load an RTF file into the editor
      try
      {
         FileInputStream fi = new FileInputStream(filePath);
         rtf.read(fi, editor.getDocument(), 0);
         fi.close();
      }
      catch (FileNotFoundException e)
      {
         System.out.println("File not found");
      }
      catch (IOException e)
      {
         System.out.println("I/O error");
      }
      catch (BadLocationException e)
      {
      }
   }

   public static void main(String args[])
   {
      // Create an instance of the test application
      String filePath = HELP_RTF;
  RTFViewer mainFrame = new RTFViewer(filePath);
      mainFrame.setVisible(true);
   }

}