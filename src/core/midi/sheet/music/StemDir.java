package core.midi.sheet.music;

public enum StemDir
{
    Up, Down
};
