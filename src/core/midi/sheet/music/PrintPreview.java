package core.midi.sheet.music;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * An example of a printable window in Java 1.2. The key point here is that <B>any</B> component is
 * printable in Java 1.2. However, you have to be careful to turn off double buffering globally (not
 * just for the top-level window). See the PrintUtilities class for the printComponent method that
 * lets you print an arbitrary component with a single function call. 7/99 Marty Hall,
 * http://www.apl.jhu.edu/~hall/java/
 */

public class PrintPreview extends JFrame implements ActionListener
{
    /**
    *
    */
    private static final long serialVersionUID = 7520735100874225429L;

    public static void main(String[] args)
    {
        new PrintPreview(null);
    }

    private int pageNumber;
    private PageDivider iPd;
    private JPanel pagePanel;

    public PrintPreview(PageDivider pd)
    {
        super("Print Preview");
        pageNumber = 0;
        iPd = pd;
        WindowUtilities.setNativeLookAndFeel();
        Container content = getContentPane();

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBackground(Color.WHITE);

        JButton prevButton = new JButton("Prev Page");
        prevButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                changePage(-1);

            }

        });
        buttonPanel.add(prevButton);

        JButton printButton = new JButton("Print");
        printButton.addActionListener(this);
        buttonPanel.add(printButton);

        JButton nextButton = new JButton("Next Page");
        nextButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                changePage(1);

            }

        });
        buttonPanel.add(nextButton);

        setBackground(Color.WHITE);
        content.setBackground(Color.WHITE);
        content.add(buttonPanel, BorderLayout.SOUTH);
        pagePanel = new JPanel();
        pagePanel.setLayout(new BorderLayout());
        content.add(pagePanel);
        changePage(1);
        setVisible(true);
    }

    private void changePage(int i)
    {
        if (iPd.getPages().size() >= pageNumber + i && pageNumber + i >= 1)
        {
            pageNumber += i;
            pagePanel.removeAll();
            pagePanel.add(iPd.getPages().get(pageNumber - 1), BorderLayout.CENTER);
            pack();
            setSize(570, 900);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        PrintUtilities.printComponent(this);
    }
}
